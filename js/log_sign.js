
$(document).ready(function(){
    $(document).on('submit','#submit_log_sign',function(e)
    {
        var formObj =$(this);
        var formURL =formObj.attr("action");
        var formData = new FormData(this);
        $.ajax({
            url: formURL,
            type: 'POST',
            data:formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
               if(data=="success"){
                   window.location="http://localhost/school/admin/dashboard";
               }
               else{
                   alert(data);
               }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
            }
                        
        });
        e.preventDefault();
    });
});