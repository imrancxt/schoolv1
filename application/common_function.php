<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of common_function
 *
 * @author imran
 */
class common_function {

    public function print_session() {
        echo"<select id='session' name = 'year' style='width:100%;color:white;height:34px;line-height: 1.42857143;background-color:#31b0d5; border: 1px #ccc solid;border-radius: 0px'>";
        $option = array("2014", "2015", "2016", "2016", "2017", "2018");
        echo"<option>SESSION</option>";
        foreach ($option as $option) {
            $session = $option + 1;
            echo"<option>$option-$session</option>";
        }
        echo"</select>";
    }

    public function _url() {
        return "http://localhost/school/";
    }

    public function print_std_page($total_row, $status, $class) {
        $interval = 2;
        $loop_lim = $total_row / $interval;
        $extra = $total_row % $interval;
        $k = 0;
        $low = 0;
        $up = $interval;
        $page_counter = 0;
        for ($i = 0; $i < (int) $loop_lim; $i++) {
            $info[$k] = "$low-$up";
            $page_counter++;
            $page = "page-$page_counter";
            echo"<a href='#' class='$class' status='$status' limit='$info[$k]'>$page</a>";
            $low+=$interval;
            $k++;
        }
        if ($extra != 0) {
            if ($total_row < $interval) {
                $info = "0-$interval";
                echo"<a href='#' class='$class' status='$status' limit='$info'>page1</a>";
            } else {
                $info = "$low-$up";
                $page_counter++;
                $page = "page-$page_counter";
                echo"<a href='#' class='$class' status='$status' limit='$info'>$page</a>";
            }
        }
    }

    public function print_class_routine($time, $period, $sub, $teacher, $active, $class = "") {
        echo"<div class='item $active'>";
        echo"<div class='panel panel-warning'>
            <div class='panel-heading'>
                <h4>CLASS ROUTINE OF CLASS $class</h4>
            </div>
            <div class='table-responsive'>
                <table class='table table-bordered table-hover'>
                    <thead>
                        <tr class='active'>
                            <th>
                                TIME
                            </th>";

        if (isset($time)) {
            for ($i = 0; $i < count($time) / 7; $i++) {
                echo"<th>$time[$i]</th>";
            }
        }

        echo"</tr>
                        <tr class='danger'>
                            <th>
                                PERIOD
                            </th>";

        if (isset($period)) {
            for ($i = 0; $i < count($period) / 7; $i++) {
                echo"<th>$period[$i]</th>";
            }
        }

        echo"</tr>";

        if (isset($sub)) {
            $lim = count($sub) / 7;
            echo"<tr class='info'>";
            echo"<td>SAT</td>";
            for ($i = 0; $i < 1 * $lim; $i++) {
                echo"<td>$sub[$i]<br>$teacher[$i]</td>";
            }
            echo"</tr>";

            echo"<tr class='info'>";
            echo"<td>SUN</td>";
            for ($j = $i; $j < 2 * $lim; $j++) {
                echo"<td>$sub[$j]<br>$teacher[$j]</td>";
            }
            echo"</tr>";


            echo"<tr class='info'>";
            echo"<td>MON</td>";
            for ($k = $j; $k < 3 * $lim; $k++) {
                echo"<td>$sub[$k]<br>$teacher[$k]</td>";
            }
            echo"</tr>";

            echo"<tr class='warning'>";
            echo"<td>TUE</td>";
            for ($l = $k; $l < 4 * $lim; $l++) {
                echo"<td>$sub[$l]<br>$teacher[$l]</td>";
            }
            echo"</tr>";

            echo"<tr class='success'>";
            echo"<td>WED</td>";
            for ($m = $l; $m < 5 * $lim; $m++) {
                echo"<td>$sub[$m]<br>$teacher[$m]</td>";
            }
            echo"</tr>";

            echo"<tr class='success'>";
            echo"<td>THR</td>";
            for ($n = $m; $n < 6 * $lim; $n++) {
                echo"<td>$sub[$n]<br>$teacher[$n]</td>";
            }
            echo"</tr>";

            echo"<tr class='success'>";
            echo"<td>FRI</td>";
            for ($o = $n; $o < 7 * $lim; $o++) {
                echo"<td>$sub[$o]<br>$teacher[$o]</td>";
            }
            echo"</tr>";
        }

        echo"</thead>
                </table>
 
            </div>
        </div>";
        echo"</div>";
    }

    public function print_courses($serial, $course_title, $course_name, $active, $class) {
          echo"<div class='item $active'>";
        echo"<div class='panel panel-warning'>
            <div class='panel-heading'>
                <h4>COURSES OF CLASS $class</h4>
            </div>";
        echo"<div class = 'table-responsive'>
<table class ='table table-bordered table-hover'>
<tr class = 'active'>
<th>SERIAL</th>
<th>COURSE TITLE</th>
<th>COURSE NAME</th>
</tr>";
        if (isset($serial)) {
            for ($i = 0; $i < count($serial); $i++) {
                echo"<tr class='warning'><td>$serial[$i]</td><td>$course_title[$i]</td><td>$course_name[$i]</td></tr>";
            }
        }
        echo"</table>
         </div></div></div>";
    }

}
?>
