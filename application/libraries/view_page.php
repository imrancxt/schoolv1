<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of view_page
 *
 * @author imran
 */
class view_page {

//put your code here
    public $ci;

    function __construct() {
        $this->ci = &get_instance();
    }

    function admin_page($page_name = "", $data="") {
        //$custome_variable['asset_url'] = "http://localhost/school/";
        $theme="theme/paper-admin";
        $this->ci->load->view("$theme/header.php");
        $this->ci->load->view("vadmin/$page_name",$data);
        $this->ci->load->view("$theme/footer.php");
       // echo "test";
    }
    function admin_page2($page_name = "", $data) {
        $custome_variable['asset_url'] = "http://localhost/school/";
        $this->ci->load->view('vadmin/navigation2.php', $custome_variable);
        $this->ci->load->view("vadmin/$page_name", $data);
        $this->ci->load->view("vadmin/footer2.php");
    }

    public function get_dept_id($dept_name) {
        switch ($dept_name) {
            case "SCIENCE":
                return 'S';
            case "ARTS":
                return 'A';
            case "COMMERCE":
                return 'C';
            default:
                return 'O';
        }
    }

    public function genarate_id($dept, $series, $class, $roll, $section) {
        $dept_id = $this->get_dept_id($dept);
        $roll_no = $this->genarate_roll($roll);
        $id = "$dept_id$series$class$roll_no$section";
        return $id;
    }

    public function genarate_roll($roll) {
        $roll_lntn = strlen($roll);
        if ($roll_lntn == 3) {
            $roll_no = $roll;
        } else {
            if ($roll_lntn == 2) {
                $roll_no = "0$roll";
            } else {
                if ($roll_lntn == 1) {
                    $roll_no = "00$roll";
                } else {
                    $roll_no = $roll;
                }
            }
        }
        return $roll_no;
    }

    public function get_term($term) {
        switch ($term) {
            case '1ST TERM':
                return 1;
            case '2ND TERM':
                return 2;
            case '3RD TERM':
                return 3;
            case 'FINAL':
                return 'f';
            default:
                return 'f';
        }
    }

    public function get_full_exam_name($term) {
        switch ($term) {
            case '1':
                return "1ST TERM";
            case '2':
                return "2ND TERM";
            case '3':
                return "3RD TERM";
            case 'f':
                return 'FINAL';
            default:
                return 'NULL';
        }
    }

}

?>
