<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author imran
 */

class login extends CI_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        session_start();
    }

    function index() {
        $this->load->view('v_login');
    }

    public function submit_login() {
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();
        $rs = $this->db->get_where('admin_info', array('admin' => $data['admin'], 'password' => $data['password']));
        if ($rs->num_rows()> 0) {
            foreach ($rs->result() as $row) {
                $_SESSION['admin'] = $row->serial;
                $_SESSION['sidetop_color']=$row->sidetop_color;
                $_SESSION['body_color']=$row->body_color;
                $_SESSION['school_name']=$row->school_name;
            }
            $_SESSION['login_time'] = date("Y-m-d h:i:s");
            $logged_info = array('admin' => $_SESSION['admin'], 'login_time' => $_SESSION['login_time']);
            if ($this->db->insert('logged_time', $logged_info)) {
                echo"success";
            }
        } else {
            echo"WRONG ADMIN";
        }
    }

    public function log_out() {
        if (isset($_SESSION['admin'])) {
            $logout_time = date("Y-m-d h:i:s");
            if ($this->db->update('logged_time', array('logout_time' => $logout_time), array('admin' => $_SESSION['admin'], 'login_time' => $_SESSION['login_time']))) {
                $url = base_url() . "login";
                unset($_SESSION['admin']);
                header("Location:$url");
            }
        }
    }

}

?>
