<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin2
 *
 * @author imran
 */
session_start();
    class admin2 extends CI_Controller {

        //put your code here
        function __construct() {
            parent::__construct();
            $this->load->library('view_page');
            $this->load->model('mstudent');
            $this->load->database();
            $_SESSION['admin']="admin";
            $this->admin = $_SESSION['admin'];
            $this->asset_url = "http://localhost/school/";
        }

        public function update_student_transaction() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $amount = $data['amount'];
            if (isset($data['serial'])) {
                if ($data['trans_type'] == 'dr') {
                    $this->update_person_transaction('student_fee', $data['serial'], 'fee_amount', 'fee_due_amount', $amount);
                } else {
                    if ($data['trans_type'] == 'cr') {
                        $this->update_person_transaction('student_cost', $data['serial'], 'pay_amount', 'pay_due_amount', $amount);
                        //print_r($data);
                    }
                }
            }
        }

        public function update_teacher_transaction() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $amount = $data['amount'];
            if (isset($data['serial'])) {
                if ($data['trans_type'] == 'cr') {
                    $this->update_person_transaction('paidtoteacher', $data['serial'], 'sallary_ammount', 'sallary_due_amount', $amount);
                } else {
                    if ($data['trans_type'] == 'dr') {
                        $this->update_person_transaction('loanfromteacher', $data['serial'], 'loan_ammount', 'loan_due_amount', $amount);
                        //print_r($data);
                    }
                }
            }
        }

        public function update_stuff_transaction() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $amount = $data['amount'];
            if (isset($data['serial'])) {
                if ($data['trans_type'] == 'cr') {
                    $this->update_person_transaction('paidtostuff', $data['serial'], 'sallary_ammount', 'sallary_due_amount', $amount);
                } else {
                    if ($data['trans_type'] == 'dr') {
                        $this->update_person_transaction('paidbystuff', $data['serial'], 'pay_ammount', 'pay_due_amount', $amount);
                        //print_r($data);
                    }
                }
            }
        }

        private function update_person_transaction($table, $serial, $paid, $due, $amount) {
            $this->db->join("$table t2", 't1.serial = t2.serial');
            $this->db->where_in('t1.serial', $serial);
            $this->db->where('t1.admin', $this->admin);
            $rs = $this->db->get("$table t1");
            $info = array();
            if ($rs->num_rows() > 0) {
                $i = 0;
                foreach ($rs->result() as $row) {
                    $new_paid_amount = $row->$paid + $amount;
                    $new_due_amount = $row->$due - $amount;
                    $info[$i] = array('serial' => $row->serial, $paid => $new_paid_amount, $due => $new_due_amount);
                    $i++;
                }
                //print_r($info);
                $this->db->update_batch($table, $info, 'serial');
                echo"UPDATED!";
            }
        }

        public function admin_profile() {
            $query1 = "SELECT * FROM `logged_time` where admin=$this->admin limit 30";
            $query2 = "SELECT * FROM `admin_info` where serial=$this->admin";
            $data1 = $this->mstudent->get_logged_time($query1);
            $data2 = $this->mstudent->get_admin_info($query2);
            $data = array_merge($data1, $data2);
            // print_r($data2);
            $this->view_page->admin_page('v_admin_profile', $data);
        }

        public function get_admin_logged_date_btn_date($date1 = "", $date2 = "") {
            $query = "SELECT * FROM `logged_time` where admin=$this->admin  and login_time between '$date1 00:00:01' and '$date2 23:59:59' limit 30";
            $data = $this->mstudent->get_logged_time($query);
            $this->load->view('vadmin/v_filter_admin_log_time', $data);
        }

        public function change_admin_info() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $query1 = "SELECT * FROM `admin_info` where serial=$this->admin and password='{$data['old_password']}'";
            $info = $this->mstudent->get_admin_info($query1);
            if (isset($info['admin_serial'])) {
                if (isset($data['chng_name']) && isset($data['chng_password'])) {
                    if ($this->db->update('admin_info', array('admin' => $data['new_admin'], 'password' => $data['new_password']), array('serial' => $this->admin))) {
                        echo"UPDATED!";
                    }
                } else {
                    if (isset($data['chng_name'])) {
                        if ($this->db->update('admin_info', array('admin' => $data['new_admin']), array('serial' => $this->admin))) {
                            echo"UPDATED!";
                        }
                    } else {
                        if (isset($data['chng_password']) && $data['new_password'] != "") {
                            if ($this->db->update('admin_info', array('password' => $data['new_password']), array('serial' => $this->admin))) {
                                echo"UPDATED!";
                            }
                        }
                    }
                }
            }
        }

        public function add_new_admin() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array('admin' => $data['admin'], 'password' => $data['password']);
            if ($this->db->insert('admin_info', $info)) {
                echo"INSERTED";
            }
            //print_r($info);
        }

        public function upgrade_student($serial = "no") {
            $url = base_url() . "admin/addstudent";
            if ($serial > 0) {
                $query1 = "SELECT * FROM `student_info` where serial=$serial and admin=$this->admin";
                $query2 = "select * from parent_info as t1
                      where t1.id=(SELECT t2.id FROM `student_info` as t2 where t2.serial=$serial) and t1.admin=$this->admin";
                $data1 = $this->mstudent->mget_student_info($query1);
                $data2 = $this->mstudent->mget_partent_info($query2);
                $data3 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='father'", array('fatheroccupation', 'type'));
                $data4 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='mother'", array('motheroccupation', 'type'));
                $data = array_merge($data1, $data2, $data3, $data4);
                // print_r($data);
                if (isset($data['std_serial'])) {
                    $this->view_page->admin_page('v_upgrade_student', $data);
                } else {
                    header("Location:$url");
                }
            } else {
                header("Location:$url");
            }
        }

        public function coppy_image() {
            $url1 = base_url() . "img/slider/1.jpg";
            $url2 = base_url() . "img/slider/10.jpg";
            $url = $url1;
            $img = $url2;
            file_put_contents($img, file_get_contents($url));
        }

        public function student_dr_receipt( $serial = "") {
            $info = array();
            $query1 = "SELECT * FROM `student_fee` where admin=$this->admin and serial=$serial";
            $query2 = "SELECT * FROM `student_info` where admin=$this->admin and serial=(SELECT t2.std_serial FROM `student_fee` as t2 where t2.serial=$serial and t2.admin=$this->admin)";
            $query3 = "SELECT sum(fee_due_amount) as total_amount FROM `student_fee` where admin=$this->admin and 
                      std_serial=(SELECT t2.std_serial FROM `student_fee` as t2 where t2.serial=$serial and t2.admin=$this->admin)";
            $query4 = "SELECT * FROM `admin_info` where serial=$this->admin";
            $data1 = $this->mstudent->get_student_fee($query1);
            $data2 = $this->mstudent->mget_student_info($query2);
            $data3 = $this->mstudent->get_total_amount($query3, 'total_amount');
            $data4 = $this->mstudent->get_admin_info($query4);

            if (isset($data1['fee_serial'][0]) && isset($data2['std_name'][0])) {
                $info['receipt_date'] = $data1['fee_date'][0];
                $info['receipt_serial'] = $data1['fee_serial'][0];
                $info['receipt_for'] = $data1['fee_for'][0];
                $info['receipt_amount'] = $data1['fee_amount'][0];
                $info['receipt_type'] = $data1['std_fee_payment_method'][0];
                $info['receipt_due'] = $data1['fee_due_amount'][0];
                $info['receiver'] = "{$data2['std_name'][0]},CLASS:{$data2['class'][0]},SECTION:{$data2['section'][0]},SESSION:{$data2['session'][0]},IDENTITY:{$data2['std_id'][0]}";
                $info['total_due'] = $data3['total_amount'];
                $info['school_name'] = $data4['school_name'];
                $info['school_address'] = $data4['school_address'];
                $info['asset_url'] = $this->asset_url;
                $this->load->view('ajax_code/v_receipt2', $info);
            }
        }

        public function tech_dr_receipt($serial = "") {
            $query1 = "SELECT * FROM `loanfromteacher` where serial=$serial and admin=$this->admin";
            $query2 = "SELECT * FROM `teacher_basic_info` where admin=$this->admin and serial=(SELECT t2.teach_serial FROM `loanfromteacher` as t2 where t2.serial=$serial and t2.admin=$this->admin)";
            $query3 = "SELECT sum(loan_due_amount) as total_amount FROM `loanfromteacher` where admin=$this->admin and teach_serial=(SELECT t2.teach_serial FROM `loanfromteacher` as t2 where t2.serial=$serial and t2.admin=$this->admin)";
            $query4 = "SELECT * FROM `admin_info` where serial=$this->admin";
            $data1 = $this->mstudent->get_loan_from_teacher($query1);
            $data2 = $this->mstudent->get_current_teacher($query2);
            $data3 = $this->mstudent->get_total_amount($query3, 'total_amount');
            $data4 = $this->mstudent->get_admin_info($query4);
            if (isset($data1['loan_serial'][0]) && isset($data2['tname'][0])) {
                $info['receipt_date'] = $data1['loan_date'][0];
                $info['receipt_serial'] = $data1['loan_serial'][0];
                $info['receipt_for'] = $data1['loan_for'][0];
                $info['receipt_amount'] = $data1['loan_ammount'][0];
                $info['receipt_type'] = $data1['loan_payment_method'][0];
                $info['receipt_due'] = $data1['loan_due_amount'][0];
                $info['receiver'] = "{$data2['tname'][0]},IDENTITY:{$data2['tid'][0]}";
                $info['total_due'] = $data3['total_amount'];
                $info['school_name'] = $data4['school_name'];
                $info['school_address'] = $data4['school_address'];
                $info['asset_url'] = $this->asset_url;
                $this->load->view('ajax_code/v_receipt2', $info);
            }
        }

        public function test() {
            $class = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
            $i = 0;
            $data = array();
            foreach ($class as $class) {
                $data['class_course'][$i] = $this->mstudent->get_course_data("SELECT * FROM `course` where admin=$this->admin and class='$class'");
                $data['class'][$i] = $class;
                $i++;
            }
            // print_r($data);
            $this->view_page->admin_page2('v_test', $data);
        }
        public function solarsystem(){
             $this->view_page->admin_page2('v_solarsystem',"");
        }

        function addmoretedu() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['qualification'])) {
                for ($i = 0; $i < count($data['qualification']); $i++) {
                    $edu_info[$i] = array('admin' => $this->admin, 'tid' => $data['tid'], 'edu_type' => $data['qualification'][$i], 'pass_year' => $data['passing_year'][$i],
                        'specialization' => $data['specialization'][$i], 'institute' => $data['institute'][$i], 'cgpa' => $data['gpa'][$i]);
                }
                $rs = $this->db->query("select tid from teacher_basic_info where admin={$this->db->escape($this->admin)} and tid={$this->db->escape($data['tid'])}");
                if ($rs->num_rows() > 0) {
                    $this->db->insert_batch('teacher_edu', $edu_info);
                    echo"<div class='alert alert-success'>SUCCESS</div>";
                }
                //print_r($edu_info);
            //
            }
        }

        function addmorestedu() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['qualification'])) {
                for ($i = 0; $i < count($data['qualification']); $i++) {
                    $edu_info[$i] = array('admin' => $this->admin, 'stid' => $data['stid'], 'edu_type' => $data['qualification'][$i], 'pass_year' => $data['passing_year'][$i],
                        'specialization' => $data['specialization'][$i], 'institute' => $data['institute'][$i], 'cgpa' => $data['gpa'][$i]);
                }
                $rs = $this->db->query("select stid from stuff_basic_info where admin={$this->db->escape($this->admin)} and stid={$this->db->escape($data['stid'])}");
                if ($rs->num_rows() > 0) {
                    //print_r($edu_info);
                    $this->db->insert_batch('stuff_edu', $edu_info);
                    echo"<div class='alert alert-success'>SUCCESS</div>";
                }
                //print_r($edu_info);
            //
            }
        }

        public function deletestudentcourse() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            //$query="DELETE FROM `student_course`  WHERE `serial` ={$data['taken_course_serial'][0]}";
            if (isset($data['taken_course_serial'])) {
                foreach ($data['taken_course_serial'] as $serial) {
                    $this->db->delete('student_course', array('serial' => $serial));
                }
                echo"deleted";
            }
        }

        public function editstdresult($info) {
            $info2 = explode("-", $info);
            $student_serial = $info2[0];
            $exam_term = $info2[1];
            $query1 = "SELECT * FROM `std_exam_info` where admin={$this->db->escape($this->admin)} and student_serial={$this->db->escape($student_serial)}
                      and exam_term={$this->db->escape($exam_term)}";
            $query2 = "SELECT  distinct(t1.course_serial),t1.serial,t2.course_name,t2.course_title,t1.marks,t1.grade  FROM
                     `std_exam_marks` as t1,course as t2 where t1.course_serial=t2.serial && t1.admin={$this->db->escape($this->admin)} && t1.student_serial={$this->db->escape($student_serial)}
                      && t1.exam_term={$this->db->escape($exam_term)}";
            $data1 = $this->mstudent->get_exam_info($query1);
            $data2 = $this->mstudent->get_course_marks($query2);
            $data = array_merge($data1, $data2);
            $this->load->view('vadmin/v_update_result_sheet', $data);
            //print_r($data);
        }

        public function change_theme_color() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['sidetop_color'])) {
                if ($this->db->update('admin_info', array('sidetop_color' => $data['sidetop_color'], 'body_color' => $data['body_color']), array('serial' => $this->admin))) {
                    $_SESSION['sidetop_color'] = $data['sidetop_color'];
                    $_SESSION['body_color'] = $data['body_color'];
                    echo "THEMR COLOR CHANGED!..PLEAZE REFRESS THIS PAGE..";
                }
            }
        }

        public function update_school_info() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if ($this->db->update('admin_info', array('school_name' => $data['school_name'], 'school_address' => $data['school_address']), array('serial' => $this->admin))) {
                $_SESSION['school_name'] = $data['school_name'];
                echo"UPDATED!";
            }
        }

        public function updatestdresult() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['exam_mark_serial']) && isset($data['exam_serial'])) {
                $i = 0;
                $total_marks = 0;
                $exam_term = $this->view_page->get_term($data['exam_term']);
                $marks_info = array();
                foreach ($data['exam_mark_serial'] as $serial) {
                    $marks_info[$i] = array('serial' => $serial, 'admin' => $this->admin, 'exam_term' => $exam_term, 'marks' => $data["mark$serial"], 'grade' => $data["grade$serial"]);
                    $total_marks+=$data["mark$serial"];
                    $i++;
                }
                $exam_info = array('comment' => $data['exam_comment'], 'total_marks' => $total_marks, 'gpa' => $data['exam_gpa']);

                $this->db->trans_start();
                $this->db->update('std_exam_info', $exam_info, array('serial' => $data['exam_serial'], 'admin' => $this->admin, 'exam_term' => $exam_term));
                $this->db->update_batch('std_exam_marks', $marks_info, 'serial', 'admin', 'exam_term');
                echo"UPDATED!";
                $this->db->trans_complete();
            }
        }

        public function get_std_page($class = "", $status = "", $session = "", $limit = "") {
            $uplow = explode("-", $limit);
            $query = "SELECT t1.serial, t1.id,t1.class,t1.roll,t1.section,t1.student_name,t1.student_phone,t1.tution_fee,t2.father_name,t2.mother_name
                      FROM `student_info` as t1,parent_info as t2 where t1.id=t2.id and t1.admin=$this->admin and t1.class='$class' and t1.status='$status' and t1.session='$session'
                      group by t1.id order by ABS(t1.roll) asc limit $uplow[0],$uplow[1]";
            //echo $query;
            $data = $this->mstudent->get_current_student($query);
            $data['student_class'] = $class;
            $data['student_session'] = $session;
            $data['asset_url'] = $this->asset_url;
            if (isset($data['id'])) {
                if ($session == "current") {
                    $this->load->view('ajax_code/v_filter_current_std_tbl_body', $data);
                } else {
                    $this->load->view('ajax_code/v_filter_old_std_tbl_body', $data);
                }

                //print_r($data);
            }
        }

        public function get_std_rslt_page($class = "", $status = "", $session = "", $limit = "") {
            $uplow = explode("-", $limit);
            $query = "SELECT t1.serial, t1.id,t1.class,t1.roll,t1.section,t1.student_name,t1.student_phone,t1.tution_fee,t2.father_name,t2.mother_name
                      FROM `student_info` as t1,parent_info as t2 where t1.id=t2.id and t1.admin=$this->admin
                      and t1.class='$class' and t1.status='$status' and t1.session='$session'   group by t1.id order by ABS(t1.roll) asc limit $uplow[0],$uplow[1]";
            $data = $this->mstudent->get_current_student($query);
            $data['asset_url'] = $this->asset_url;
            $data['student_class'] = $class;
            $data['student_session'] = $session;
            if (isset($data['serial'])) {
                $this->load->view('ajax_code/v_filter_rslt_page_tbl_body', $data);
            }
        }

        public function stdprintresult($info = "") {
            $info2 = explode("-", $info);
            if (isset($info2[0]) && isset($info2[1])) {
                $data = $this->get_print_result_data($info2);
                $this->view_page->admin_page('v_stdprintresult', $data);
            }
        }

        private function get_print_result_data($info2) {
            $student_serial = $info2[0];
            $exam_term = $info2[1];
            $query1 = "SELECT * FROM `std_exam_info` where admin={$this->db->escape($this->admin)} and student_serial={$this->db->escape($student_serial)}
                      and exam_term={$this->db->escape($exam_term)}";
            $query2 = "SELECT  distinct(t1.course_serial),t1.serial,t2.course_name,t2.course_title,t1.marks,t1.grade  FROM
                     `std_exam_marks` as t1,course as t2 where t1.course_serial=t2.serial && t1.admin={$this->db->escape($this->admin)} && t1.student_serial={$this->db->escape($student_serial)}
                      && t1.exam_term={$this->db->escape($exam_term)}";
            $query3 = "SELECT * FROM `student_info` where serial={$this->db->escape($student_serial)} and admin=$this->admin";

            $data1 = $this->mstudent->get_exam_info($query1);
            $data2 = $this->mstudent->get_course_marks($query2);
            $data3 = $this->mstudent->mget_student_info($query3);
            $data = array_merge($data1, $data2, $data3);
            $data['exam_term_name'] = $this->view_page->get_full_exam_name($exam_term);
            return $data;
        }

        public function get_more_exam_result($info = "") {
            $info2 = explode("-", $info);
            if (isset($info2[0]) && isset($info2[1])) {
                $data = $this->get_print_result_data($info2);
                //print_r($data);
                $this->load->view('ajax_code/v_print_other_result', $data);
            }
        }

        public function addmoretdr() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();

            $check_data_base = "SELECT serial from teacher_basic_info where serial={$this->db->escape($data['teach_serial'])} and admin=$this->admin";
            if ($this->mstudent->check_data_base($check_data_base) == true) {
                $info = array('admin' => $this->admin, 'teach_serial' => $data['teach_serial'], 'loan_date' => $data['loan_date'],
                    'loan_for' => $data['loan_for'], 'payment_method' => $data['loan_payment_method'], 'loan_ammount' => $data['loan_ammount'], 'loan_due_amount' => $data['loan_due_amount']);
                if ($this->db->insert('loanfromteacher', $info)) {
                    echo"<div class='alert alert-success'>SUCCESS!</div>";
                } else {
                    echo"<div class='alert alert-warning'>FAILED!</div>";
                }
            }
        }

        public function addmoretcr() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();

            $check_data_base = "SELECT serial from teacher_basic_info where serial={$this->db->escape($data['teach_serial'])} and admin=$this->admin";
            if ($this->mstudent->check_data_base($check_data_base) == true) {
                $info = array('admin' => $this->admin, 'teach_serial' => $data['teach_serial'], 'sallary_date' => $data['sallary_date'],
                    'sallary_for' => $data['sallary_for'], 'payment_method' => $data['sallary_payment_method'], 'sallary_ammount' => $data['sallary_ammount'], 'sallary_due_amount' => $data['sallary_due_amount']);
                if ($this->db->insert('paidtoteacher', $info)) {
                    echo"<div class='alert alert-success'>SUCCESS!</div>";
                } else {
                    echo"<div class='alert alert-warning'>FAILED!</div>";
                }
            }
        }

        public function addstddr() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $check_data_base = "SELECT serial FROM `student_info` where serial={$this->db->escape($data['std_serial'])} and admin=$this->admin";
            if ($this->mstudent->check_data_base($check_data_base) == true) {
                $fee_date = $_POST['fee_date'];
                $fee_for = $_POST['fee_for'];
                $fee_amount = $_POST['fee_amount'];
                $fee_due_amount = $_POST['fee_due_amount'];
                $payment_method = $_POST['fee_payment_method'];
                $student_info = array('admin' => $this->admin, 'std_serial' => $data['std_serial'], 'fee_date' => $fee_date, 'fee_for' => $fee_for, 'fee_amount' => $fee_amount, 'fee_due_amount' => $fee_due_amount, 'payment_method' => $payment_method);
                if ($this->db->insert('student_fee', $student_info)) {
                    echo"SUCCESSFULLY INSERTED!";
                } else {
                    echo"FAILED!";
                }
            }
        }

        public function addstdcr() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $check_data_base = "SELECT serial FROM `student_info` where serial={$this->db->escape($data['std_serial'])} and admin=$this->admin";
            if ($this->mstudent->check_data_base($check_data_base) == true) {
                $pay_date = $_POST['pay_date'];
                $pay_for = $_POST['pay_for'];
                $pay_amount = $_POST['pay_amount'];
                $pay_due_amount = $_POST['pay_due_amount'];
                $payment_method = $_POST['sch_payment_method'];
                $student_info = array('admin' => $this->admin, 'std_serial' => $data['std_serial'], 'pay_date' => $pay_date, 'pay_for' => $pay_for, 'pay_amount' => $pay_amount, 'pay_due_amount' => $pay_due_amount, 'payment_method' => $payment_method);
                if ($this->db->insert('student_cost', $student_info)) {
                    echo"SUCCESSFULLY INSERTED!";
                } else {
                    echo"FAILED!";
                }
            }
        }

        public function create_event() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            print_r($data);
            //echo"{$data['content']}";
        }

        public function getstddrdata($serial) {
            $query1 = "SELECT * FROM `student_fee` where admin=$this->admin and serial={$this->db->escape($serial)}";
            $data1 = $this->mstudent->get_student_fee($query1);
            $data2 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='payment_method' && cost_type='pm'", array("payment_method", 'option'));
            $data3 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='student' && cost_type='dr'", array("studentdr", 'option'));
            $data = array_merge($data1, $data2, $data3);
            $this->load->view('ajax_code/v_stddredit', $data);
            // print_r($data1);
            // echo $serial;
        }

        public function getstdcrdata($serial = "") {
            $query1 = "SELECT * FROM `student_cost` where admin=$this->admin and serial={$this->db->escape($serial)}";
            $data1 = $this->mstudent->get_student_cost($query1);
            $data2 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='payment_method' && cost_type='pm'", array("payment_method", 'option'));
            $data3 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='student' && cost_type='cr'", array("studentcr", 'option'));
            $data = array_merge($data1, $data2, $data3);
            $this->load->view('ajax_code/v_stdcredit', $data);
        }

        public function updatestddr() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['fee_serial'])) {
                $check_data_base = "SELECT serial FROM `student_fee` where admin=$this->admin and serial={$this->db->escape($data['fee_serial'])}";
                if ($this->mstudent->check_data_base($check_data_base) == true) {
                    $info = array('fee_date' => $data['fee_date'], 'fee_for' => $data['fee_reason'], 'fee_amount' => $data['fee_amount'], 'fee_due_amount' => $data['fee_due_amount']
                        , 'payment_method' => $data['payment_method']);
                    //print_r($info);
                    if ($this->db->update('student_fee', $info, array('admin' => $this->admin, 'serial' => $data['fee_serial']))) {
                        echo"UPDATED!";
                    }
                }
            }
        }

        public function updatestdcr() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['pay_serial'])) {
                $check_data_base = "SELECT serial FROM `student_cost` where admin=$this->admin and serial={$this->db->escape($data['pay_serial'])}";
                if ($this->mstudent->check_data_base($check_data_base) == true) {
                    $info = array('pay_date' => $data['pay_date'], 'pay_for' => $data['pay_reason'], 'pay_amount' => $data['pay_amount'], 'pay_due_amount' => $data['pay_due_amount']
                        , 'payment_method' => $data['payment_method']);
                    //print_r($info);
                    if ($this->db->update('student_cost', $info, array('admin' => $this->admin, 'serial' => $data['pay_serial']))) {
                        echo"UPDATED!";
                    }
                }
            }
        }

        public function dltstddrdata($serial = "") {
            $check_data_base = "SELECT serial FROM `student_fee` where admin=$this->admin and serial={$this->db->escape($serial)}";
            if ($this->mstudent->check_data_base($check_data_base) == true) {
                if ($this->db->delete('student_fee', array('admin' => $this->admin, 'serial' => $serial))) {
                    echo"DELETED!";
                }
            }
        }

        public function dltstdcrdata($serial = "") {
            $check_data_base = "SELECT serial FROM `student_cost` where admin=$this->admin and serial={$this->db->escape($serial)}";
            if ($this->mstudent->check_data_base($check_data_base) == true) {
                if ($this->db->delete('student_cost', array('admin' => $this->admin, 'serial' => $serial))) {
                    echo"DELETED!";
                }
            }
        }

        public function getteachdrdata($serial = "") {
            $query1 = "SELECT * FROM `loanfromteacher` where serial={$this->db->escape($serial)} and admin=$this->admin";
            $data1 = $this->mstudent->get_loan_from_teacher($query1);
            $data2 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='teacher' && cost_type='dr'", array("teacherdr", 'option'));
            $data3 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='payment_method' && cost_type='pm'", array("payment_method", 'option'));
            $data = array_merge($data1, $data2, $data3);
            $this->load->view('ajax_code/v_teachdredit', $data);
            //print_r($data1);
        }

        public function updateteachdr() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['loan_serial'])) {
                $check_data_base = "SELECT serial FROM `loanfromteacher` where admin=$this->admin and serial={$this->db->escape($data['loan_serial'])}";
                if ($this->mstudent->check_data_base($check_data_base) == true) {
                    $info = array('loan_date' => $data['loan_date'], 'loan_for' => $data['loan_reason'], 'loan_ammount' => $data['loan_ammount'], 'loan_due_amount' => $data['loan_due_amount']
                        , 'payment_method' => $data['payment_method']);
                    if ($this->db->update('loanfromteacher', $info, array('admin' => $this->admin, 'serial' => $data['loan_serial']))) {
                        echo"UPDATED!";
                    }
                }
            }
        }

        public function dltteachdrdata($serial = "") {
            $check_data_base = "SELECT serial FROM `loanfromteacher` where admin=$this->admin and serial={$this->db->escape($serial)}";
            if ($this->mstudent->check_data_base($check_data_base) == true) {
                if ($this->db->delete('loanfromteacher', array('admin' => $this->admin, 'serial' => $serial))) {
                    echo"DELETED!";
                }
            }
        }

        function getteachcrdata($serial) {
            $query1 = "SELECT * FROM `paidtoteacher` where serial={$this->db->escape($serial)} and admin=$this->admin";
            $data1 = $this->mstudent->get_teacher_sallary_info($query1);
            $data2 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='teacher' && cost_type='cr'", array("teachercr", 'option'));
            $data3 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='payment_method' && cost_type='pm'", array("payment_method", 'option'));
            $data = array_merge($data1, $data2, $data3);
            $this->load->view('ajax_code/v_teachcredit', $data);
            //print_r($data);
        }

        public function updateteachcr() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['sallary_serial'])) {
                $check_data_base = "SELECT serial FROM `paidtoteacher` where admin=$this->admin and serial={$this->db->escape($data['sallary_serial'])}";
                if ($this->mstudent->check_data_base($check_data_base) == true) {
                    $info = array('sallary_date' => $data['sallary_date'], 'sallary_for' => $data['sallary_reason'], 'sallary_ammount' => $data['sallary_ammount'], 'sallary_due_amount' => $data['sallary_due_amount']
                        , 'payment_method' => $data['payment_method']);
                    if ($this->db->update('paidtoteacher', $info, array('admin' => $this->admin, 'serial' => $data['sallary_serial']))) {
                        echo"UPDATED!";
                    }
                }
            }
        }

        public function dltteachcrdata($serial = "") {
            $check_data_base = "SELECT serial FROM `paidtoteacher` where admin=$this->admin and serial={$this->db->escape($serial)}";
            if ($this->mstudent->check_data_base($check_data_base) == true) {
                if ($this->db->delete('paidtoteacher', array('admin' => $this->admin, 'serial' => $serial))) {
                    echo"DELETED!";
                }
            }
        }

        public function tran_edit_data($serial = "") {
            $query = "SELECT * FROM `transaction` where serial={$this->db->escape($serial)} and admin=$this->admin";
            $info = $this->mstudent->get_transaction_data($query);
            $this->load->view('ajax_code/v_edittransdata', $info);
        }

        public function update_trans() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['serial'])) {
                if ($data['tn_type'] == "DR") {
                    $info = array('description' => $data['description'], 'dr'=>$data['amount'],'cr'=>0,'tn_date'=>$data['tn_date']);
                }
                else{
                    $info = array('description' => $data['description'], 'cr'=>$data['amount'],'dr'=>0,'tn_date'=>$data['tn_date']);
                }
                if($this->db->update('transaction',$info,array('admin'=>$this->admin,'serial'=>$data['serial']))){
                    echo"<div class='alert alert-warning'>UPDATED!
                    <button type ='button' class = 'close' data-dismiss = 'alert' aria-label = 'Close'><span aria-hidden = 'true'>&times;
                    </span></button>
                    </div>";
                }
            }
        }
        public function periodictable(){
            $this->view_page->admin_page2('v_periodictable',"");
        }
        public function calculator(){
            $this->view_page->admin_page2('v_calculator',"");
        }
        public function game(){
            $this->view_page->admin_page2('v_snakegame',"");
        }
        public function about_me(){
             $this->view_page->admin_page2('v_about_me',"");
        }
        public function get_stuff_dr_receipt($serial=""){
            $query1 = "SELECT * FROM `paidbystuff` where serial=$serial and admin=$this->admin";
            $query2 = "SELECT * FROM `stuff_basic_info` where admin=$this->admin and serial=(SELECT t2.st_serial FROM `paidbystuff` as t2 where t2.serial=$serial and t2.admin=$this->admin)";
            $query3 = "SELECT sum(pay_due_amount) as total_amount FROM `paidbystuff` where admin=$this->admin and st_serial=(SELECT t2.st_serial FROM `paidbystuff` as t2 where t2.serial=$serial and t2.admin=$this->admin)";
            $query4 = "SELECT * FROM `admin_info` where serial=$this->admin";
            $data1 = $this->mstudent->get_paid_by_stuff($query1);
            $data2 = $this->mstudent->get_current_stuff($query2);
            $data3 = $this->mstudent->get_total_amount($query3, 'total_amount');
            $data4 = $this->mstudent->get_admin_info($query4);
            if (isset($data1['paidby_serial'][0]) && isset($data2['stname'][0])) {
                $info['receipt_date'] = $data1['pay_date'][0];
                $info['receipt_serial'] = $data1['paidby_serial'][0];
                $info['receipt_for'] = $data1['pay_for'][0];
                $info['receipt_amount'] = $data1['pay_ammount'][0];
                $info['receipt_type'] = $data1['paid_payment_method'][0];
                $info['receipt_due'] = $data1['pay_due_amount'][0];
                $info['receiver'] = "{$data2['stname'][0]},IDENTITY:{$data2['stid'][0]}";
                $info['total_due'] = $data3['total_amount'];
                $info['school_name'] = $data4['school_name'];
                $info['school_address'] = $data4['school_address'];
                $info['asset_url'] = $this->asset_url;
                $this->load->view('ajax_code/v_receipt2', $info);
            }
        }
       

    }


?>
