<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author imran
 */
session_start();
if (1==1) {

    class admin extends CI_Controller {

//put your code here
        function __construct() {
            parent::__construct();
            $_SESSION["admin"]=1;
            $this->load->library('view_page');
            $this->load->model('mstudent');
            $this->admin = $_SESSION['admin'];
            $this->asset_url = "http://localhost/school/";
            $this->default_class = "01";
            $this->default_session = "2014-2015";
        }

        function addstudent() {
            $data1 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='father'", array('fatheroccupation', 'type'));
            $data2 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='mother'", array('motheroccupation', 'type'));
            $data = array_merge($data1, $data2);
            $this->view_page->admin_page('v_addstudent', $data);
        }

        function add_new_student() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $series = substr($data['year'], 2, 2);
//$get_dept_id = $this->view_page->get_dept_id($data['dept']);
            $id = $this->view_page->genarate_id($data['dept'], $series, $data['class'], $data['roll'], $data['section']);
// echo $id;
//STORE STUDENT INFO
            $student_info = array('admin' => 1, 'id' => $id, 'student_name' => $data['student_name'], 'birth_date' => $data['birth_date'],
                'prev_ins' => $data['prev_ins'], 'student_phone' => $data['student_phone'], 'student_email' => $data['student_email'], 'student_address' => $data['student_address'],
                'class' => $data['class'], 'section' => $data['section'], 'roll' => $data['roll'], 'session' => $data['year'], 'tution_fee' => $data['tution_fee']);


//STORE PARENT INFO
            $parents_info = array('admin' => 1, 'id' => $id, 'father_name' => $data['father_name'], 'mother_name' => $data['mother_name'], 'religion' => $data['religion'],
                'parent_phone' => $data['parent_phone'], 'parent_email' => $data['parent_email'], 'parent_address' => $data['parent_address'],
                'father_occupation' => $data['father_occupation'], 'mother_occupation' => $data['mother_occupation']);


//SAVE DATA
            $this->mstudent->madd_new_student($student_info, $parents_info);
        }

        private function get_dept_id($dept_name) {
            switch ($dept_name) {
                case "SCIENCE":
                    return 0;
                case "ARTS":
                    return 1;
                case "COMMERCE":
                    return 2;
                default:
                    return 0;
            }
        }

        public function currentstudent() {
            $query1 = "SELECT t1.serial, t1.id,t1.class,t1.roll,t1.section,t1.student_name,t1.student_phone,t1.tution_fee,t2.father_name,t2.mother_name
                FROM `student_info` as t1,parent_info as t2 where t1.id=t2.id and t1.admin=$this->admin and t1.class='$this->default_class'  and t1.session='$this->default_session' and t1.status='current' 
                      group by t1.id order by ABS(t1.roll) asc limit 10";
            $data1 = $this->mstudent->get_current_student($query1);
            $data2 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='student' && cost_type='dr'", array("studentdr", 'option'));
            $data3 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='student' && cost_type='cr'", array("studentcr", 'option'));
            $data4 = $this->mstudent->get_only_teacher("SELECT tid,tname FROM `teacher_basic_info` where admin=$this->admin and status='current'");
            $data5 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='payment_method' && cost_type='pm'", array("payment_method", 'option'));
            $data = array_merge($data1, $data2, $data3, $data4, $data5);
            $data['student_class'] = $this->default_class;
            ;
            $data['student_session'] = $this->default_session;
            $total_std_query = "SELECT serial FROM `student_info` as t1 where t1.admin=$this->admin and t1.class='$this->default_class'  and t1.session='$this->default_session' and t1.status='current'";
            $rs = $this->db->query($total_std_query);
            $data['total_std'] = $rs->num_rows();

            $this->view_page->admin_page('v_currentstudent', $data);
        }

        public function oldstudent() {
            $query = "SELECT t1.serial, t1.id,t1.class,t1.roll,t1.section,t1.student_name,t1.student_phone,t1.tution_fee,t2.father_name,t2.mother_name
                FROM `student_info` as t1,parent_info as t2 where t1.id=t2.id and t1.admin=$this->admin and t1.class='$this->default_class'  and t1.session='$this->default_session' and t1.status='old' 
                      group by t1.id order by ABS(t1.roll) asc limit 10";
            $data = $this->mstudent->get_current_student($query);
            $data['student_class'] = $this->default_class;
            $data['student_session'] = $this->default_session;
            $total_std_query = "SELECT serial FROM `student_info` as t1 where t1.admin=$this->admin and t1.class='$this->default_class'  and t1.session='$this->default_session' and t1.status='old'";
            $rs = $this->db->query($total_std_query);
            $data['total_std'] = $rs->num_rows();
//print_r($data);
            $data['asset_url'] = $this->asset_url;
            $this->view_page->admin_page('v_oldstudent', $data);
        }

        public function filter_current_student($class, $status = 'current', $session = 'session') {
            $query = "SELECT t1.serial, t1.id,t1.class,t1.roll,t1.section,t1.student_name,t1.student_phone,t1.tution_fee,t2.father_name,t2.mother_name
                      FROM `student_info` as t1,parent_info as t2 where t1.id=t2.id and t1.admin=$this->admin and t1.class='$class' and t1.status='$status' and t1.session='$session'
                      group by t1.id order by ABS(t1.roll) asc limit 15";
            $data = $this->mstudent->get_current_student($query);
            $data['student_class'] = $class;
            $data['student_session'] = $session;
            $data['asset_url'] = $this->asset_url;
            $data['status'] = $status;
            $total_std_query = "SELECT serial FROM `student_info` as t1 where t1.admin=$this->admin and t1.class='$class'  and t1.session='$session' and t1.status='$status'";
            $rs = $this->db->query($total_std_query);
            $data['total_std'] = $rs->num_rows();

            $this->load->view('vadmin/v_filter_current_student_table', $data);
//print_r($data);
        }

        public function add_std_info() {
            /* $this->input->post(NULL, TRUE);
              $data = $this->input->post();
              print_r($data); */
            $message = "";
            $student_info = array();
            if (isset($_POST['serial'])) {
                $serial = $_POST['serial'];
                $option = $_POST['option'];
                if ($option == "add_fee") {
                    $fee_date = $_POST['fee_date'];
                    $fee_for = $_POST['fee_for'];
                    $fee_amount = $_POST['fee_amount'];
                    $fee_due_amount = $_POST['fee_due_amount'];
                    $payment_method = $_POST['fee_payment_method'];
                    for ($i = 0; $i < count($serial); $i++) {
                        $student_info[$i] = array('admin' => $this->admin, 'std_serial' => $serial[$i], 'fee_date' => $fee_date, 'fee_for' => $fee_for, 'fee_amount' => $fee_amount, 'fee_due_amount' => $fee_due_amount, 'payment_method' => $payment_method);
                    }
                    $this->db->trans_start();
                    $this->db->insert_batch('student_fee', $student_info);
//echo"FEE INSERTED!";
                    $message = "FEE INSERTED SUCCESSFULLY!";
                    $this->db->trans_complete();
                } else {
                    if ($option == "add_pay") {
                        $pay_date = $_POST['pay_date'];
                        $pay_for = $_POST['pay_for'];
                        $pay_amount = $_POST['pay_amount'];
                        $pay_due_amount = $_POST['pay_due_amount'];
                        $payment_method = $_POST['sch_payment_method'];
                        for ($i = 0; $i < count($serial); $i++) {
                            $student_info[$i] = array('admin' => $this->admin, 'std_serial' => $serial[$i], 'pay_date' => $pay_date, 'pay_for' => $pay_for, 'pay_amount' => $pay_amount, 'pay_due_amount' => $pay_due_amount, 'payment_method' => $payment_method);
                        }
                        $this->db->trans_start();
                        $this->db->insert_batch('student_cost', $student_info);
                        $message = "PAID SUCCESSFULLY!";
                        $this->db->trans_complete();
                    } else {
                        if ($option == "add_attendence") {
                            $attendence_date = $_POST['attendence_date'];
                            if (isset($_POST['class_teacher'])) {
                                $class_teacher = $_POST['class_teacher'];
                            } else {
                                $class_teacher = "NO";
                            }
                            $attendence_status = $_POST['attendence_status'];
                            for ($i = 0; $i < count($serial); $i++) {
                                $student_info[$i] = array('admin' => $this->admin, 'std_serial' => $serial[$i],
                                    'attendence_date' => $attendence_date, 'class_teacher' => $class_teacher, 'attendence_status' => $attendence_status);
                            }
                            $this->db->trans_start();
                            $this->db->insert_batch('attendence_sheet', $student_info);
                            $message = "ATTENDENCE SHEET UPDATED!";
                            $this->db->trans_complete();
//print_r($student_info);
                        } else {
                            if ($option == "trash") {
                                $serial = $_POST['serial'];
                                for ($i = 0; $i < count($serial); $i++) {
                                    $student_info[$i] = array('serial' => $serial[$i], 'status' => 'old');
                                }
                                $this->db->update_batch('student_info', $student_info, 'serial');
                                $message = "trashed";
                            }
                        }
                    }
                }
                if ($message != "trashed") {
                    echo"<div class='alert alert-warning'>$message
                    <button type ='button' class = 'close' data-dismiss = 'alert' aria-label = 'Close'><span aria-hidden = 'true'>&times;
                    </span></button>
                    </div>";
                } else {
                    echo $message;
                }
            }
// print_r($student_info);
        }

        public function addtocurrentstudent() {
            $student_info = array();
            if (isset($_POST['serial'])) {
                $serial = $_POST['serial'];
                for ($i = 0; $i < count($serial); $i++) {
                    $student_info[$i] = array('serial' => $serial[$i], 'status' => 'current');
                }
                $this->db->update_batch('student_info', $student_info, 'serial');
                echo"recovered";
// print_r($student_info);
            }
        }

        public function student($serial = "") {
            $curdate = date("Y-m-01");
            $query1 = "SELECT * FROM `student_info` where serial=$serial and admin=$this->admin";
            $query2 = "select * from parent_info as t1
                 where t1.id=(SELECT t2.id FROM `student_info` as t2 where t2.serial=$serial) and t1.admin=$this->admin";
            $query3 = "SELECT * FROM `student_fee` where admin=$this->admin and std_serial=$serial";
            $query4 = "SELECT * FROM `student_cost` where admin=$this->admin and std_serial=$serial";

            $query5 = $this->get_attendence_query($curdate, $serial, 'attendence_sheet', 'std_serial');
            $query6 = "SELECT * FROM `std_exam_info` where admin=$this->admin and student_serial=$serial";
            $query7 = "SELECT t1.serial,t1.course_title,t1.course_name,t2.serial as std_course_serial FROM `course` as t1,student_course as t2
                    where t1.serial=t2.course_serial and t1.admin=t2.admin and t2.admin=$this->admin and t2.student_serial=$serial";

            $data1 = $this->mstudent->mget_student_info($query1);
            if (isset($data1['std_serial'])) {
                $data2 = $this->mstudent->mget_partent_info($query2);
                $data3 = $this->mstudent->get_student_fee($query3);
                $data4 = $this->mstudent->get_student_cost($query4);
                $data5 = $this->mstudent->get_attendence($query5);
                $data6 = $this->mstudent->get_exam_info($query6);
                $data7 = $this->mstudent->get_student_course($query7);
                $data8 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='student' && cost_type='dr'", array("studentdr", 'option'));
                $data9 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='student' && cost_type='cr'", array("studentcr", 'option'));
                $data10 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='payment_method' && cost_type='pm'", array("payment_method", 'option'));
                $data = array_merge($data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10);

                $this->view_page->admin_page('v_student', $data);
            } else {
                $this->view_page->admin_page('page_not_found', "");
            }
        }

        private function get_attendence_query($date, $serial, $table = "", $column = "") {
            $date1 = strtotime($date);
            $date1 = date("Y-m-d", $date1);
            $date2 = new DateTime($date1);
            $date2 = $date2->format("Y-m-t");
            $query = "SELECT attendence_date as date_time, day(attendence_date) as date,attendence_status as status FROM $table
                   where attendence_date between '$date1' and '$date2' and admin=$this->admin and $column=$serial";
            return $query;
//echo "$date1,$date2";
        }

        public function addteacher() {
            $data1 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='teacher'", array('teachertype', 'type'));
            $this->view_page->admin_page('v_addteacher', $data1);
        }

        public function add_new_teacher() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $basic_info = array('admin' => $this->admin, 'tid' => $data['tid'], 'tname' => $data['tname'], 'tbdate' => $data['tbdate'], 'tjdate' => $data['tjdate'], 'tfname' => $data['tfname'],
                'tmname' => $data['tmname'], 'tphone' => $data['tphone'], 'temail' => $data['temail'], 'tsallary' => $data['tsallary'], 'taddress' => $data['taddress'],
                'tposition' => $data['tposition'], 'prev_exp' => $data['prev_exp'], 'about' => $data['about']);

            /*  $ssc_info = array('admin' => $this->admin, 'tid' => $data['tid'], 'edu_type' => 'ssc', 'pass_year' => $data['ssc_pass_year'], 'specialization' => $data['ssc_specialization'], 'institute' => $data['ssc_institute'],
              'cgpa' => $data['ssc_cgpa']);
              $hsc_info = array('admin' => $this->admin, 'tid' => $data['tid'], 'edu_type' => 'hsc', 'pass_year' => $data['hsc_pass_year'], 'specialization' => $data['hsc_specialization'], 'institute' => $data['hsc_institute'],
              'cgpa' => $data['hsc_cgpa']);
              $bsc_info = array('admin' => $this->admin, 'tid' => $data['tid'], 'edu_type' => 'bsc', 'pass_year' => $data['bsc_pass_year'], 'specialization' => $data['bsc_specialization'], 'institute' => $data['bsc_institute'],
              'cgpa' => $data['bsc_cgpa']); */
            if (isset($data['qualification'])) {
                for ($i = 0; $i < count($data['qualification']); $i++) {
                    $edu_info[$i] = array('admin' => $this->admin, 'tid' => $data['tid'], 'edu_type' => $data['qualification'][$i], 'pass_year' => $data['passing_year'][$i],
                        'specialization' => $data['specialization'][$i], 'institute' => $data['institute'][$i], 'cgpa' => $data['gpa'][$i]);
                }
                $this->db->trans_start();
                $this->db->insert('teacher_basic_info', $basic_info);
                $this->db->insert_batch('teacher_edu', $edu_info);
//print_r($basic_info);
                echo"INSERTED!";
                $this->db->trans_complete();
            }
        }

        public function currentteacher() {
            $query1 = "SELECT * FROM `teacher_basic_info` where admin=$this->admin and status='current'";
            $data1 = $this->mstudent->get_current_teacher($query1);
            $data2 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='teacher' && cost_type='cr'", array("teachercr", 'option'));
            $data3 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='teacher' && cost_type='dr'", array("teacherdr", 'option'));
            $data4 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='teacher'", array('teachertype', 'type'));
            $data5 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='payment_method' && cost_type='pm'", array("payment_method", 'option'));
            $data = array_merge($data1, $data2, $data3, $data4, $data5);
            $this->view_page->admin_page('v_currentteacher', $data);
        }

        public function oldteacher() {
            $query = "SELECT * FROM `teacher_basic_info` where admin=$this->admin and status='old'";
            $data1 = $this->mstudent->get_current_teacher($query);
            $data2 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='teacher'", array('teachertype', 'type'));
            $data = array_merge($data1, $data2);
            $this->view_page->admin_page('v_oldteacher', $data);
        }

        public function filter_current_teacher($type = "", $status = 'current') {
            $type = str_replace("%20", " ", $type);
            if ($type == "ALL") {
                $query = "SELECT * FROM `teacher_basic_info` where admin=1 and status='$status'";
            } else {
                $query = "SELECT * FROM `teacher_basic_info` where admin=1 and tposition='$type' and status='$status'";
            }
            $data = $this->mstudent->get_current_teacher($query);
            $data['asset_url'] = $this->asset_url;
            $this->load->view('vadmin/v_filter_current_teacher_table', $data);
        }

        public function add_teacher_info() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $teacher_info = array();
            $message = "";
            if (isset($_POST['serial'])) {
                if ($data['option'] == "add_sallary") {
                    for ($i = 0; $i < count($data['serial']); $i++) {
                        $teacher_info[$i] = array('admin' => $this->admin, 'teach_serial' => $data['serial'][$i], 'sallary_date' => $data['sallary_date'], 'sallary_for' => $data['sallary_for'],
                            'sallary_ammount' => $data['sallary_ammount'], 'sallary_due_amount' => $data['sallary_due_amount'], 'payment_method' => $data['sallary_payment_method']);
                    }
                    $this->db->trans_start();
                    $this->db->insert_batch('paidtoteacher', $teacher_info);
                    $message = "SALARY PAID SUCCESSFULLY!";
                    $this->db->trans_complete();
                } else {
                    if ($data['option'] == "add_loan") {
                        for ($i = 0; $i < count($data['serial']); $i++) {
                            $teacher_info[$i] = array('admin' => $this->admin, 'teach_serial' => $data['serial'][$i], 'loan_date' => $data['loan_date'], 'loan_for' => $data['loan_for'],
                                'loan_ammount' => $data['loan_ammount'], 'loan_due_amount' => $data['loan_due_amount'], 'payment_method' => $data['loan_payment_method']);
                        }
                        $this->db->trans_start();
                        $this->db->insert_batch('loanfromteacher', $teacher_info);
                        $message = "PAID SUCCESSFULLY!";
                        $this->db->trans_complete();
                    } else {
                        if ($data['option'] == "add_attendence") {
                            for ($i = 0; $i < count($data['serial']); $i++) {
                                $teacher_info[$i] = array('admin' => $this->admin, 'teach_serial' => $data['serial'][$i], 'attendence_date' => $data['attendence_date'], 'attendence_status' => $data['attendence_status']);
                            }
                            $this->db->trans_start();
                            $this->db->insert_batch('attendenceteacher', $teacher_info);
                            $message = "ATTENDANCE SHEET UPDATED SUCCESSFULLY!";
                            $this->db->trans_complete();
                        } else {
                            if ($data['option'] == "trash") {
                                $serial = $data['serial'];
                                for ($i = 0; $i < count($serial); $i++) {
                                    $teacher_info[$i] = array('serial' => $serial[$i], 'status' => 'old');
                                }
// print_r($teacher_info);
                                $this->db->update_batch('teacher_basic_info', $teacher_info, 'serial');
                                $message = "trashed";
                            }
                        }
                    }
                }
                if ($message != "trashed") {
                    echo"<div class='alert alert-warning'>$message
                    <button type ='button' class = 'close' data-dismiss = 'alert' aria-label = 'Close'><span aria-hidden = 'true'>&times;
                    </span></button>
                    </div>";
                } else {
                    echo $message;
                }
            }
//print_r($teacher_info);
        }

        public function addtocurrentteacher() {
            $teacher_info = array();
            if (isset($_POST['serial'])) {
                $serial = $_POST['serial'];
                for ($i = 0; $i < count($serial); $i++) {
                    $teacher_info[$i] = array('serial' => $serial[$i], 'status' => 'current');
                }
                $this->db->update_batch('teacher_basic_info', $teacher_info, 'serial');
                echo"recovered";
            }
        }

        public function teacher($serial) {
            $curdate = date("Y-m-01");
            $query1 = "SELECT * FROM `teacher_basic_info` where admin=$this->admin and serial=$serial";
            $query2 = "SELECT * FROM `teacher_edu` where tid=(SELECT t2.tid FROM `teacher_basic_info` as t2 where t2.serial=$serial) and admin=$this->admin";
            $query3 = "SELECT * FROM `paidtoteacher` where teach_serial=$serial and admin=$this->admin";
            $query4 = "SELECT * FROM `loanfromteacher` where teach_serial=$serial and admin=$this->admin";
            $query5 = $this->get_attendence_query($curdate, $serial, 'attendenceteacher', 'teach_serial');

            $data1 = $this->mstudent->get_current_teacher($query1);
            if (isset($data1['tserial'])) {
                $data2 = $this->mstudent->get_teacher_edu_info($query2);
                $data3 = $this->mstudent->get_teacher_sallary_info($query3);
                $data4 = $this->mstudent->get_loan_from_teacher($query4);
                $data5 = $this->mstudent->get_attendence($query5);
                $data6 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='teacher'", array('teachertype', 'type'));
                $data7 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='teacher' && cost_type='cr'", array("teachercr", 'option'));
                $data8 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='teacher' && cost_type='dr'", array("teacherdr", 'option'));
                $data9 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='payment_method' && cost_type='pm'", array("payment_method", 'option'));
                $data = array_merge($data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9);
                $this->view_page->admin_page('v_teacher', $data);
            } else {
                $this->view_page->admin_page('page_not_found', "");
            }
        }

        public function update_teacher_basic_info() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $this->db->update('teacher_basic_info', $data, array('admin' => $this->admin, 'serial' => $data['serial']));
            echo"<div class='alert alert-warning'>
                    <strong>SUCCESS!</strong>You Have Successfully Updated Teacher's Info <i class='fa fa-fw fa-smile-o'></i>.
                </div>";
        }

        public function update_student_basic_info() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $this->db->update('student_info', $data, array('admin' => $this->admin, 'serial' => $data['serial']));
            echo"<div class='alert alert-warning'>
                    <strong>SUCCESS!</strong>You Have Successfully Updated Student's Info <i class='fa fa-fw fa-smile-o'></i>.
                </div>";
        }

        public function update_parent_info() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $this->db->update('parent_info', $data, array('admin' => $this->admin, 'serial' => $data['serial']));
            echo"<div class='alert alert-warning'>
                    <strong>SUCCESS!</strong>You Have Successfully Updated Parents Info <i class='fa fa-fw fa-smile-o'></i>.
                </div>";
        }

        public function student_attendence_sheet() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $curdate = "{$data['month']}-01";
            $query = $this->get_attendence_query($curdate, $data['std_serial'], 'attendence_sheet', 'std_serial');
            $info = $this->mstudent->get_attendence($query);
            $info['today'] = $curdate;
            $info['std_serial'] = $data['std_serial'];
            $this->load->view('vadmin/v_student_attendence', $info);
//print_r($info);
        }

        public function teacher_attendence_sheet() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $curdate = "{$data['month']}-01";
            $query = $this->get_attendence_query($curdate, $data['tserial'], 'attendenceteacher', 'teach_serial');
            $info = $this->mstudent->get_attendence($query);
            $info['today'] = $curdate;
            $info['tserial'] = $data['tserial'];
            $this->load->view('vadmin/v_teacher_attendence', $info);
// print_r($data);
        }

        public function addstuff() {
            $data1 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='stuff'", array('stufftype', 'type'));
            $this->view_page->admin_page('v_addstuff', $data1);
        }

        public function add_new_stuff() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $basic_info = array('admin' => $this->admin, 'stid' => $data['stid'], 'stname' => $data['stname'], 'stbdate' => $data['stbdate'], 'stjdate' => $data['stjdate'], 'stfname' => $data['stfname'],
                'stmname' => $data['stmname'], 'stphone' => $data['stphone'], 'stemail' => $data['stemail'], 'stsallary' => $data['stsallary'], 'staddress' => $data['staddress'],
                'stposition' => $data['stposition'], 'prev_exp' => $data['prev_exp'], 'about' => $data['about']);

            /*  $ssc_info = array('admin' => $this->admin, 'stid' => $data['stid'], 'edu_type' => 'ssc', 'pass_year' => $data['ssc_pass_year'], 'specialization' => $data['ssc_specialization'], 'institute' => $data['ssc_institute'],
              'cgpa' => $data['ssc_cgpa']);
              $hsc_info = array('admin' => $this->admin, 'stid' => $data['stid'], 'edu_type' => 'hsc', 'pass_year' => $data['hsc_pass_year'], 'specialization' => $data['hsc_specialization'], 'institute' => $data['hsc_institute'],
              'cgpa' => $data['hsc_cgpa']);
              $bsc_info = array('admin' => $this->admin, 'stid' => $data['stid'], 'edu_type' => 'bsc', 'pass_year' => $data['bsc_pass_year'], 'specialization' => $data['bsc_specialization'], 'institute' => $data['bsc_institute'],
              'cgpa' => $data['bsc_cgpa']); */

            if (isset($data['qualification'])) {
                for ($i = 0; $i < count($data['qualification']); $i++) {
                    $edu_info[$i] = array('admin' => $this->admin, 'stid' => $data['stid'], 'edu_type' => $data['qualification'][$i], 'pass_year' => $data['passing_year'][$i],
                        'specialization' => $data['specialization'][$i], 'institute' => $data['institute'][$i], 'cgpa' => $data['gpa'][$i]);
                }
                $this->db->trans_start();
                $this->db->insert('stuff_basic_info', $basic_info);
                $this->db->insert_batch('stuff_edu', $edu_info);
                echo"INSERTED!";
                $this->db->trans_complete();
            }
            /* $this->db->trans_start();
              $this->db->insert('stuff_basic_info', $basic_info);
              $this->db->insert('stuff_edu', $ssc_info);
              $this->db->insert('stuff_edu', $hsc_info);
              $this->db->insert('stuff_edu', $bsc_info);
              echo"INSERTED!";
              $this->db->trans_complete(); */
//print_r($data);
        }

        public function currentstuff() {
            $query = "SELECT * FROM `stuff_basic_info` where admin=$this->admin and status='current'";
            $data1 = $this->mstudent->get_current_stuff($query);
            $data2 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='stuff' && cost_type='cr'", array("stuffcr", 'option'));
            $data3 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='stuff' && cost_type='dr'", array("stuffdr", 'option'));
            $data4 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='stuff'", array('stufftype', 'type'));
            $data5 = $this->mstudent->get_person_cost("SELECT * FROM `person_cost_type` where admin=$this->admin && person='payment_method' && cost_type='pm'", array("payment_method", 'option'));
            $data = array_merge($data1, $data2, $data3, $data4, $data5);
            $this->view_page->admin_page('v_currentstuff', $data);
        }

        public function oldstuff() {
            $query = "SELECT * FROM `stuff_basic_info` where admin=$this->admin and status='old'";
            $data1 = $this->mstudent->get_current_stuff($query);
            $data2 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='stuff'", array('stufftype', 'type'));
            $data = array_merge($data1, $data2);
            $this->view_page->admin_page('v_oldstuff', $data);
        }

        public function filter_current_stuff($type, $status = 'current') {
            $type = str_replace("%20", " ", $type);
            if ($type == "ALL") {
                $query = "SELECT * FROM `stuff_basic_info` where admin=$this->admin and status='$status'";
            } else {
                $query = "SELECT * FROM `stuff_basic_info` where admin=$this->admin and stposition='$type' and status='$status'";
            }
            $data = $this->mstudent->get_current_stuff($query);
            $data['asset_url'] = $this->asset_url;
            $this->load->view('vadmin/v_filter_current_stuff_table', $data);
        }

        public function add_stuff_info() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $message = "";
//print_r($data);
            $stuff_info = array();
            if (isset($_POST['serial'])) {
                if ($data['option'] == "add_sallary") {
                    for ($i = 0; $i < count($data['serial']); $i++) {
                        $stuff_info[$i] = array('admin' => $this->admin, 'st_serial' => $data['serial'][$i], 'sallary_date' => $data['sallary_date'], 'sallary_for' => $data['sallary_for'],
                            'sallary_ammount' => $data['sallary_ammount'], 'sallary_due_amount' => $data['sallary_due_amount'], 'payment_method' => $data['sallary_payment_method']);
                    }
                    $this->db->trans_start();
                    $this->db->insert_batch('paidtostuff', $stuff_info);
                    $message = "INSERTED SUCCESSFULLY!";
                    $this->db->trans_complete();
                } else {
                    if ($data['option'] == "add_pay") {
                        for ($i = 0; $i < count($data['serial']); $i++) {
                            $stuff_info[$i] = array('admin' => $this->admin, 'st_serial' => $data['serial'][$i], 'pay_date' => $data['pay_date'], 'pay_for' => $data['pay_for'],
                                'pay_ammount' => $data['pay_ammount'], 'pay_due_amount' => $data['pay_due_amount'], 'payment_method' => $data['pay_payment_method']);
                        }
                        $this->db->trans_start();
                        $this->db->insert_batch('paidbystuff', $stuff_info);
                        $message = "INSERTED SUCCESSFULLY!";
                        $this->db->trans_complete();
// print_r($stuff_info);
                    } else {
                        if ($data['option'] == "add_attendence") {
                            for ($i = 0; $i < count($data['serial']); $i++) {
                                $stuff_info[$i] = array('admin' => $this->admin, 'st_serial' => $data['serial'][$i], 'attendence_date' => $data['attendence_date'], 'attendence_status' => $data['attendence_status']);
                            }
                            $this->db->trans_start();
                            $this->db->insert_batch('attendencestuff', $stuff_info);
                            $message = "ATTENDANCE SHEET UPDATED SUCCESSFULLY!";
                            $this->db->trans_complete();
                        } else {
                            if ($data['option'] == "trash") {
                                $serial = $data['serial'];
                                for ($i = 0; $i < count($serial); $i++) {
                                    $stuff_info[$i] = array('serial' => $serial[$i], 'status' => 'old');
                                }
                                $this->db->update_batch('stuff_basic_info', $stuff_info, 'serial');
                                $message = "trashed";
                            }
                        }
                    }
                }
                if ($message != "trashed") {
                    echo"<div class='alert alert-warning'>$message
                    <button type ='button' class = 'close' data-dismiss = 'alert' aria-label = 'Close'><span aria-hidden = 'true'>&times;
                    </span></button>
                    </div>";
                } else {
                    echo $message;
                }
            }
// print_r(_info);
        }

        public function stuff($serial = "") {
            $curdate = date("Y-m-01");
            $query1 = "SELECT * FROM `stuff_basic_info` where admin=$this->admin and serial=$serial";
            $query2 = "SELECT * FROM `stuff_edu` where stid=(SELECT t2.stid FROM `stuff_basic_info` as t2 where t2.serial=$serial) and admin=$this->admin";
            $query3 = "SELECT * FROM `paidtostuff` where st_serial=$serial and admin=$this->admin";
            $query4 = "SELECT * FROM `paidbystuff` where st_serial=$serial and admin=$this->admin";
            $query5 = $this->get_attendence_query($curdate, $serial, 'attendencestuff', 'st_serial');

            $data1 = $this->mstudent->get_current_stuff($query1);
            if (isset($data1['stserial'])) {
                $data2 = $this->mstudent->get_stuff_edu_info($query2);
                $data3 = $this->mstudent->get_stuff_sallary_info($query3);
                $data4 = $this->mstudent->get_paid_by_stuff($query4);
                $data5 = $this->mstudent->get_attendence($query5);
                $data6 = $this->mstudent->get_person_cost("SELECT * FROM `person_occupation_type` where admin=$this->admin and person='stuff'", array('stufftype', 'type'));
                $data = array_merge($data1, $data2, $data3, $data4, $data5, $data6);
                $this->view_page->admin_page('v_stuff', $data);
            }
            else{
                $this->view_page->admin_page('page_not_found', "");
            }
        }

        public function stuff_attendence_sheet() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $curdate = "{$data['month']}-01";
            $query = $this->get_attendence_query($curdate, $data['stserial'], 'attendencestuff', 'st_serial');
            $info = $this->mstudent->get_attendence($query);
            $info['today'] = $curdate;
            $info['stserial'] = $data['stserial'];
            $this->load->view('vadmin/v_stuff_attendence', $info);
// print_r($data);
        }

        public function update_stuff_basic_info() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $this->db->update('stuff_basic_info', $data, array('admin' => $this->admin, 'serial' => $data['serial']));
            echo"<div class='alert alert-warning'>
                    <strong>SUCCESS!</strong>You Have Successfully Updated Stuff's Info <i class='fa fa-fw fa-smile-o'></i>.
                </div>";
        }

        public function add_to_current_stuff() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if (isset($data['serial'])) {
                $serial = $data['serial'];
                for ($i = 0; $i < count($serial); $i++) {
                    $stuff_info[$i] = array('serial' => $serial[$i], 'status' => 'current');
                }
                $this->db->update_batch('stuff_basic_info', $stuff_info, 'serial');
                echo"recovered";
            }
        }

        public function update_std_pro_pic() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $path = "img/profile/student/";

            if (isset($data['serial'])) {
                if ((($_FILES["image"]["type"] == "image/gif")
                        || ($_FILES["image"]["type"] == "image/jpeg")
                        || ($_FILES["image"]["type"] == "image/pjpeg")
                        || ($_FILES["image"]["type"] == "image/png"))) {
//$img=$data['serial'].".jpg";
                    $new_image_name = "{$data['serial']}.jpg"; //".jpg"; //base64_encode($_COOKIE['id']).".jpg";
// $new_image_name = "20.jpg";
                    if ($_FILES["image"]["error"] > 0) {
                        echo "Return Code: " . $_FILES["image"]["error"] . "<br />";
                    } else {
//$data = array('orginal_name' => $_FILES['image']['name']);
//$this->db->update('project_image', $data, array('image_id' => $image_id));
                        $temp_image = move_uploaded_file($_FILES["image"]["tmp_name"], $path . $_FILES["image"]["name"]);
                        $url = $this->asset_url . "img/profile/student/$new_image_name";
                        if (rename($path . $_FILES['image']['name'], $path . $new_image_name) && $temp_image == true) {
                            echo"<img src='$url' height='200px' width='100%'/>";
                        }
                    }
                }
            }
        }

        public function update_tech_pro_pic() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $path = "img/profile/teacher/";
            if (isset($data['serial'])) {
                if ((($_FILES["image"]["type"] == "image/gif")
                        || ($_FILES["image"]["type"] == "image/jpeg")
                        || ($_FILES["image"]["type"] == "image/pjpeg")
                        || ($_FILES["image"]["type"] == "image/png"))) {

                    $new_image_name = "{$data['serial']}.jpg"; //base64_encode($_COOKIE['id']).".jpg";
                    if ($_FILES["image"]["error"] > 0) {
                        echo "Return Code: " . $_FILES["image"]["error"] . "<br />";
                    } else {
//$data = array('orginal_name' => $_FILES['image']['name']);
//$this->db->update('project_image', $data, array('image_id' => $image_id));
                        $temp_image = move_uploaded_file($_FILES["image"]["tmp_name"], $path . $_FILES["image"]["name"]);
                        $url = $this->asset_url . "img/profile/teacher/$new_image_name";
                        if (rename($path . $_FILES['image']['name'], $path . $new_image_name) && $temp_image == true) {
                            echo"<img src='$url' height='200px' width='100%'/>";
                        }
                    }
                }
            }
        }

        public function update_stuff_pro_pic() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $path = "img/profile/stuff/";
            if (isset($data['serial'])) {
                if ((($_FILES["image"]["type"] == "image/gif")
                        || ($_FILES["image"]["type"] == "image/jpeg")
                        || ($_FILES["image"]["type"] == "image/pjpeg")
                        || ($_FILES["image"]["type"] == "image/png"))) {

                    $new_image_name = "{$data['serial']}.jpg"; //base64_encode($_COOKIE['id']).".jpg";
                    if ($_FILES["image"]["error"] > 0) {
                        echo "Return Code: " . $_FILES["image"]["error"] . "<br />";
                    } else {
//$data = array('orginal_name' => $_FILES['image']['name']);
//$this->db->update('project_image', $data, array('image_id' => $image_id));
                        $temp_image = move_uploaded_file($_FILES["image"]["tmp_name"], $path . $_FILES["image"]["name"]);
                        $url = $this->asset_url . "img/profile/stuff/$new_image_name";
                        if (rename($path . $_FILES['image']['name'], $path . $new_image_name) && $temp_image == true) {
                            echo"<img src='$url' height='200px' width='100%'/>";
                        }
                    }
                }
            }
        }

        public function classroutine() {
            $class = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
//$class=array("13");
            $i = 0;
            $data = array();
            foreach ($class as $class) {
                $data['class_routine'][$i] = $this->mstudent->mget_class_routine($class, $this->admin, 'sat');
                $data['class'][$i] = $class;
                $i++;
            }
            $this->view_page->admin_page('v_classroutine', $data);
        }

        public function create_class_routine() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();

            $query = "SELECT * FROM `classroutine` where admin=$this->admin and class='{$data['class']}'";
            if (isset($data['time']) && $this->mstudent->check_class_routine($query) == 0) {
                $class = $data['class'];
                $info = array();
                $day = array('sat', 'sun', 'mon', 'tue', 'wed', 'thr', 'fri');
                $time = count($data['time']);
                $i = 0;
                $t = 0;
                for ($i = 0; $i < $time * 1; $i++) {
                    $info[$i] = array('admin' => $this->admin, 'class' => $class, 'time' => $data['time'][$t], 'period' => $data['period'][$t], 'teacher' => $data['teacher'][$i], 'day' => 'sat', 'sub' => $data['subject'][$i]);
                    $t++;
                }
                $t = 0;
                for ($j = $i; $j < 2 * $time; $j++) {
                    $info[$j] = array('admin' => $this->admin, 'class' => $class, 'time' => $data['time'][$t], 'period' => $data['period'][$t], 'teacher' => $data['teacher'][$j], 'day' => 'sun', 'sub' => $data['subject'][$j]);
                    $t++;
                }
                $t = 0;
                for ($k = $j; $k < 3 * $time; $k++) {
                    $info[$k] = array('admin' => $this->admin, 'class' => $class, 'teacher' => $data['teacher'][$k], 'time' => $data['time'][$t], 'period' => $data['period'][$t], 'day' => 'mon', 'sub' => $data['subject'][$k]);
                    $t++;
                }
                $t = 0;
                for ($l = $k; $l < 4 * $time; $l++) {
                    $info[$l] = array('admin' => $this->admin, 'class' => $class, 'teacher' => $data['teacher'][$l], 'time' => $data['time'][$t], 'period' => $data['period'][$t], 'day' => 'tue', 'sub' => $data['subject'][$l]);
                    $t++;
                }
                $t = 0;
                for ($m = $l; $m < 5 * $time; $m++) {
                    $info[$m] = array('admin' => $this->admin, 'class' => $class, 'teacher' => $data['teacher'][$m], 'time' => $data['time'][$t], 'period' => $data['period'][$t], 'day' => 'wed', 'sub' => $data['subject'][$m]);
                    $t++;
                }
                $t = 0;
                for ($n = $m; $n < 6 * $time; $n++) {
                    $info[$n] = array('admin' => $this->admin, 'class' => $class, 'teacher' => $data['teacher'][$n], 'time' => $data['time'][$t], 'period' => $data['period'][$t], 'day' => 'thr', 'sub' => $data['subject'][$n]);
                    $t++;
                }
                $t = 0;
                for ($o = $n; $o < 7 * $time; $o++) {
                    $info[$o] = array('admin' => $this->admin, 'class' => $class, 'teacher' => $data['teacher'][$o], 'time' => $data['time'][$t], 'period' => $data['period'][$t], 'day' => 'fri', 'sub' => $data['subject'][$o]);
                    $t++;
                }
                $this->db->trans_start();
                $this->db->insert_batch('classroutine', $info);
                echo"SUCCESS!";
                $this->db->trans_complete();
// print_r($info['sun']);
//echo"<br>";
            } else {
                echo"FAILED";
            }

//print_r($info['sun']);
        }

        public function get_class_routine($class = "00") {
            $day = array("sat", "sun", "mon", "tue", "wed", "thr", "fri");
            $data = array();
            $data = $this->mstudent->mget_class_routine($class, $this->admin, 'sat');
            $this->load->view('vadmin/v_specific_classroutine', $data);
        }

        public function transaction() {
            $query1 = "SELECT * FROM `transaction` where admin=$this->admin";
            $query2 = "SELECT sum(t1.fee_amount) as std_dr,
                 (SELECT sum(t2.pay_amount) FROM `student_cost` as t2 where t2.admin=$this->admin) as std_cr,
                 (SELECT sum(t3.loan_ammount) FROM `loanfromteacher` as t3 where t3.admin=$this->admin)as tec_dr,
                 (SELECT sum(t4.sallary_ammount) FROM `paidtoteacher` as t4 where t4.admin=$this->admin) as tec_cr,
                 (SELECT sum(t5.pay_ammount) FROM `paidbystuff` as t5 where t5.admin=$this->admin) as stf_dr,
                 (SELECT sum(t6.sallary_ammount)  FROM `paidtostuff` as t6 where t6.admin=$this->admin) as stf_cr
                 FROM `student_fee` as t1 where t1.admin=$this->admin";
            $data1 = $this->mstudent->get_transaction_data($query1);
            $data2 = $this->mstudent->get_other_transaction_data($query2);
            $data = array_merge($data1, $data2);
//print_r($data2);
            /* $query2="select sum(t1.fee_amount) as dr,(select sum(t2.pay_amount) from student_cost as t2 where t2.admin=$ths->admin) as cr
              from student_fee as t1 where t1.admin=$this->admin";
              //$data2=$this->madmin->get_sts_transaction($query2,array('std_dr','std_cr')); */
            $this->view_page->admin_page('v_transaction', $data);
        }

        public function import_trans() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            if ($data['dr_cr'] == "DR") {
                $info = array('admin' => $this->admin, 'tn_date' => $data['tn_date'], 'description' => $data['description'], 'dr' => $data['ammount'], 'cr' => 0);
            } else {
                $info = array('admin' => $this->admin, 'tn_date' => $data['tn_date'], 'description' => $data['description'], 'cr' => $data['ammount'], 'dr' => 0);
            }
            if ($this->db->insert('transaction', $info)) {
                $query1 = "SELECT * FROM `transaction` where admin=$this->admin";
                $query2 = "SELECT sum(t1.fee_amount) as std_dr,
                 (SELECT sum(t2.pay_amount) FROM `student_cost` as t2 where t2.admin=$this->admin) as std_cr,
                 (SELECT sum(t3.loan_ammount) FROM `loanfromteacher` as t3 where t3.admin=$this->admin)as tec_dr,
                 (SELECT sum(t4.sallary_ammount) FROM `paidtoteacher` as t4 where t4.admin=$this->admin) as tec_cr,
                 (SELECT sum(t5.pay_ammount) FROM `paidbystuff` as t5 where t5.admin=$this->admin) as stf_dr,
                 (SELECT sum(t6.sallary_ammount)  FROM `paidtostuff` as t6 where t6.admin=$this->admin) as stf_cr
                 FROM `student_fee` as t1 where t1.admin=$this->admin";
                $info1 = $this->mstudent->get_transaction_data($query1);
                $info2 = $this->mstudent->get_other_transaction_data($query2);
                $info3['date1'] = "TILL";
                $info3['date2'] = "DAY";
                $info = array_merge($info1, $info2, $info3);
                $this->load->view('vadmin/v_filter_trans_tbl', $info);
            }

// print_r($info);
        }

        public function filter_trans_tbl() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
//print_r($data);
            $query1 = "SELECT * FROM `transaction` where admin=$this->admin and tn_date between '{$data['date1']}' and '{$data['date2']}'";
            $query2 = "SELECT sum(t1.fee_amount) as std_dr,
(SELECT sum(t2.pay_amount) FROM `student_cost` as t2 where t2.admin=$this->admin and t2.pay_date between '{$data['date1']}' and '{$data['date2']}') as std_cr,
(SELECT sum(t3.loan_ammount) FROM `loanfromteacher` as t3 where t3.admin=$this->admin and t3.loan_date between '{$data['date1']}' and '{$data['date2']}')as tec_dr,
(SELECT sum(t4.sallary_ammount) FROM `paidtoteacher` as t4 where t4.admin=$this->admin and t4.sallary_date between '{$data['date1']}' and '{$data['date2']}') as tec_cr,
(SELECT sum(t5.pay_ammount) FROM `paidbystuff` as t5 where t5.admin=$this->admin and t5.pay_date between '{$data['date1']}' and '{$data['date2']}') as stf_dr,
(SELECT sum(t6.sallary_ammount)  FROM `paidtostuff` as t6 where t6.admin=$this->admin and t6.sallary_date between '{$data['date1']}' and '{$data['date2']}') as stf_cr
FROM `student_fee` as t1 where t1.admin=$this->admin and t1.fee_date between '{$data['date1']}' and '{$data['date2']}'";

            $info1 = $this->mstudent->get_transaction_data($query1);
            $info2 = $this->mstudent->get_other_transaction_data($query2);
            $info3['date1'] = $data['date1'];
            $info3['date2'] = $data['date2'];
            $info = array_merge($info1, $info2, $info3);
            $this->load->view('vadmin/v_filter_trans_tbl', $info);
        }

        public function courses() {
            $class = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
            $i = 0;
            $data = array();
            foreach ($class as $class) {
                $data['class_course'][$i] = $this->mstudent->get_course_data("SELECT * FROM `course` where admin=$this->admin and class='$class'");
                $data['class'][$i] = $class;
                $i++;
            }
            $this->view_page->admin_page('v_courses', $data);
        }

        public function add_course() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $info = array();
            for ($i = 0; $i < count($data['ctitle']); $i++) {
                $info[$i] = array('admin' => $this->admin, 'class' => $data['cclass'][$i], 'course_title' => $data['ctitle'][$i], 'course_name' => $data['cname'][$i]);
            }
            $this->db->trans_start();
            $this->db->insert_batch('course', $info);
            echo"INSERTED!";
            $this->db->trans_complete();
        }

        public function get_course($class = "") {
            $query = "SELECT * FROM `course` where admin=$this->admin and class='$class'";
            $data = $this->mstudent->get_course_data($query);
            $this->load->view('vadmin/v_filter_course', $data);
        }

        public function studentcourse($serial = "1") {
            $query1 = "SELECT * FROM `student_info` where serial=$serial and admin=$this->admin";
            $data1 = $this->mstudent->mget_student_info($query1);
            $query2 = "SELECT t1.serial,t1.course_title,t1.course_name,t2.serial as std_course_serial FROM `course` as t1,student_course as t2
                    where t1.serial=t2.course_serial and t1.admin=t2.admin and t2.admin=$this->admin and t2.student_serial=$serial";
            $data2 = $this->mstudent->get_student_course($query2);
            $data = array_merge($data1, $data2);
            $this->view_page->admin_page('v_student_course', $data);
        }

        public function choose_course($class = "") {
            $query = "SELECT * FROM `course` where admin=$this->admin and class='$class'";
            $data = $this->mstudent->get_course_data($query);
            $this->load->view('vadmin/v_choose_course', $data);
        }

        public function select_course() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $cserial = array();
            if (isset($data['course_serial'])) {
                for ($i = 0; $i < count($data['course_serial']); $i++) {
                    $serial = $data['course_serial'][$i];
                    $title = $data["course_title$serial"];
                    $name = $data["course_name$serial"];
                    echo"<tr class='warning'><td><input type='checkbox' name='final_course_selection[]' value='$serial'/></td><td>$title</td><td>$name</td></tr>";
                }
            }
        }

        public function submitstudentcourse() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
// print_r($data);
//$serial = $data['final_course_selection'];
//print_r($serial);
            if (isset($data['final_course_selection']) && isset($data['student_serial'])) {
                $query = "SELECT t1.serial,t1.course_title,t1.course_name,t2.serial as std_course_serial FROM `course` as t1,student_course as t2
                    where t1.serial=t2.course_serial and t1.admin=t2.admin and t2.admin=$this->admin and t2.student_serial={$data['student_serial']}";

                $already_course = $this->mstudent->get_student_course($query);
                if (isset($already_course['std_course_serial'])) {
                    $old_course = $already_course['std_course_serial'];
                } else {
                    $old_course = array();
                }
                $selected_course = array_unique($data['final_course_selection']);
                $intersec = array_intersect($old_course, $selected_course);

                $course_serial = array_values(array_diff($selected_course, $intersec));

//print_r($course_serial);

                for ($i = 0; $i < count($course_serial); $i++) {
                    $info[$i] = array('admin' => $this->admin, 'student_serial' => $data['student_serial'], 'course_serial' => $course_serial[$i]);
                }
                if (isset($info)) {
                    $this->db->trans_start();
                    $this->db->insert_batch('student_course', $info);
                    echo"INSERTED!";
                    $this->db->trans_complete();
                }
            }
        }

        public function prepare_result_sheet() {
            $query = "SELECT t1.serial, t1.id,t1.class,t1.roll,t1.section,t1.student_name,t1.student_phone,t1.tution_fee,t2.father_name,t2.mother_name
                       FROM `student_info` as t1,parent_info as t2 where t1.id=t2.id and t1.admin=$this->admin 
                       and t1.class='$this->default_class' and t1.status='current' and t1.session='$this->default_session' group by t1.id order by ABS(t1.roll) asc limit 10";
            $data = $this->mstudent->get_current_student($query);
            $data['student_class'] = $this->default_class;
            $data['student_session'] = $this->default_session;

            $total_std_query = "SELECT serial FROM `student_info` as t1 where t1.admin=$this->admin and t1.class='$this->default_class'  and t1.session='$this->default_session' and t1.status='current'";
            $rs = $this->db->query($total_std_query);
            $data['total_std'] = $rs->num_rows();

            $this->view_page->admin_page('v_prepare_result_sheet', $data);
        }

        public function filter_std_rslt_sheet($class = "", $status = "", $session = "") {
            $query = "SELECT t1.serial, t1.id,t1.class,t1.roll,t1.section,t1.student_name,t1.student_phone,t1.tution_fee,t2.father_name,t2.mother_name
                      FROM `student_info` as t1,parent_info as t2 where t1.id=t2.id and t1.admin=$this->admin
                      and t1.class='$class' and t1.status='$status' and t1.session='$session'   group by t1.id order by ABS(t1.roll) asc limit 10";
            $data = $this->mstudent->get_current_student($query);
            $data['asset_url'] = $this->asset_url;
            $data['student_class'] = $class;
            $data['student_session'] = $session;

            $total_std_query = "SELECT serial FROM `student_info` as t1 where t1.admin=$this->admin and t1.class='$class'  and t1.session='$session' and t1.status='current'";
            $rs = $this->db->query($total_std_query);
            $data['total_std'] = $rs->num_rows();

            $this->load->view('vadmin/v_filter_std_rslt_sheet', $data);
        }

        public function get_student_course($serial = 1) {
            $query = "SELECT t1.serial,t1.course_title,t1.course_name,t2.serial as std_course_serial  FROM `course` as t1,student_course as t2
                    where t1.serial=t2.course_serial and t1.admin=t2.admin and t2.admin=$this->admin and t2.student_serial=$serial";
            $data = $this->mstudent->get_student_course($query);
            $data['std_serial'] = $serial;
            $this->load->view('vadmin/v_student_course_fro_result_sheet', $data);
        }

        public function submitresultsheet() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $exam_term = $this->view_page->get_term($data['exam_term']);
            $query = "select exam_term from std_exam_info where admin=$this->admin and student_serial={$data['std_serial']} and exam_term='$exam_term'";
            $marks_info = array();
            $total_marks = 0;

            if (isset($data['course_serial']) && $this->mstudent->check_exam($query) == 0) {
                for ($i = 0; $i < count($data['course_serial']); $i++) {
                    $marks_info[$i] = array('admin' => $this->admin, 'student_serial' => $data['std_serial'], 'exam_term' => $exam_term, 'course_serial' => $data['course_serial'][$i],
                        'marks' => $data['marks'][$i], 'grade' => $data['grade'][$i]);
                    $total_marks+=$data['marks'][$i];
                }
                $exam_info = array('admin' => $this->admin, 'student_serial' => $data['std_serial'], 'exam_term' => $exam_term,
                    'gpa' => $data['gpa'], 'comment' => $data['comment'], 'total_marks' => $total_marks);
                $this->db->trans_start();
                $this->db->insert('std_exam_info', $exam_info);
                $this->db->insert_batch('std_exam_marks', $marks_info);
                echo"INSERTED!";
                $this->db->trans_complete();
            } else {
                echo"THIS EXAM'S MARKS ALREADY EXISTS OR ANOTHER ERROR";
            }
        }

        public function get_student_details_result($exam_info_serial = "") {
            $info = explode("-", $exam_info_serial);
            $query = "SELECT distinct(t1.course_serial),t1.serial,t2.course_name,t2.course_title,t1.marks,t1.grade  FROM
               `std_exam_marks` as t1,course as t2 where t1.course_serial=t2.serial && t1.admin=$this->admin && t1.student_serial=$info[0] && t1.exam_term='$info[1]'";
            $data = $this->mstudent->get_course_marks($query);
            $this->load->view('vadmin/student_exam_marks', $data);
// print_r($data);
// echo"$exam_info_serial";
//echo"$query";
        }

        public function dashboard() {
            $income_cost_query = "SELECT sum(fee_amount)+(select sum(loan_ammount) from loanfromteacher where admin=$this->admin)+(select sum(pay_ammount) from paidbystuff where admin=$this->admin)+
             (select sum(dr) from transaction where admin=$this->admin)
             as income,
             (select sum(pay_amount) from student_cost where admin=1)+(select sum(sallary_ammount) from paidtoteacher where admin=$this->admin)
             +(select sum(sallary_ammount) from paidtostuff where admin=1)+(select sum(cr) from transaction where admin=$this->admin)
             as cost 
             FROM `student_fee` where admin=$this->admin";

            $people_query = "SELECT count(t1.serial) as total FROM `student_info` as t1 where t1.admin=$this->admin and t1.status='current'
                       union all
                       SELECT count(t2.serial) as total FROM `teacher_basic_info` as t2 where t2.admin=$this->admin and t2.status='current'
                       union all
                       SELECT count(t3.serial) as total FROM `stuff_basic_info` as t3 where t3.admin=$this->admin and t3.status='current'";

            $year_student_query = "SELECT count(serial) as student,substr(session,1,4) as year FROM `student_info` where admin=$this->admin group by session";

            $teacher_query = "SELECT * FROM `teacher_basic_info` where admin=$this->admin and status='current' limit 3";

            $student_query = "SELECT t1.serial, t1.id,t1.class,t1.roll,t1.section,t1.student_name,t1.student_phone,t1.tution_fee,t2.father_name,t2.mother_name
                              FROM `student_info` as t1,parent_info as t2 where t1.id=t2.id and t1.admin=$this->admin and t1.status='current'  group by t1.id limit 3";

            $stuff_query = "SELECT * FROM `stuff_basic_info` where admin=$this->admin and status='current'";

            $json['total_income_cost'] = $this->mstudent->get_income_cost_graph($income_cost_query);
            $json['people'] = $this->mstudent->get_public($people_query);
            $json['year_student'] = $this->mstudent->get_year_student_graph($year_student_query);
            $json['student_info'] = $this->mstudent->get_current_student($student_query);
            $json['teacher_info'] = $this->mstudent->get_current_teacher($teacher_query);
            $json['stuff_info'] = $this->mstudent->get_current_stuff($stuff_query);
//$class_routine = $this->mstudent->mget_class_routine($this->default_class, $this->admin, 'sat');


            $class = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
// $class=array("13");
            $i = 0;
            $data = array();
            foreach ($class as $class) {
                $data['class_routine'][$i] = $this->mstudent->mget_class_routine($class, $this->admin, 'sat');
                $data['class'][$i] = $class;
                $i++;
            }

            $info = array_merge($json, $data);
            $this->view_page->admin_page('v_dashboard', $info);
        }

        public function search($search_data = " ") {
            $this->search_data($search_data);
        }

        private function search_data($search_data) {
            $info = str_replace("%20", "", $search_data);
            $student_query = "SELECT * FROM `student_info` WHERE admin=$this->admin && (student_name like '%$info%' || id like '$info' || student_phone like '%$info%'
                || student_email like '%$info%'
                || session like '%$info%' || birth_date like '%$info%')";
            $data1 = $this->mstudent->mget_student_info($student_query);

            $teacher_query = "SELECT * FROM `teacher_basic_info` WHERE admin=$this->admin && (tname like '%$info%' || tid like '%$info%'
                        || tphone like '%$info%' || temail like '%$info%'
                        || tjdate ='$info')";
            $data2 = $this->mstudent->get_current_teacher($teacher_query);

            $stuff_query = "SELECT * FROM `stuff_basic_info` WHERE admin=$this->admin && (stname like '%$info%' || stid like '%$info%'
                        || stphone like '%$info%' || stemail like '%$info%'
                        || stjdate ='$info')";
            $data3 = $this->mstudent->get_current_stuff($stuff_query);
            $data = array_merge($data1, $data2, $data3);
            $data['asset_url'] = $this->asset_url;
            $this->load->view('vadmin/v_search', $data);
        }

        public function search2() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $this->search_data($data['search_query']);
        }

        public function settings() {
            $person = array('student', 'student', 'teacher', 'teacher', 'stuff', 'stuff', 'payment_method');
            $cost_type = array('dr', 'cr', 'cr', 'dr', 'cr', 'dr', 'pm');
            $var = array('studentdr', 'studentcr', 'teachercr', 'teacherdr', 'stuffcr', 'stuffdr', 'payment_method');
            for ($i = 0; $i < count($person); $i++) {
                $query[$i] = "SELECT * FROM `person_cost_type` where admin=$this->admin && person='$person[$i]' && cost_type='$cost_type[$i]'";
                $info[$i] = $this->mstudent->get_person_cost($query[$i], array($var[$i], 'option'));
            }


            $person2 = array('teacher', 'stuff', 'father', 'mother');
            $var2 = array('teacher_type', 'stuff_type', 'father_occupation', 'mother_occupation');
            for ($i = 0; $i < count($person2); $i++) {
                $query2[$i] = "SELECT * FROM `person_occupation_type` where admin=$this->admin && person='$person2[$i]'";
                $info2[$i] = $this->mstudent->get_person_cost($query2[$i], array($var2[$i], 'type'));
            }
            $data = array_merge($info[0], $info[1], $info[2], $info[3], $info[4], $info[5], $info[6], $info2[0], $info2[1], $info2[2], $info2[3]);
            $this->view_page->admin_page('v_settings', $data);
        }

        public function addpersoncosttype() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $option = str_replace("'", "", $data['option']);
            $info = array('admin' => $this->admin, 'person' => $data['person'], 'cost_type' => $data['cost_type'], 'option' => $option);
            if ($this->db->insert('person_cost_type', $info)) {
                echo"<div class='alert alert-warning'>
                    <strong>SUCCESS!</strong>You Have Successfully Inserted The Data <i class='fa fa-fw fa-smile-o'></i>.
                </div>";
            } else {
                echo"<div class='alert alert-danger'>
                    <strong>FAILED!</strong>
                </div>";
            }
        }

        public function addpersonoccupationtype() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $type = str_replace("'", "", $data['type']);
            $info = array('admin' => $this->admin, 'person' => $data['person'], 'type' => $type);
            if ($this->db->insert('person_occupation_type', $info)) {
                echo"<div class='alert alert-warning'>
                    <strong>SUCCESS!</strong>You Have Successfully Inserted The Data <i class='fa fa-fw fa-smile-o'></i>.
                </div>";
            } else {
                echo"<div class='alert alert-danger'>
                    <strong>FAILED!</strong>
                </div>";
            }
        }

        public function deletepersoncosttype() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $query = "DELETE FROM `person_cost_type` where `admin`=$this->admin && `person`='{$data['person']}' && `cost_type`='{$data['cost_type']}' && `option`='{$data['option']}' limit 1";
            if ($this->db->query($query) == 1) {
                echo"<div class='alert alert-warning'>
                    <strong>SUCCESS!</strong>You Have Successfully Deleted The Data <i class='fa fa-fw fa-smile-o'></i>.
                </div>";
            }
//echo $query;
        }

        public function deletepersonoccupationtype() {
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $query = "DELETE FROM `person_occupation_type` where admin=$this->admin && person='{$data['person']}' && type='{$data['type']}' limit 1";
            if ($this->db->query($query) == 1) {
                echo"<div class='alert alert-warning'>
                    <strong>SUCCESS!</strong>You Have Successfully Deleted The Data <i class='fa fa-fw fa-smile-o'></i>.
                </div>";
            }
        }

    }

} else {
    header("Location:http://demo.techcarebd.com/school/index.php?login");
}
?>
