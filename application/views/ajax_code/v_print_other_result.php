        
<div class="col-lg-10" style="border:1px #CCC dashed; padding: 5px">
    <div style="text-align: center;">
        <h2 style="color:green"><? echo $_SESSION['school_name'] ?></h2>
        <p style="color:blue">EXAMINATION RESULT SHEET OF <? echo $exam_term_name;?></p>
        <hr>
    </div>
    <div class="table-responsive">
        <table class="print_tbl">
            <h4>
                STUDENT NAME:<? if (isset($std_name)) {
    echo $std_name[0];
} ?>
            </h4>
            <thead>
                <tr class="active">
                    <th>CLASS</th>
                    <th>SECTION</th>
                    <th>ROLL</th>
                    <th>IDENTITY</th>
                </tr>
            <tbody>

                <?php
                if (isset($std_id)) {
                    echo"<tr><td>$class[0]</td><td>$section[0]</td><td>$roll[0]</td><td>$std_id[0]</td><tr>";
                }
                ?>

            </tbody>

            </thead>
        </table>
    </div>
    <div class="table-responsive">
        <table class="print_tbl">
            <thead>
                <tr class="active">
                    <th>COURSE TITLE</th>
                    <th>COURSE NAME</th>
                    <th>OBTAINED MARKS</th>
                    <th>GRADE</th>
                </tr>
            </thead>
            <tbody>
                <?
                if (isset($serial)) {
                    for ($i = 0; $i < count($serial); $i++) {
                        echo"<tr><td>$mcourse_title[$i]</td><td>$mcourse_name[$i]</td><td>$mcourse_marks[$i]</td><td>$mcourse_grade[$i]</td></tr>";
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table class="print_tbl">
            <thead>
                <tr class="active">
                    <th>
                        COMMENT
                    </th>
                    <th>
                        TOTAL MARKS
                    </th>
                    <th>GPA</th>

                </tr>
            </thead>
            <tbody>
                <?
                if (isset($exam_term)) {
                    echo"<tr><td>$exam_comment[0]</td><td>$exam_marks[0]</td><td>$exam_gpa[0]</td></tr>";
                }
                ?>
                </body>
        </table>
    </div>
    <div style="width: 100%; text-align: right">
        <p style="color:green">POWERED BY TECHCARE BANGLADESH</p>
    </div>
</div>