<div class="modal-body" style="border: 1px #000 dashed;background-color:#cccccc; padding:5px">
    <div class="row">
        <div class="col-lg-12" style="text-align: center;">
            <div class="school_name">
                <h3><? echo $school_name?></h3>
                <?echo $school_address?><hr>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table style="width:100%">
                <tr>
                    <td>
                        RECEIPT SERIAL:#<? echo $receipt_serial ?>
                    </td>
                    <td style="float:right">
                        DATE:#<? echo $receipt_date ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <br>
            <p class="text-warning">
                CASH RECEIVED FROM <? echo $receiver; ?> OF AMMOUNT TAKA <strong><? echo $receipt_amount; ?>/=</strong> FOR <? echo $receipt_for?>
            </p>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

            <table style="width:100%">
                <tr>
                    <td>
                        <p class="text-info">
                            PAYMENT METHOD:<? echo $receipt_type; ?><br>
                            POWERED BY:
                            TechCare Bangladesh
                        </p>
                    </td>
                    <td style="float:right">
                        <p class="text-info">
                            TOTAL AMOUNT DUE:<? echo $total_due; ?> TAKA<BR>
                            AMOUNT RECEIVED:<? echo $receipt_amount; ?> TAKA<BR>
                            BALANCE DUE:<? echo $receipt_due; ?> TAKA.<br>

                            RECEIVED BY:CASHIER
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>