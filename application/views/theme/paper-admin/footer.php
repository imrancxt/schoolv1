

                           

                            </div> <!-- .container-fluid -->
                        </div> <!-- #page-content -->
                    </div>
                    <footer role="contentinfo">
                        <div class="clearfix">
                            <ul class="list-unstyled list-inline pull-left">
                                <li><h6 style="margin: 0;">&copy; 2016 KaijuThemes</h6></li>
                            </ul>
                        </div>
                    </footer>

                </div>
            </div>
        </div>

        <!-- Das FAB -->
        <button class="btn btn-primary btn-fab demo-switcher-fab" data-toggle="tooltip" data-placement="top" title="Click for Settings"><i class="material-icons">settings</i></button>

        <!-- Switcher -->
        <div class="demo-options">
            <div class="demo-body">
                <div class="tabular">
                    <div class="tabular-row">
                        <div class="tabular-cell">Fixed Header</div>
                        <div class="tabular-cell demo-switches text-right">
                            <div class="togglebutton checkbox-primary">
                                <label>
                                    <input type="checkbox" name="demo-fixedheader" />
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tabular-row">
                        <div class="tabular-cell">Boxed Layout</div>
                        <div class="tabular-cell demo-switches text-right">
                            <div class="togglebutton toggle-primary">
                                <label>
                                    <input type="checkbox" name="demo-layoutboxed">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tabular-row">
                        <div class="tabular-cell">Collapse Leftbar</div>
                        <div class="tabular-cell demo-switches text-right">
                            <div class="togglebutton toggle-primary">
                                <label>
                                    <input type="checkbox" name="demo-collapseleftbar">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="demo-body">
                <div class="option-title">Brand</div>
                <ul id="demo-header-color" class="demo-color-list">
                    <li><span data-addclass="navbar-brand-default" class="brand-switcher demo-default"></span></li>
                    <li><span data-addclass="navbar-brand-primary" class="brand-switcher demo-primary"></span></li>
                    <li><span data-addclass="navbar-brand-success" class="brand-switcher demo-success"></span></li> 
                    <li><span data-addclass="navbar-brand-danger" class="brand-switcher demo-danger"></span></li>
                    <li><span data-addclass="navbar-brand-warning" class="brand-switcher demo-warning"></span></li>
                    <li><span data-addclass="navbar-brand-info" class="brand-switcher demo-info"></span></li> 
                    <li><span data-addclass="navbar-brand-inverse" class="brand-switcher demo-inverse"></span></li> 
                </ul>
            </div>

            <div class="demo-body">
                <div class="option-title">Topnav</div>
                <ul id="demo-header-color" class="demo-color-list">
                    <li><span data-addclass="navbar-cyan" class="topnav-switcher demo-cyan"></span></li>
                    <li><span data-addclass="navbar-light-blue" class="topnav-switcher demo-light-blue"></span></li>
                    <li><span data-addclass="navbar-blue" class="topnav-switcher demo-blue"></span></li>
                    <li><span data-addclass="navbar-indigo" class="topnav-switcher demo-indigo"></span></li>
                    <li><span data-addclass="navbar-deep-purple" class="topnav-switcher demo-deep-purple"></span></li> 
                    <li><span data-addclass="navbar-purple" class="topnav-switcher demo-purple"></span></li> 
                    <li><span data-addclass="navbar-pink" class="topnav-switcher demo-pink"></span></li> 
                    <li><span data-addclass="navbar-red" class="topnav-switcher demo-red"></span></li>
                    <li><span data-addclass="navbar-teal" class="topnav-switcher demo-teal"></span></li>
                    <li><span data-addclass="navbar-green" class="topnav-switcher demo-green"></span></li>
                    <li><span data-addclass="navbar-light-green" class="topnav-switcher demo-light-green"></span></li>
                    <li><span data-addclass="navbar-orange" class="topnav-switcher demo-orange"></span></li>               
                    <li><span data-addclass="navbar-deep-orange" class="topnav-switcher demo-deep-orange"></span></li>

                    <li><span data-addclass="navbar-bluegray" class="topnav-switcher demo-bluegray"></span></li>


                    <li><span data-addclass="navbar-gray" class="topnav-switcher demo-gray"></span></li> 

                    <li><span data-addclass="navbar-default" class="topnav-switcher demo-default"></span></li>
                    <li><span data-addclass="navbar-bleachedcedar" class="topnav-switcher demo-bleachedcedar"></span></li>
                    <li><span data-addclass="navbar-brown" class="topnav-switcher demo-brown"></span></li>
                </ul>
            </div>

            <div class="demo-body">
                <div class="option-title">Sidebar</div>
                <ul id="demo-sidebar-color" class="demo-color-list">
                    <li><span data-addclass="sidebar-cyan" class="leftbar-switcher demo-cyan"></span></li>
                    <li><span data-addclass="sidebar-light-blue" class="leftbar-switcher demo-light-blue"></span></li>
                    <li><span data-addclass="sidebar-blue" class="leftbar-switcher demo-blue"></span></li>
                    <li><span data-addclass="sidebar-indigo" class="leftbar-switcher demo-indigo"></span></li>
                    <li><span data-addclass="sidebar-deep-purple" class="leftbar-switcher demo-deep-purple"></span></li> 
                    <li><span data-addclass="sidebar-purple" class="leftbar-switcher demo-purple"></span></li> 
                    <li><span data-addclass="sidebar-pink" class="leftbar-switcher demo-pink"></span></li> 
                    <li><span data-addclass="sidebar-red" class="leftbar-switcher demo-red"></span></li>
                    <li><span data-addclass="sidebar-teal" class="leftbar-switcher demo-teal"></span></li>
                    <li><span data-addclass="sidebar-green" class="leftbar-switcher demo-green"></span></li>
                    <li><span data-addclass="sidebar-light-green" class="leftbar-switcher demo-light-green"></span></li>
                    <li><span data-addclass="sidebar-orange" class="leftbar-switcher demo-orange"></span></li>               
                    <li><span data-addclass="sidebar-deep-orange" class="leftbar-switcher demo-deep-orange"></span></li>

                    <li><span data-addclass="sidebar-bluegray" class="leftbar-switcher demo-bluegray"></span></li>


                    <li><span data-addclass="sidebar-gray" class="leftbar-switcher demo-gray"></span></li> 

                    <li><span data-addclass="sidebar-default" class="leftbar-switcher demo-default"></span></li>
                    <li><span data-addclass="sidebar-bleachedcedar" class="leftbar-switcher demo-bleachedcedar"></span></li>
                    <li><span data-addclass="sidebar-brown" class="leftbar-switcher demo-brown"></span></li>
                </ul>
            </div>



        </div>
        <!-- /Switcher -->
        <!-- Load site level scripts -->

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

       								<!-- Load Bootstrap -->
        <script src="../paper-theme/assets/js/enquire.min.js"></script> 									<!-- Load Enquire -->

        <script src="../paper-theme/assets/plugins/velocityjs/velocity.min.js"></script>					<!-- Load Velocity for Animated Content -->
        <script src="../paper-theme/assets/plugins/velocityjs/velocity.ui.min.js"></script>

        <script src="../paper-theme/assets/plugins/progress-skylo/skylo.js"></script> 		<!-- Skylo -->

        <script src="../paper-theme/assets/plugins/wijets/wijets.js"></script>     						<!-- Wijet -->

        <script src="../paper-theme/assets/plugins/sparklines/jquery.sparklines.min.js"></script> 			 <!-- Sparkline -->

        <script src="../paper-theme/assets/plugins/codeprettifier/prettify.js"></script> 				<!-- Code Prettifier  -->

        <script src="../paper-theme/assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->

        <script src="../paper-theme/assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->

        <script src="../paper-theme/assets/plugins/dropdown.js/jquery.dropdown.js"></script> <!-- Fancy Dropdowns -->
        <script src="../paper-theme/assets/plugins/bootstrap-material-design/js/material.min.js"></script> <!-- Bootstrap Material -->
        <script src="../paper-theme/assets/plugins/bootstrap-material-design/js/ripples.min.js"></script> <!-- Bootstrap Material -->

        <script src="../paper-theme/assets/js/application.js"></script>
        <script src="../paper-theme/assets/demo/demo.js"></script>
        <script src="../paper-theme/assets/demo/demo-switcher.js"></script>

        <!-- End loading site level scripts -->

        <!-- Load page level scripts-->

        <script>
            $("#dyna").click(function () {
                if ($("#dynamo").text() == "")
                    $("#dynamo").text("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
                else {
                    $("#dynamo").html($("#dynamo").text() + $("#dynamo").text());
                }
            })

            $("#dyna-del").click(function () {
                $("#dynamo").html("");
            })	
        </script>

        <!-- End loading page level scripts-->


    </body>
</html>