$(document).ready(function(){
    var row_serial=1;
    var base_url="http://school.techcarebd.com/demo/index.php?";
    $("#filter_current_student").click(function(){
        var Class=$("#get_class").val();
        var Status=$(this).attr("status");
        var Session=$("#session").val();
        //alert(Session);
        page="admin/filter_current_student/"+Class+"/"+Status+"/"+Session;
        mf.change_content("#current_student_tbl",page);
    });
    $(document).on('click','.std_page',function(){
         $('.std_page').css({"background-color":"#cc6600"});
           $(this).css({"background-color":"black"});
        var Class=$("#student_class").val();
        var Status=$(this).attr("status");
        var Session=$("#student_session").val();
        var Limit=$(this).attr("limit");
        page="admin2/get_std_page/"+Class+"/"+Status+"/"+Session+"/"+Limit;
        mf.change_content("#std_tbl_content",page);
        
        
    });
    $(document).on('click','.std_rslt_page',function(){
        $('.std_rslt_page').css({"background-color":"#cc6600"});
           $(this).css({"background-color":"black"});
        var Class=$("#student_class").val();
        var Status=$(this).attr("status");
        var Session=$("#student_session").val();
        var Limit=$(this).attr("limit");
        page="admin2/get_std_rslt_page/"+Class+"/"+Status+"/"+Session+"/"+Limit;
        mf.change_content("#std_tbl_content",page);
         
        //alert(page);
    });
    
    $("#filter_std_rslt_sheet").click(function(){
        var Class=$("#get_class").val();
        var Status=$(this).attr("status");
        var Session=$("#session").val();
        page="admin/filter_std_rslt_sheet/"+Class+"/"+Status+"/"+Session;
        //alert(page);
       mf.change_content("#current_student_tbl",page);
    });
    $("#filter_current_teacher").click(function(){
        var Type=$("#get_teacher_type").val();
        var Status=$(this).attr('status');
        // alert(Type);
        page="admin/filter_current_teacher/"+Type+"/"+Status;
        mf.change_content("#current_teacher_tbl",page);
    });
    
    $("#filter_current_stuff").click(function(){
        var Type=$("#get_stuff_type").val();
        var Status=$(this).attr('status');
        // alert(Type);
        page="admin/filter_current_stuff/"+Type+"/"+Status;
        mf.change_content("#current_stuff_tbl",page);
    });
    
    $(".click_radio").click(function(){
        div=$(this).attr("attr");
        /*$(".std_info").fadeOut("slow");
        $("#info_"+div).fadeIn("slow");*/
        $(".std_info").slideUp();
        $("#info_"+div).slideDown();
    });
    $('.std_info').css({
        "display":"none"
    });
    $(".get_class_routine").click(function(){
        $(".get_class_routine").css({
            "background-color":"#6666ff"
        });
        $(this).css({
            "background-color":"green"
        });
        Class=$(this).attr('title');
        page="admin/get_class_routine/"+Class;
        mf.change_content("#specific_classroutine", page);
        $('#specific_classroutine').slideUp("slow");
        ;
        $('#specific_classroutine').slideDown("slow");;
    });
    $(".get_courses").click(function(){
        //
        //$("#specific_course").html("im");
        $(".get_courses").css({
            "background-color":"#6666ff"
        });
        $(this).css({
            "background-color":"green"
        });
        Class=$(this).attr('title');
        page="admin/get_course/"+Class;
        mf.change_content("#specific_course", page);
        $('#specific_course').slideUp();
        ;
        $('#specific_course').slideDown();;
    })
    $("#add_course_field").click(function(){     
        Class=$("#get_class").val();
        if(Class!="SELECT CLASS"){
            field= parseInt($("#row_field").val());
            for(var i=0;i<field;i++){
                html="<tr class='success'><td>"+row_serial+"</td><td><input required name='ctitle[]' class='form-control' placeholder='COURSE TITLE'/></td><td><input required name='cname[]' class='form-control' placeholder='COURSE NAME'/></td><td><input required name='cclass[]' class='form-control' value='"+Class+"'/></td></tr>";
                $("#add_course_tbl").append(html);
                row_serial++;
            }   
        }
        else{
            alert("SELECT A CLASS...PLEASE");
        }
        
    });
    $(".get_course_list").click(function(){
        Class=$(this).attr('title');
        page="admin/choose_course/"+Class;
        mf.change_content("#course_list", page);
       
    });
    $("#delete_row").click(function(){
        // alert("im");
        //var rowCount = $("#add_course_tbl").rows.length;
        //$("#add_course_tbl").deleteRow(rowCount -1);
        if(row_serial>1){
            row_serial--;   
        }
        var table = document.getElementById("add_course_tbl");
        var rowCount = table.rows.length;

        table.deleteRow(rowCount -1);
        


    });
    $(document).on('click','#mark_all',function(){
        // $('.my_checkbox').is(':checked');
        $('.my_checkbox').attr('checked', true);
    });
    $(document).on('click','#unmark_all',function(){
        $('.my_checkbox').removeAttr("checked");
    });
    $(document).on('click','.make_result_sheet',function(){
        $(".make_result_sheet").css({
            "background-color":"coral"
        });
        $(this).css({
            "background-color":"green"
        });
        serial=$(this).attr('title');
        page="admin/get_student_course/"+serial;
        mf.change_content("#course_content", page);
    });
    $("#search_button").click(function(){
        search_query=$("#search_query").val();
        page="admin/search/"+search_query;
        mf.change_content(".container-fluid", page);
    });
    $(".edit_result").click(function(){
        info=$(this).attr('title');
        page="admin2/editstdresult/"+info;
        mf.change_content("#exam_details_content", page);
    });
    /* $(".add_new_cr").click(function(){
        cost_type="cr";
        person=$(this).attr('person');
        option=$("#cr_"+person).val();
        page="http://localhost/school/admin/addpersoncosttype/"+person+"/"+cost_type+"/"+option;
        mf.change_alert(page);
    //alert(type_value);
    });
    $(".add_new_dr").click(function(){
        cost_type="dr";
        person=$(this).attr('person');
        option=$("#dr_"+person).val();
        page="http://localhost/school/admin/addpersoncosttype/"+person+"/"+cost_type+"/"+option;
        mf.change_alert(page);
    });*/
    $(".get_details_result").click(function(){
        $('#exam_details_content').slideUp("slow");
        $(".get_details_result").css({
            "background-color":"coral"
        });
        $(this).css({
            "background-color":"green"
        });
        ;
        serial=$(this).attr('title');
        page="admin2/editstdresult/"+serial;
        mf.change_content("#exam_details_content", page);
        $('#exam_details_content').slideDown("slow");
    })
    var mf={
        change_content:function(content,page){
            $.ajax({
                url:base_url+page,
                success: function(data){
                    if(data!=""){
                        $(content).html(data);
                    }
                }
            })
        },
        change_alert:function(page){
            $.ajax({
                url:base_url+page,
                success: function(data){
                    if(data!=""){
                        alert(data);
                    }
                }
            })
        }
    };
    
    $(document).on('submit','.alert_ajax_form',function(e)
    {
        var formObj =$(this);
        var formURL =formObj.attr("action");
        var formData = new FormData(this);
        var refresh=$(this).attr("refresh");
        $.ajax({
            url: formURL,
            type: 'POST',
            data:formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                if(data!=""){
                    if(data=='trashed'||data=="recovered"){
                        window.location=base_url+"admin/"+refresh;
                    }
                    else{
                        alert(data);
                    }
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
                        
        });
        e.preventDefault();
    });
    $(document).on('submit','.change_content_by_form',function(e)
    {
        var formObj =$(this);
        var formURL =formObj.attr("action");
        var formData = new FormData(this);
        var content=$(this).attr('content');
        //alert(content);
        $.ajax({
            url: formURL,
            type: 'POST',
            data:formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                if(data!=""){
                    $(content).html(data);
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
                        
        });
        e.preventDefault();
    });
    $(document).on('submit','.append_by_form',function(e)
    {
        var formObj =$(this);
        var formURL =formObj.attr("action");
        var formData = new FormData(this);
        var content=$(this).attr('content');
        //alert(content);
        $.ajax({
            url: formURL,
            type: 'POST',
            data:formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                if(data!=""){
                    $(content).append(data);
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
                        
        });
        e.preventDefault();
    });
    $("#add_new_route").click(function(){
        // alert("imran");
        $("#add_new_routine_content").slideToggle(); 
    });
    add_educational_back=function(){
        var info1= new Array('SSC','HSC','BSC(hons)','BA(hons)','BCOM');
        option1="";
        for(var i=0;i<info1.length;i++){
            option1=option1+"<option>"+info1[i]+"</option>";
        // alert(str);
        }
        html1="<div class='col-lg-4'><div class='table-responsive'><table class='table table-bordered table-hover'><tbody><tr><td>QUALIFICATION</td><td>"+
        "<select name='qualification[]' class='form-control'>"+option1+"</select></td></tr>";
        html2="<tr><td>PASSING YEAR</td><td><input name='passing_year[]' type='date' placeholder='PASSING YEAR' class='form-control'/></td></tr>";
        html3="<tr><td>SPECIALIZATION</td><td><input name='specialization[]' placeholder='SPECIALIZATION' class='form-control'/></td></tr>";
        html4="<tr><td>INSTITUTE</td><td><textarea name='institute[]' placeholder='INSTITUTE' class='form-control'></textarea></td></tr>";
        html5="<tr><td>CGPA</td><td><input name='gpa[]' placeholder='GPA' class='form-control'/></td></tr>";
        html6="</tbody></table></div></div>";
        content=html1+html2+html3+html4+html5;
        $("#education_back_row").append(content);
    };
    $("#view_admin_log_btn").click(function(){
        date1=$("#date1").val();
        date2=$("#date2").val();
        page="admin2/get_admin_logged_date_btn_date/"+date1+"/"+date2;
        mf.change_content("#admin_log_tbl", page);
    });
    add_new_admin=function(){
        $("#add_new_admin_content").slideToggle("slow");
    };
    stddrreceipt=function(serial){
        $('#dr_receipt').slideUp("slow");
        page="admin2/stdreceipt/dr/"+serial;
        mf.change_content("#dr_receipt", page); 
        $('#dr_receipt').slideDown("slow");;
    };
    tecdrreceipt=function(serial){
        $('#dr_receipt').slideUp("slow");
        page="admin2/tecreceipt/dr/"+serial;
        mf.change_content("#dr_receipt", page); 
        $('#dr_receipt').slideDown("slow");;
    };
    $(document).on('click','#print_this',function(){
        // $('.my_checkbox').is(':checked');
        $(this).hide();
        $("#dr_receipt").print();
        $(this).show();
    });
    $("#search_query").trigger("myEvent",["hello"])
    
});