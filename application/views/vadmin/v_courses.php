
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<? echo base_url() ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/courses">COURSES</a>
            </li>
 
        </ol>
    </div>
</div>

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php
        if (isset($class)) {
            for ($i = 0; $i < count($class); $i++) {
                if (isset($class_course[$i]['serial'])) {
                    if ($i == 0) {
                        echo"<li data-target='#carousel-example-generic' data-slide-to='$i' class='active'></li>";
                    } else {
                        echo"<li data-target='#carousel-example-generic' data-slide-to='$i'></li>";
                    }
                }
            }
        }
        ?>
    </ol>

    <div class="carousel-inner" role="listbox">
        <?php
        include_once 'common_function.php';
        $cm = new common_function();
        if (isset($class)) {
            for ($i = 0; $i < count($class); $i++) {
                if (isset($class_course[$i]['serial'])) {
                    if ($i == 0) {
                        // print_courses($serial, $course_title, $course_name, $active, $class)
                        $cm->print_courses($class_course[$i]['serial'], $class_course[$i]['course_title'], $class_course[$i]['course_name'], "active", $class[$i]);
                    } else {
                        $cm->print_courses($class_course[$i]['serial'], $class_course[$i]['course_title'], $class_course[$i]['course_name'], "", $class[$i]);
                    }
                }
            }
        }
        ?>
    </div>

    <!-- Controls -->
    <?php
    if (isset($class_course[0]['serial'])) {
        echo"<a class='left carousel-control' href='#carousel-example-generic' role='button' data-slide='prev'>
        <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>
        <span class='sr-only'>Previous</span>
    </a>
    <a class='right carousel-control' href='#carousel-example-generic' role='button' data-slide='next'>
        <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>
        <span class='sr-only'>Next</span>
    </a>";
    }
    ?>
</div>
<div class="row">
    <div class="col-lg-3">
        <select class="form-control" id="get_class">
            <?php
            $option = array('SELECT CLASS', "00", '01', '02', '03', '04', '05', '07', '08', '09', '10', '11', '12');
            foreach ($option as $option) {
                echo"<option>$option</option>";
            }
            ?>
        </select>
    </div>
    <div class="col-lg-3">
        <input class="form-control" placeholder="NUMBER OF COURSE" id="row_field"/>
    </div>
    <div class="col-lg-3">
        <button id="add_course_field" type="submit" class="btn btn-success" style="width: 100%">ADD COURSE</button>
    </div>
    <div class="col-lg-3">
        <button id="delete_row" type="submit" class="btn btn-success" style="width: 100%">DELETE COURSE</button>
    </div>
</div>
<div class="row">
    <form class='alert_ajax_form' action='../admin/add_course' method='POST' enctype='multipart/form-data'>
        <div class="col-lg-12">
            <div class="table-responsive">
                <br>
                <button  type="submit" class="btn btn-warning">SUBMIT</button>
                <table class="table table-bordered table-hover" id="add_course_tbl">
                    <tr class="active">
                        <th>SERIAL</th>
                        <th>COURSE TITLE</th>
                        <th>COURSE NAME</th>
                        <th>CLASS</th>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>
<style>
    .get_courses{
        background-color:#6666ff;
        border: 1px white solid;
        color: white;
        min-width: 37px;
        border-radius: 4px;
    }
</style>
<script>
    $(function () {
        $('.carousel').carousel({
            interval: 3000
        })
    })
</script>