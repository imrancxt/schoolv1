<div class="panel-heading">
    <div class='alert alert-success'>
        <?php
        if (isset($today)) {
            $today1 = date("F-Y", strtotime($today));
            echo"$today1";
            if (isset($stserial)) {
                echo"<input name='stserial' type='hidden' value='$stserial'/>";
            }
        }
        ?>
    </div>
</div>
<div class="panel-body">
    <?php
    if (isset($today)) {
        $date = new DateTime($today);
        $date = $date->format('t');
        for ($i = 1; $i <= $date; $i++) {
            /* if (isset($yatt["date$i"])) {
              echo"<div class='attny'>$i</div>";
              } else {
              if (isset($natt["date$i"])) {
              echo"<div class='attnn'>$i</div>";
              } else {
              echo"<div class='attno'>$i</div>";
              }
              } */
            if (isset($yatt["date$i"])) {
                $title = date('D', strtotime($yatt["title$i"]));
                echo"<div class='attny' title='$title Day<br>PRESENT'>$i</div>";
            } else {
                if (isset($natt["date$i"])) {
                    $title = date('D', strtotime($natt["title$i"]));
                    echo"<div class='attnn' title='$title Day<br>ABSENT'>$i</div>";
                } else {
                    //$title=date("D",strtotime($name));
                    $date1 = date('Y-m', strtotime($today));
                    $title = date("D", strtotime("$date1-$i"));
                    echo"<div class='attno' title='$title Day'>$i</div>";
                }
            }
        }
    }
    ?>
</div>

<script>
    $(document).ready(function() {
        new $.Zebra_Tooltips($('.attno,.attny,.attnn'));
    });
</script>