<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead>
            <tr class="active">
                <th>
                    TIME
                </th>
                <?php
                if (isset($time)) {
                    for ($i = 0; $i < count($time) / 7; $i++) {
                        echo"<th>$time[$i]</th>";
                    }
                }
                ?>
            </tr>
            <tr class="danger">
                <th>
                    PERIOD
                </th>
                <?php
                if (isset($period)) {
                    for ($i = 0; $i < count($period) / 7; $i++) {
                        echo"<th>$period[$i]</th>";
                    }
                }
                ?>
            </tr>
            <?php
            if (isset($sub)) {
                $lim = count($sub) / 7;
                echo"<tr class='info'>";
                echo"<td>SAT</td>";
                for ($i = 0; $i < 1 * $lim; $i++) {
                    echo"<td>$sub[$i]<br>$teacher[$i]</td>";
                }
                echo"</tr>";

                echo"<tr class='info'>";
                echo"<td>SUN</td>";
                for ($j = $i; $j < 2 * $lim; $j++) {
                    echo"<td>$sub[$j]<br>$teacher[$j]</td>";
                }
                echo"</tr>";


                echo"<tr class='info'>";
                echo"<td>MON</td>";
                for ($k = $j; $k < 3 * $lim; $k++) {
                    echo"<td>$sub[$k]<br>$teacher[$k]</td>";
                }
                echo"</tr>";

                echo"<tr class='warning'>";
                echo"<td>TUE</td>";
                for ($l = $k; $l < 4 * $lim; $l++) {
                    echo"<td>$sub[$l]<br>$teacher[$l]</td>";
                }
                echo"</tr>";

                echo"<tr class='success'>";
                echo"<td>WED</td>";
                for ($m = $l; $m < 5 * $lim; $m++) {
                    echo"<td>$sub[$m]<br>$teacher[$m]</td>";
                }
                echo"</tr>";
                
                 echo"<tr class='success'>";
                echo"<td>THR</td>";
                for ($n = $m; $n < 6 * $lim; $n++) {
                    echo"<td>$sub[$n]<br>$teacher[$n]</td>";
                }
                echo"</tr>";
                
                 echo"<tr class='success'>";
                echo"<td>FRI</td>";
                for ($o = $n; $o < 7 * $lim; $o++) {
                    echo"<td>$sub[$o]<br>$teacher[$o]</td>";
                }
                echo"</tr>";
            }
            ?>
        </thead>
    </table>
</div>