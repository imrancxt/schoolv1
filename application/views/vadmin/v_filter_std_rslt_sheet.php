<table class="table table-bordered table-hover">
    <thead>
        <tr class="active">
            <th>IMAGE</th>
            <th>NAME</th>
            <th>STUDENT's ID</th>
            <th>CLASS</th>
            <th>ROLL</th>
            <th>SECTION</th>
            <th>PHONE</th>
            <th>FATHER's NAME</th>
            <th>MOTHER's NAME</th>                                
            <th>MAKE RESULT SHEET</th>
        </tr>
    </thead>
    <tbody id="std_tbl_content">
        <?php
        if (isset($id)) {
            for ($i = 0; $i < count($id); $i++) {
                $url1 = $asset_url . "img/profile/student/$serial[$i].jpg";
                $url2 = base_url() . "admin/student/$serial[$i]";
                echo"<tr class='warning'>
                                           <td>
                                            <a href='$url2' target='_blank'> <img src='$url1' height='50px' width='50px'/></a>
                                           </td>
                                    <td><a href='$url2' target='_blank'>$student_name[$i]</a></td>
                                         <td> <a href='$url2' target='_blank'> $id[$i]</a></td>
                                           <td>$class[$i]</td>
                                            <td>$roll[$i]</td>
                                    <td>$section[$i]</td>
                                           <td>$student_phone[$i]</td>
                                           <td>$father_name[$i]</td>
                                           <td>$mother_name[$i]</td>
                                           <td> <button  class='make_result_sheet' title='$serial[$i]'>MAKE RESULT</button></td>
                                         </tr>";
            }
            echo"<input type='hidden' value='$student_class' id='student_class'/>";
            echo"<input type='hidden' value='$student_session' id='student_session'/>";
        }
        ?>
    </tbody>
</table>
<div class="col-lg-12" style="text-align: center">
    <hr>
</div>
<div class="col-lg-12" style="text-align: center">
    <i class='fa fa-fw fa-arrow-left'></i>
    <?
    if ($total_std>0) {
        include_once 'common_function.php';
        $cm = new common_function();
        $cm->print_std_page($total_std, 'current', 'std_rslt_page');
    }
    ?>
    <i class='fa fa-fw fa-arrow-right'></i>
    <hr>
</div>