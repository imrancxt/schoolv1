<style>
    .select_box {
        display: block;
        width: 100%;
        height: 34px;

    }
</style>
<!-- Page Heading -->


<!-- /.row -->
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heding">Filter Student</div>
            <div class="panel-body">
         <div class="form-group">
        <select  name="class" id="get_class" class="select form-control">
            <?php
            $option = array("CLASS", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
            foreach ($option as $option) {
                echo"<option>$option</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <?php
        include_once 'common_function.php';
        $cm = new common_function();
        $cm->print_session();
        ?>
    </div>
    <div class="form-group">
        <button class="btn btn-default btn-raised" id="filter_current_student" status="current" style="width:100%;">FILTER</button>
    </div>
    </div>
    <div class="col-lg-8">
        
    </div>
        </div>
        </div>
</div>




<form  class="change_content_by_form" content="#content" action='../admin/add_std_info' method='POST' enctype='multipart/form-data'>
    <div class="row">
        <div class="col-lg-3">
            <label  class="btn btn-info active" style="width:100%; border-radius: 0px">
                <!--<input class="click_radio" attr="fee" type='radio' name='option' id='' value='add_fee'>-->
                <input  autocomplete="off"  type='radio' name='option' value='add_fee' data-toggle="modal" data-target="#exampleModal1" data-whatever="@mdo">
                ADD FEE
            </label>

            <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">ADD FEE</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr class="warning">
                                        <td>DATE</td>
                                        <td>
                                            <input type="date" class="form-control" name="fee_date">
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>REASON</td>
                                        <td>
                                            <select class="form-control" name="fee_for">
                                                <?php
                                                if (isset($studentdr)) {
                                                    foreach ($studentdr as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            PAYMENT METHOD
                                        </td>
                                        <td>
                                            <select class="form-control" name="fee_payment_method">
                                                <?php
                                                echo"<option>PAYMENT METHOD</option>";
                                                if (isset($payment_method)) {
                                                    foreach ($payment_method as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            PAID AMOUNT
                                        </td>
                                        <td>
                                            <input name="fee_amount" class="form-control" placeholder="PAID AMOUNT">
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            DUE AMOUNT
                                        </td>
                                        <td>
                                            <input name="fee_due_amount" class="form-control" placeholder="DUE AMOUNT">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">ADD</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <label class="btn btn-info active" style="width:100%;border-radius: 0px">
                <input autocomplete="off"  type='radio' name='option' value='add_pay' data-toggle="modal" data-target="#exampleModal2" data-whatever="@mdo">
                ADD SCHOLARSHIP
            </label>

            <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">ADD SCHOLARSHIP</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr class="warning">
                                        <td>DATE</td>
                                        <td>
                                            <input  type="date" class="form-control" name="pay_date">
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>REASON</td>
                                        <td>
                                            <select class="form-control" name="pay_for">
                                                <?php
                                                if (isset($studentcr)) {
                                                    foreach ($studentcr as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>PAYMENT METHOD</td>
                                        <td>
                                            <select class="form-control" name="sch_payment_method">
                                                <?php
                                                echo"<option>PAYMENT METHOD</option>";
                                                if (isset($payment_method)) {
                                                    foreach ($payment_method as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>PAID AMOUNT</td>
                                        <td>
                                            <input name="pay_amount" class="form-control" placeholder="PAID AMOUNT">
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            DUE AMOUNT
                                        </td>
                                        <td>
                                            <input name="pay_due_amount" class="form-control" placeholder="DUE AMOUNT">
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">ADD</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-3">
            <label class="btn btn-info active" style="width:100%;border-radius: 0px">
                <input class="click_radio" type='radio' name='option' value='add_attendence' data-toggle="modal" data-target="#exampleModal3" data-whatever="@mdo">
                UPDATE ATTENDENCE SHEET
            </label>
            <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">UPDATE ATTENDENCE SHEET</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr class="warning">
                                        <td>
                                            DATE
                                        </td>
                                        <td>
                                            <input  name="attendence_date"  type="date" class="form-control">
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            CLASS TEACHER
                                        </td>
                                        <td>
                                            <select class="form-control" name="class_teacher">
                                                <?php
                                                if (isset($teacher_name)) {
                                                    foreach ($teacher_name as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            STATUS
                                        </td>
                                        <td>
                                            <select class="form-control" name="attendence_status">
                                                <?php
                                                $option = array("YES", "NO");
                                                foreach ($option as $option) {
                                                    echo"<option>$option</option>";
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">UPDATE</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-3">
            <label class="btn btn-info active" style="width:100%;border-radius: 0px">
                <input class="click_radio" type='radio' name='option' value='trash' data-toggle="modal" data-target="#exampleModal4" data-whatever="@mdo">
                TRASH
            </label>
            <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">TRASH THESE STUDENT </h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-warning">
                                ARE YOU SURE?
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">YES</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div id="content" style="text-align:center">

    </div>
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" id="mark_unmark" style="width:113px; border-radius: 0px">MARK ALL</a>
            <div class="table-responsive" id="current_student_tbl">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="active">
                            <th>CHECK</th>
                            <th>NAME</th>
                            <th>STUDENT's ID</th>
                            <th>CLASS</th>
                            <th>ROLL</th>
                            <th>SECTION</th>
                            <th>PHONE NO</th>
                            <th>FATHER's NAME</th>
                            <th>MOTHER's Name</th>
                            <th>TUITION FEE SCALE</th>
                        </tr>
                    </thead>
                    <tbody id="std_tbl_content">
                        <?php
                        if (isset($id)) {
                            /* for ($i = 0; $i < count($id); $i++) {
                              $url = $GLOBALS['asset_url'] . "img/profile/student/$serial[$i]";
                              $url2 = base_url() . "admin/student/$serial[$i]";
                              echo"<tr class='warning'>
                              <td>
                              <input class='my_checkbox' type='checkbox' name='serial[]' value='$serial[$i]'/>
                              <a href='$url2'><img src='$url.jpg' height='50px' width='50px'/></a>
                              </td>
                              <td><a href='$url2' target='_blank'>$student_name[$i]</a></td>
                              <td><a href='$url2'>$id[$i]</a></td>
                              <td>$class[$i]</td>
                              <td>$roll[$i]</td>
                              <td>$section[$i]</td>
                              <td>$student_phone[$i]</td>
                              <td>$father_name[$i]</td>
                              <td>$mother_name[$i]</td>
                              <td>$tution_fee[$i]</td>
                              </tr>";

                              } */
                            for ($i = 0; $i < count($id); $i++) {
                                $url = $GLOBALS['asset_url'] . "img/profile/student/$serial[$i]";
                                $url2 = base_url() . "admin/student/$serial[$i]";
                                echo"<tr class='warning'>
                                           <td>
                                           <input class='toggle-one' unchecked type='checkbox' data-size='mini' name='serial[]' value='$serial[$i]'/>
                                           <a href='$url2'><img src='$url.jpg' height='50px' width='50px'/></a>
                                           </td>
                                        <td><a href='$url2' target='_blank'>$student_name[$i]</a></td>
                                           <td><a href='$url2'>$id[$i]</a></td>
                                           <td>$class[$i]</td>
                                           <td>$roll[$i]</td>
                                           <td>$section[$i]</td>
                                           <td>$student_phone[$i]</td>
                                           <td>$father_name[$i]</td>
                                           <td>$mother_name[$i]</td>
                                           <td>$tution_fee[$i]</td>
                                         </tr>";
                                //<td><pre>$parent_address[$i]</pre></td>
                            }
                            echo"<input type='hidden' value='$student_class' id='student_class'/>";
                            echo"<input type='hidden' value='$student_session' id='student_session'/>";

                            //class="toggle-one" unchecked type="checkbox" data-size="mini"
                        }
                        ?>
                    </tbody>
                </table>
                <div class="col-lg-12" style="text-align: center">
                    <hr>
                </div>
                <div class="col-lg-12" style="text-align: center">
                    <i class='fa fa-fw fa-arrow-left'></i>
                    <?php
                    if ($total_std > 0) {
                       $cm->print_std_page($total_std, 'current', 'std_page');
                    }
                    ?>
                    <i class='fa fa-fw fa-arrow-right'></i>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</form>
