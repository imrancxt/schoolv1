
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
    <?php
    if(isset($class)){
        for($i=0;$i<count($class);$i++){
             if (isset($class_routine[$i]['time'])) {
                 if($i==0){
                     echo"<li data-target='#carousel-example-generic' data-slide-to='$i' class='active'></li>";
                 }
                 else{
                     echo"<li data-target='#carousel-example-generic' data-slide-to='$i'></li>";
                 }
             }
        }
    }
    ?>
    </ol>
    <!-- Indicators -->
    <!--<ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php
        include_once 'common_function.php';
        $cm = new common_function();
        if (isset($class)) {
            for ($i = 0; $i < count($class); $i++) {
                if (isset($class_routine[$i]['time'])) {
                    if ($i == 0) {
                        $cm->print_class_routine($class_routine[$i]['time'], $class_routine[$i]['period'], $class_routine[$i]['sub'], $class_routine[$i]['teacher'], "active", $class[$i]);
                    } else {
                        $cm->print_class_routine($class_routine[$i]['time'], $class_routine[$i]['period'], $class_routine[$i]['sub'], $class_routine[$i]['teacher'], "", $class[$i]);
                    }
                }
            }
        }
        ?>
    </div>

    <!-- Controls -->
    <?php
    if (isset($class_routine[0]['time'])){
        echo"<a class='left carousel-control' href='#carousel-example-generic' role='button' data-slide='prev'>
        <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>
        <span class='sr-only'>Previous</span>
    </a>
    <a class='right carousel-control' href='#carousel-example-generic' role='button' data-slide='next'>
        <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>
        <span class='sr-only'>Next</span>
    </a>";
    }
    ?>
</div>


<div class="row" id="test">

</div>
<div class="row" id="add_new_routine_content">
    <div class="col-lg-6">
        <button class="btn btn-success" onclick="addColumn('create_class_routine')">ADD CLASS TIME</button>
        <button class="btn btn-info" onclick="deleteColumn('create_class_routine')">DELETE CLASS TIME</button>
        <button class="btn btn-danger" onclick="class_break('create_class_routine')">BREAK</button>
    </div>

    <form class='alert_ajax_form' action='../admin/create_class_routine' method='POST' enctype='multipart/form-data'>
        <div class="col-lg-4">
            <select name="class" class='form-control'>
                <?php
                $class = array("SELECT CLASS", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                foreach ($class as $class) {
                    echo"<option>$class</option>";
                }
                ?>
            </select>
        </div>
        <div class="col-sm-2"> <button type="submit" class="btn btn-warning" style="width:100%">SUBMIT</button></div>
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="create_class_routine">
                    <thead>
                        <tr class="active">
                            <th>TIME</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="danger">
                            <td>PERIOD</td>
                        </tr>
                        <tr class="info">
                            <td>SAT</td>
                        </tr>
                        <tr class="info">
                            <td>SUN</td>
                        </tr>
                        <tr class="info">
                            <td>MON</td>
                        </tr>
                        <tr class='warning'>
                            <td>TUE</td>
                        </tr>
                        <tr class="success">
                            <td>WED</td>
                        </tr>
                        <tr class="success">
                            <td>THR</td>
                        </tr>
                        <tr class="success">
                            <td>FRI</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>

</div>

<script>
    // 2006-08-21 - Created
    // 2006-11-05 - Modified - head and body
    function addColumn(tblId)
    {
        var tblHeadObj = document.getElementById(tblId).tHead;
        for (var h=0; h<tblHeadObj.rows.length; h++) {
            var newTH = document.createElement('th');
            tblHeadObj.rows[h].appendChild(newTH);
            //newTH.innerHTML = '[th] row:' + h + ', cell: ' + (tblHeadObj.rows[h].cells.length - 1)
            newTH.innerHTML = "<input required class='form-control' placeholder='TIME' name='time[]'/>";
        }

        var tblBodyObj = document.getElementById(tblId).tBodies[0];
        for (var i=0; i<tblBodyObj.rows.length; i++) {
            var newCell = tblBodyObj.rows[i].insertCell(-1);
            //newCell.innerHTML = '[td] row:' + i + ', cell: ' + (tblBodyObj.rows[i].cells.length - 1)
            if(i==0){
                newCell.innerHTML = "<input class='form-control' placeholder='PERIOD' name='period[]' type='number' min='1' max='30'/>";
            }
            else{
                html1="<textarea required name='subject[]' class='form-control' placeholder='SUBJECT'></textarea>";
                html2="<input required name='teacher[]' class='form-control' placeholder='TEACHER'/>";
                newCell.innerHTML =html1+html2;
            }
                   
        }
    }
    function deleteColumn(tblId)
    {
        var allRows = document.getElementById(tblId).rows;
        for (var i=0; i<allRows.length; i++) {
            if (allRows[i].cells.length > 1) {
                allRows[i].deleteCell(-1);
            }
        }
    }
    function class_break(tblId){
        var tblHeadObj = document.getElementById(tblId).tHead;
        for (var h=0; h<tblHeadObj.rows.length; h++) {
            var newTH = document.createElement('th');
            tblHeadObj.rows[h].appendChild(newTH);
            //newTH.innerHTML = '[th] row:' + h + ', cell: ' + (tblHeadObj.rows[h].cells.length - 1)
            newTH.innerHTML = "<input required class='form-control' placeholder='TIME' name='time[]'/>";
        }

        var tblBodyObj = document.getElementById(tblId).tBodies[0];
        var brk=new Array("BREAK","BREAK","BREAK","BREAK","BREAK","BREAK","BREAK","BREAK");
        for (var i=0; i<tblBodyObj.rows.length; i++) {
            var newCell = tblBodyObj.rows[i].insertCell(-1);
            //newCell.innerHTML =brk[i];
            if(i==0){
                newCell.innerHTML = "<input rquired class='form-control' placeholder='PERIOD' name='period[]'/>";
            }
            else{
                newCell.innerHTML ="<div class='brk'>"+brk[i]+"</div><input required type='hidden' name='subject[]' value='break'/><input type='hidden' name='teacher[]' value=''/>";  
            }
                   
        }
    }
</script>
<style>
    .brk{
        background-color: red;
        height:88px;
        color:white;
        font-size: 30px;
        padding: 10px;
    }
    #add_new_routine_content{

    }
    .get_class_routine{
        background-color:#6666ff;
        border: 1px white solid;
        color: white;
        min-width: 37px;
        border-radius: 4px;
    }
    #specific_classroutine{

    }
</style>
<script>
    $(function () {
        $('.carousel').carousel({
            interval: 2000
        })
    })
</script>