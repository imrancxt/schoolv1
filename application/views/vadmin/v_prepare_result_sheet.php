
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-default"></div>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/prepare_result_sheet">PREPARE RESULT SHEET</a>
            </li>
            
           
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">Filter Result</div>
            <div class="panel-body">
                <div class="form-group">

   
    
        <select  name="class" id="get_class" class="select form-control"> 
            <?php
            $option = array("CLASS", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
            foreach ($option as $option) {
                echo"<option>$option</option>";
            }
            ?>
        </select>
   
    <div class="col-lg-12">
        <?php
        include_once 'common_function.php';
        $cm = new common_function();
        $cm->print_session();
        ?>
    </div>
                    <br>
    <div class="form-group">
        <button class="btn btn-default btn-raised" id="filter_current_student" status="current" style="width:100%;">FILTER</button>
    </div>
   
</div>
            </div>
            </div>
     </div>
    </div>




<br>
<div class="row">
    <div class="col-lg-12" id="course_content">

    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            
       
        <div class="table-responsive" id="current_student_tbl">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="active">
                        <th>IMAGE</th>
                        <th>NAME</th>
                        <th>STUDENT's ID</th>
                        <th>CLASS</th>
                        <th>ROLL</th>
                        <th>SECTION</th>
                        <th>PHONE</th>
                        <th>FATHER's NAME</th>
                        <th>MOTHER's NAME</th>                                
                        <th>MAKE RESULT SHEET</th>
                    </tr>
                </thead>
                <tbody id="std_tbl_content">
                    <?php
                    if (isset($id)) {
                        for ($i = 0; $i < count($id); $i++) {
                            $url1 = $GLOBALS['asset_url'] . "img/profile/student/$serial[$i].jpg";
                            $url2 = base_url() . "admin/student/$serial[$i]";
                            echo"<tr class='warning'>
                                           <td>
                                            <a href='$url2' target='_blank'> <img src='$url1' height='50px' width='50px'/></a>
                                           </td>
                                    <td><a href='$url2' target='_blank'>$student_name[$i]</a></td>
                                         <td> <a href='$url2' target='_blank'> $id[$i]</a></td>
                                           <td>$class[$i]</td>
                                            <td>$roll[$i]</td>
                                    <td>$section[$i]</td>
                                           <td>$student_phone[$i]</td>
                                           <td>$father_name[$i]</td>
                                           <td>$mother_name[$i]</td>
                                           <td> <button  class='make_result_sheet' title='$serial[$i]'>MAKE RESULT</button></td>
                                         </tr>";
                        }
                        echo"<input type='hidden' value='$student_class' id='student_class'/>";
                        echo"<input type='hidden' value='$student_session' id='student_session'/>";
                    }
                    ?>
                </tbody>
            </table>
            <div class="col-lg-12" style="text-align: center">
                <hr>
            </div>
            <div class="col-lg-12" style="text-align: center">
                <i class='fa fa-fw fa-arrow-left'></i>
                <?php
                if ($total_std>0) {
                    $cm->print_std_page($total_std, 'current', 'std_rslt_page');
                }
                ?>
                <i class='fa fa-fw fa-arrow-right'></i>
                <hr>
            </div>
        </div>
             </div>
    </div>
</div>