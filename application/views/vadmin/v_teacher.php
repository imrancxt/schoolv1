<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class='alert alert-warning'>
                <h4>
                    <i class='fa fa-fw fa-user'></i>.
                    TIMELINE OF 
                    <?php
                    if (isset($tname)) {
                        echo"$tname[0](TEACHER)";
                    }
                    ?>
                </h4>
            </div>

        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <ul class="timeline">
                <li>
                    <div class="timeline-badge info"><i class="fa fa-user-md"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">TEACHER'S PICTURE</h4>                            

                            <hr />
                        </div>
                        <div class="timeline-body">
                            <p>
                            <div id="pp_content">
                                <?php
                                if (isset($tserial)) {
                                    $img_url = $asset_url . "assets/images/photo-1.jpg";
                                    echo"<img class='media-object' src='$img_url' height='300px' width='100%'/>";
                                } else {
                                    echo"<img class='media-object' src='' height='200px' width='100%'/>";
                                }
                                ?>
                            </div>

                            </p>
                        </div>
                    </div>
                </li>

                <li class="timeline-inverted">
                    <div class="timeline-badge info"><i class="fa fa-user"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title"></h4>
                            TEACHER'S BASIC INFORMATION
                            <hr />
                            <div id="teacher_info_update_alert">
                            </div>
                        </div>
                        <div class="timeline-body">
                            <p>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    TEACHER'S INFORMATION
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <tbody>
                                            <?php
                                            if (isset($tserial)) {
                                                $name = array('', 'tname', 'tbdate', 'tjdate', 'tfname', 'tmname', 'tposition', 'tphone', 'temail', 'tsallary', 'taddress', 'prev_exp', 'about');
                                                $title = array("IDENTITY", "NAME", "BIRTH DATE", "JOINIG DATE", "FATHER'S NAME", "MOTHER'S NAME", "POSITION", "PHONE", "E-MAIL",
                                                    "SALLARY-SCALE", "ADDRESS", "EXPERIENCE", "ABOUT");
                                                $info = array($tid[0], $tname[0], $tbdate[0], $tjdate[0], $tfname[0],
                                                    $tmname[0], $tposition[0], $tphone[0], $temail[0], $tsallary[0], $taddress[0], $prev_exp[0], $tabout[0]);
                                                echo "<tr class='warning'><td>$title[0]</td><td>$info[0]</td></tr>";
                                                for ($i = 1; $i < 6; $i++) {
                                                    echo"<tr class='warning' ><td>$title[$i]</td><td><input name='$name[$i]' value='$info[$i]' class='form-control'/></td></tr>";
                                                }
                                                echo"<tr class='warning'><td>POSITION</td><td><select class='form-control' name='tposition'>";
                                                echo"<option>$tposition[0]</option>";
                                                if (isset($teachertype)) {
                                                    $tposition1 = array_values(array_diff($teachertype, array($tposition[0])));
                                                    foreach ($tposition1 as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                echo"</select></td></tr>";
                                                for ($i = 7; $i < 10; $i++) {
                                                    echo"<tr class='warning' ><td>$title[$i]</td><td><input name='$name[$i]' value='$info[$i]' class='form-control'/></td></tr>";
                                                }
                                                for ($i = 10; $i < 13; $i++) {
                                                    echo"<tr class='warning' ><td>$title[$i]</td><td><textarea name='$name[$i]' class='form-control'>$info[$i] </textarea></td></tr>";
                                                }
                                                /* for ($i = 7; $i < count($title); $i++) {
                                                  echo"<tr><td>$title[$i]</td><td><input value='$info[$i]' class='form-control'/></td></tr>";
                                                  } */
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            </p>
                        </div>
                    </div>
                </li>
                <li class="">
                    <div class="timeline-badge info"><i class="fa fa-trophy"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">ATTENDANCE SHEET </h4>

                            <hr />
                        </div>
                        <div class="timeline-body">
                            <p>
                            <form class='change_content_by_form' content="#att_sheet" action='<? echo base_url(); ?>admin/teacher_attendence_sheet' method='POST' enctype='multipart/form-data'>
                                <div class='alert alert-success' style="text-align: center">
                                    SELECT A MONTH:
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input name="month" type="month" class='form-control'>
                                        </div>
                                        <div class="col-lg-6">
                                            <button type="submit"  class="btn btn-default" style="width:100%">VIEW</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-success">
                                            <div style="text-align: center; margin-top: 20px" id="att_sheet">
                                                <div class="panel-heading">
                                                    <div class='alert alert-success'>
                                                        <?php
                                                        $today = date("F-Y");
                                                        echo"$today";
                                                        if (isset($tserial[0])) {
                                                            echo"<input name='tserial' type='hidden' value='$tserial[0]'/>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <?php
                                                    $date = date('t');
                                                    for ($i = 1; $i <= $date; $i++) {
                                                        if (isset($yatt["date$i"])) {
                                                            $title = date('D', strtotime($yatt["title$i"]));
                                                            echo"<div class='attny' title='$title Day<br>PRESENT'>$i</div>";
                                                        } else {
                                                            if (isset($natt["date$i"])) {
                                                                $title = date('D', strtotime($natt["title$i"]));
                                                                echo"<div class='attnn' title='$title Day<br>ABSENT'>$i</div>";
                                                            } else {
                                                                //$title=date("D",strtotime($name));
                                                                $date1 = date("Y-m-") . $i;
                                                                $title = date("D", strtotime($date1));
                                                                echo"<div class='attno' title='$title Day'>$i</div>";
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- STUDENT'S INFORMATION -->
<!--PARENT'S INFORMATION -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                EDUCATIONAL BACKGROUND
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="active">
                            <th>SERIAL</th>
                            <th>QUALIFICATION</th>
                            <th>PASSING YEAR</th>
                            <th>SPECIALIZATION</th>
                            <th>INSTITUTE</th>
                            <th>CGPA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($edu_serial)) {
                            for ($i = 0; $i < count($edu_serial); $i++) {
                                echo"<tr class='warning' >";
                                echo"<td>$i.</td><td>$edu_type[$i]</td><td>$pass_year[$i]</td><td>$specialization[$i]</td><td><pre>$institute[$i]</pre><td>$cgpa[$i]</td>";
                                echo"</tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

<div class="container">
    <div class="row">
        <br>
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    TOTAL PAID BY THIS TEACHER <?php echo"<strong>$total_loan</strong> TAKA AND DUE <strong>$total_loan_due</strong> TAKA" ?>

                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="active">
                                <th>PAID FOR</th>
                                <th>PAID AMOUNT</th>
                                <th>DUE AMOUNT</th>
                                <th>PAYMENT DATE</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($loan_serial)) {
                                for ($i = 0; $i < count($loan_serial); $i++) {
                                    echo"<tr class='warning' ><td>$loan_for[$i]</td><td>$loan_ammount[$i]</td>
                                             <td>$loan_due_amount[$i]</td><td>$loan_date[$i]</td>";

                                    //echo"<td><a href='javascript:stdcrdlt($pay_serial[$i])'>DELETE</a></td>";

                                    echo"</tr>";
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <br>
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    TOTAL PAID TO THIS TEACHER <?php echo"<strong>$total_paid</strong> TAKA AND DUE <strong>$total_sallary_due</strong> TAKA" ?>

                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="active">
                                <th>CHECK</th>
                                <th>PAID FOR</th>
                                <th>PAY AMOUNT</th>
                                <th>DUE AMOUNT</th>
                                <th>PAYMENT DATE</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($paid_serial)) {
                                for ($i = 0; $i < count($paid_serial); $i++) {
                                    echo"<tr class='warning' ><td>$sallary_for[$i]</td><td>$sallary_ammount[$i]</td>
                                         <td>$sallary_due_amount[$i]</td><td>$sallary_date[$i]</td>";
                                   
                                    //echo"<td><a href='javascript:stdcrdlt($pay_serial[$i])'>DELETE</a></td>";
                                   
                                    echo"</tr>";
                                }
                               
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>



<style>
    
</style>



