<style>
    .select_box {
        display: block;
        width: 100%;
        height: 34px;
        /*padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;*/
    }
</style>

<!-- /.row -->
<div class="row">
    <div class="col-lg-4">
    <div class="panel panel-default">
        <div class="panel-heading">Filter Student</div>
        <div class="panel-body">
    
         <div class="form-group">
        <select  name="class" id="get_class" class="select form-control">
            <?php
            $option = array("CLASS", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
            foreach ($option as $option) {
                echo"<option>$option</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <?php
        include_once 'common_function.php';
        $cm = new common_function();
        $cm->print_session();
        ?>
    </div>
    <div class="form-group">
        <button class="btn btn-default btn-raised" id="filter_current_student" status="current" style="width:100%;">FILTER</button>
    </div>
        </div>
            </div>   
    </div>
</div>
    
    
    
    
    <div class="col-lg-12">
        <div class="panel panel-default">
        <div class="panel-heading">Filter Student Data Table</div>
        <div class="panel-body">
        
        
        
        <form class='alert_ajax_form' action='../admin/addtocurrentstudent' method='POST' enctype='multipart/form-data'>
    <div class="row">
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" id="mark_unmark" style="width:113px; border-radius: 0px">MARK ALL</a>
             <button type="submit" class="btn btn-info">ADD TO CURRENT STUDENT</button>
             <div class="table-responsive" id="current_student_tbl">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="active">
                            <th>CHECK</th>
                            <th>NAME</th>
                            <th>STUDENT's ID</th>
                            <th>CLASS</th>
                            <th>ROLL</th>
                            <th>SECTION</th>
                            <th>PHONE NO</th>
                            <th>FATHER's NAME</th>
                            <th>MOTHER's Name</th>
                            <th>TUTION FEE SCALE</th>
                        </tr>
                    </thead>
                    <tbody id="std_tbl_content">
                        <?php
                        if (isset($id)) {
                            for ($i = 0; $i < count($id); $i++) {
                                $url = $asset_url. "img/profile/student/$serial[$i]";
                                $url2 = base_url() . "admin/student/$serial[$i]";
                                echo"<tr class='warning'>
                                           <td>
                                           <input class='toggle-one' unchecked type='checkbox' data-size='mini' name='serial[]' value='$serial[$i]'/>
                                           <a href='$url2'><img src='$url.jpg' height='50px' width='50px'/></a>
                                           </td>
                                        <td><a href='$url2' target='_blank'>$student_name[$i]</a></td>
                                           <td><a href='$url2'>$id[$i]</a></td>
                                           <td>$class[$i]</td>
                                           <td>$roll[$i]</td>
                                           <td>$section[$i]</td>
                                           <td>$student_phone[$i]</td>
                                           <td>$father_name[$i]</td>
                                           <td>$mother_name[$i]</td>
                                           <td>$tution_fee[$i]</td>
                                         </tr>";
                                //<td><pre>$parent_address[$i]</pre></td>
                            }
                            echo"<input type='hidden' value='$student_class' id='student_class'/>";
                            echo"<input type='hidden' value='$student_session' id='student_session'/>";
                        }
                        ?>
                    </tbody>
                </table>
                <div class="col-lg-12" style="text-align: center">
                    <hr>
                </div>
                <div class="col-lg-12" style="text-align: center">
                    <i class='fa fa-fw fa-arrow-left'></i>
                    <?php
                    if ($total_std > 0) {
                        $cm->print_std_page(count($total_std), 'old','std_page');
                    }
                    ?>
                    <i class='fa fa-fw fa-arrow-right'></i>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</form>
    </div>
</div>
</div>

