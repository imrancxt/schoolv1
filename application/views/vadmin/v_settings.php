

        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="<? echo base_url(); ?>admin/dashboard">DASHBOARD</a>
                    </li>
                    <li class="active">
                        <a href="<? echo base_url(); ?>admin/settings">SETTINGS</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    FEES,SALARY,TAXES TYPE.
                </div>
            </div>
            <div class="col-lg-3">
            </div>
            <div class="col-lg-6" id="alert_add_person_cost">
                <!-- <div class="alert alert-success">
                     <strong>Well done!</strong> You successfully read this important alert message.
                 </div>-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            STUDENT'S FEE<br> CATAGORIE
                        </h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/deletepersoncosttype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="option">
                                <?php
                                if (isset($studentdr)) {
                                    foreach ($studentdr as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="student"/>
                            <input type="hidden" name="cost_type" value="dr"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/addpersoncosttype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='FEE-TYPE' name="option"/>
                            <input type="hidden" name="person" value="student"/>
                            <input type="hidden" name="cost_type" value="dr"/>
                            <button type="submit"  person="student" class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            STUDENT'S SCHOLARSHIP<br> CATAGORIE
                        </h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/deletepersoncosttype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="option">
                                <?php
                                if (isset($studentcr)) {
                                    foreach ($studentcr as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="student"/>
                            <input type="hidden" name="cost_type" value="cr"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>

                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/addpersoncosttype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='SCHOLARSHIP-TYPE' name="option"/>
                            <input type="hidden" name="person" value="student"/>
                            <input type="hidden" name="cost_type" value="cr"/>
                            <button type="submit"  person="student" class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            TEACHER'S SALARY<br> CATAGORIE
                        </h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/deletepersoncosttype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="option">
                                <?php
                                if (isset($teachercr)) {
                                    foreach ($teachercr as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="teacher"/>
                            <input type="hidden" name="cost_type" value="cr"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>

                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/addpersoncosttype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='SALLARY-TYPE' name="option"/>
                            <input type="hidden" name="person" value="teacher"/>
                            <input type="hidden" name="cost_type" value="cr"/>
                            <button type="submit"  person="teacher" class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-success">

                    <div class="panel-heading">
                        <h4 class="panel-title">
                            TEACHER'S TAX<br> CATAGORIE
                        </h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/deletepersoncosttype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="option">
                                <?php
                                if (isset($teacherdr)) {
                                    foreach ($teacherdr as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="teacher"/>
                            <input type="hidden" name="cost_type" value="dr"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/addpersoncosttype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='TAX-TYPE' name="option"/>
                            <input type="hidden" name="person" value="teacher"/>
                            <input type="hidden" name="cost_type" value="dr"/>
                            <button type="submit"  person="teacher" class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-success">

                    <div class="panel-heading">
                        <h4 class="panel-title">
                           PAYMENT METHOD
                        </h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/deletepersoncosttype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="option">
                                <?php
                                if (isset($payment_method)) {
                                    foreach ($payment_method as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="payment_method"/>
                            <input type="hidden" name="cost_type" value="pm"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/addpersoncosttype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='PAYMENT-METHOD' name="option"/>
                            <input type="hidden" name="person" value="payment_method"/>
                            <input type="hidden" name="cost_type" value="pm"/>
                            <button type="submit"  class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-success">

                    <div class="panel-heading">
                        <h4 class="panel-title">
                            STAFF'S SALARY<br> CATAGORIE
                        </h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/deletepersoncosttype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="option">
                                <?php
                                if (isset($stuffcr)) {
                                    foreach ($stuffcr as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="stuff"/>
                            <input type="hidden" name="cost_type" value="cr"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/addpersoncosttype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='SALLARY-TYPE' name="option"/>
                            <input type="hidden" name="person" value="stuff"/>
                            <input type="hidden" name="cost_type" value="cr"/>
                            <button type="submit"  class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-success">

                    <div class="panel-heading">
                        <h4 class="panel-title">
                            STAFF'S TAX<br> CATAGORIE
                        </h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/deletepersoncosttype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="option">
                                <?php
                                if (isset($stuffdr)) {
                                    foreach ($stuffdr as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="stuff"/>
                            <input type="hidden" name="cost_type" value="dr"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>
                        <form class="change_content_by_form" content="#alert_add_person_cost" action='<? echo base_url(); ?>admin/addpersoncosttype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='TAX-TYPE' name="option"/>
                            <input type="hidden" name="person" value="stuff"/>
                            <input type="hidden" name="cost_type" value="dr"/>
                            <button type="submit"  class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-12">
                <div class="alert alert-info">
                    TEACHER AND STUFF CATAGORIES
                </div>
            </div>
            <div class="col-lg-3">
            </div>
            <div class="col-lg-6" id="alert_add_person_type">
                <!--<div class="alert alert-success">
                    <strong>Well done!</strong> You successfully read this important alert message.
                </div>-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">TEACHER'S STATUS</h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_type" action='<?php echo base_url(); ?>admin/deletepersonoccupationtype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="type">
                                <?php
                                if (isset($teacher_type)) {
                                    foreach ($teacher_type as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="teacher"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>
                        <form class="change_content_by_form" content="#alert_add_person_type" action='<? echo base_url(); ?>admin/addpersonoccupationtype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='STATUS' name="type"/>
                            <input type="hidden" name="person" value="teacher"/>
                            <button type="submit"  class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">STUFF STATUS</h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_type" action='<?php echo base_url(); ?>admin/deletepersonoccupationtype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="type">
                                <?php
                                if (isset($stuff_type)) {
                                    foreach ($stuff_type as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="stuff"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>
                        <form class="change_content_by_form" content="#alert_add_person_type" action='<?php echo base_url(); ?>admin/addpersonoccupationtype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='STATUS' name="type"/>
                            <input type="hidden" name="person" value="stuff"/>
                            <button type="submit"  class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">FATHER'S OCCUPATION</h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_type" action='<?php echo base_url(); ?>admin/deletepersonoccupationtype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="type">
                                <?php
                                if (isset($father_occupation)) {
                                    foreach ($father_occupation as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="father"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>
                        <form class="change_content_by_form" content="#alert_add_person_type" action='<?php echo base_url(); ?>admin/addpersonoccupationtype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='OCCUPATION' name="type"/>
                            <input type="hidden" name="person" value="father"/>
                            <button type="submit"  class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">MOTHER'S OCCUPATION</h4>
                    </div>
                    <div class="panel-body">
                        <form class="change_content_by_form" content="#alert_add_person_type" action='<?php echo base_url(); ?>admin/deletepersonoccupationtype' method='POST' enctype='multipart/form-data'>
                            <select class="form-control" name="type">
                                <?php
                                if (isset($mother_occupation)) {
                                    foreach ($mother_occupation as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" name="person" value="mother"/>
                            <button type="submit" class="btn btn-primary" style="width: 100%">DELETE</button>
                        </form>
                        <form class="change_content_by_form" content="#alert_add_person_type" action='<?php echo base_url(); ?>admin/addpersonoccupationtype' method='POST' enctype='multipart/form-data'>
                            <input required class="form-control" placeholder='OCCUPATION' name="type"/>
                            <input type="hidden" name="person" value="mother"/>
                            <button type="submit"  class="btn btn-info" style="width: 100%">ADD NEW</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<style>
    .add_new_dr,.add_new_cr{
        min-height: 32px;
        color: white;
        background-color:#9999ff;
        border: 1px solid #9999ff;
        border-radius: 2px;
    }
    .add_new_dr:hover,.add_new_cr:hover{
        background-color:#9966ff;
        border-radius: 5px;
    }
</style>
