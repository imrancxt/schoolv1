
<div class="row">
    <div class="col-lg-12">
        <div class='alert alert-success'>
            <h4>
                <i class="fa fa-user"></i> ADMIN INFO
                <i class="fa fa-users"></i><a href="javascript:add_new_admin()">ADD NEW ADMIN</a>
                <i class="fa fa-users"></i><a href="javascript:change_theme_color()">CHANGE THEME COLOR</a>
            </h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <div class='alert alert-succes'>
            <img class="media-object" src="<?php echo $GLOBALS['asset_url']; ?>img/noimage.jpg" width="100%" height="210px"/>
        </div>
    </div>
    <div class="col-lg-4">
        <form class='alert_ajax_form' action='../admin2/change_admin_info' method='POST' enctype='multipart/form-data'>
            <div class='alert alert-success'>
                <div class="form-group">
                    <input placeholder="ADMIN NAME" required name="new_admin" class='form-control' value="<?php
if (isset($admin_name)) {
    echo $admin_name;
}
?>"/>
                </div>
                <div class="form-group">
                    <input required name="old_password" placeholder="OLD PASSWORD" class='form-control'/>
                </div>
                <div class="form-group">
                    <input name="new_password" placeholder="NEW PASSWORD" class='form-control'/>
                </div>
                <input type="checkbox"  name="chng_name" value="val"/><label>ADMIN NAME</label> <input type="checkbox" name="chng_password" value="val"/><label>ADMIN PASSWORD</label> 
                <button type="submit" class="btn btn-info" style="width:100%">UPDATE</button>
            </div>
        </form>
    </div>
    <div class="col-lg-3" id="add_new_admin_content" style="">
        <form class='alert_ajax_form' action='../admin2/add_new_admin' method='POST' enctype='multipart/form-data'>
            <div class='panel panel-success'>
                <div class="panel-heading">
                    <h4>
                        ADD NEW ADMIN
                    </h4> 
                </div>
                <div class="panel-body">
                    <div>
                        <div class="form-group">
                            <input placeholder="ADMIN NAME" required name="admin" class='form-control'/>
                        </div>
                        <div class="form-group">
                            <input required name="password" placeholder="PASSWORD" class='form-control'/>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info" style="width:100%">CREATE NEW ADMIN</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-3" id="change_theme" style="">
        <div class='panel panel-success'>
            <div class="panel-heading">
                <h4>THEME COLOR</h4>
            </div>
            <div class="panel-body">
                <form class='alert_ajax_form'  action='../admin2/change_theme_color' method='POST' enctype='multipart/form-data'>
                    <div class="form-group">
                        <input class="form-control"  type="color" name="sidetop_color" value="<?php echo$_SESSION['sidetop_color']; ?>"/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="color" name="body_color" value="<?php echo $_SESSION['body_color']; ?>"/>
                    </div>
                    <div class="form-group">
                        <button  class="btn btn-info" style="width:100%">CHANGE COLOR</button>
                    </div>
                </form>
                </body>
            </div>
        </div>
    </div>
</div>
<div class="alert alert-success">
    <div class="row">
        <form class='alert_ajax_form' action='../admin2/update_school_info' method='POST' enctype='multipart/form-data'>
            <div class="col-lg-2">SCHOOL NAME</div>
            <div class="col-lg-3"><input required class="form-control"  name="school_name" value="<?php if (isset($school_name)) {
                               echo"$school_name";
                           } ?>"/></div>
            <div class="col-lg-2">SCHOOL ADDRESS</div>
            <div class="col-lg-3"><input required class="form-control"  name="school_address" value="<?php if (isset($school_address)) {
                               echo"$school_address";
                           } ?>"/></div>
            <div class="col-lg-2">
                <button  class="btn btn-info" style="width:100%">UPDATE</button>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <i class="fa fa-clock-o"></i> LOGGED TIME <input required id="date1" type="date"/>&nbsp<input required id="date2" type="date"/>
                    <button class="update_due_btn" id="view_admin_log_btn">VIEW</button>
                </h4>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="admin_log_tbl">
                    <thead>
                        <tr>
                            <th>SERIAL</th>
                            <th>LOGIN TIME</th>
                            <th>LOGOUT TIME</th>
                            <th>SPENT TIME</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($log_serial)) {
                            for ($i = 0; $i < count($log_serial); $i++) {
                                if (date_create($logout_time[$i]) && date_create($login_time[$i])) {
                                    $date1 = date_create($logout_time[$i]);
                                    $date2 = date_create($login_time[$i]);
                                    $date = date_diff($date1, $date2);
                                    $spent = $date->format("%h hour:%i min:%s sec");
                                } else {
                                    $spent = "UNKNOWN";
                                }
                                echo"<tr><td>$i</td><td>$login_time[$i]</td><td>$logout_time[$i]</td><td>$spent</td></tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>