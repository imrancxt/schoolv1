
<div class="row">
    <div class="col-lg-12">
        <div class='alert alert-warning'>
            <h4>
                <i class='fa fa-fw fa-user'></i>.
                TIMELINE OF 
                <?php
                if (isset($stname)) {
                    echo"$stname[0](STUFF)";
                }
                ?>
            </h4>
        </div>

    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <ul class="timeline">
            <li>
                <div class="timeline-badge info"><i class="fa fa-user-md"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">STUFF'S PICTURE</h4>                            

                        <hr />
                    </div>
                    <div class="timeline-body">
                        <p>
                        <div id="pp_content">
                            <?php
                            if (isset($stserial)) {
                                $img_url = $GLOBALS['asset_url'] . "img/profile/stuff/{$stserial[0]}.jpg";
                                echo"<img class='media-object' src='$img_url' height='300px' width='100%'/>";
                            } else {
                                echo"<img class='media-object' src='' height='200px' width='100%'/>";
                            }
                            ?>
                        </div>
                        <form class='change_content_by_form' content="#pp_content" action='../admin/update_stuff_pro_pic' method='POST' enctype='multipart/form-data'>
                            <input id="file-4" type="file" class="file" name="image">
                            <?php
                            if (isset($stserial)) {
                                echo"<input type='hidden' value='$stserial[0]' name='serial'/>";
                            }
                            ?>
                        </form>
                        </p>
                    </div>
                </div>
            </li>

            <li class="timeline-inverted">
                <div class="timeline-badge info"><i class="fa fa-user"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title"></h4>
                        STUFF'S BASIC INFORMATION
                        <hr />
                    </div>
                    <div id="stuff_info_update_alert">
                    </div>
                    <div class="timeline-body">
                        <p>
                        <form class='change_content_by_form' content="#stuff_info_update_alert" action='<? echo base_url(); ?>admin/update_stuff_basic_info' method='POST' enctype='multipart/form-data'>
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <button type="submit" class="btn btn-success">UPDATE</button> STUFF'S INFORMATION
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <tbody>
                                            <?php
                                            if (isset($stserial)) {
                                                $name = array('', 'stname', 'stbdate', 'stjdate', 'stfname', 'stmname', 'stposition', 'stphone', 'stemail', 'stsallary', 'staddress', 'prev_exp', 'about');
                                                $title = array("IDENTITY", "NAME", "BIRTH DATE", "JOINIG DATE", "FATHER'S NAME", "MOTHER'S NAME", "POSITION", "PHONE", "E-MAIL",
                                                    "SALLARY-SCALE", "ADDRESS", "EXPERIENCE", "ABOUT");
                                                $info = array($stid[0], $stname[0], $stbdate[0], $stjdate[0], $stfname[0],
                                                    $stmname[0], $stposition[0], $stphone[0], $stemail[0], $stsallary[0], $staddress[0], $prev_exp[0], $stabout[0]);
                                                echo"<tr class='warning'><td>$title[0]</td><td><label>$info[0]</label></td></tr>";
                                                for ($i = 1; $i < 6; $i++) {
                                                    echo"<tr class='warning' ><td>$title[$i]</td><td><input name='$name[$i]' value='$info[$i]' class='form-control'/></td></tr>";
                                                }

                                                echo"<tr class='warning'><td>POSITION</td><td><select class='form-control' name='stposition'>";
                                                echo"<option>$stposition[0]</option>";
                                                if (isset($stufftype)) {
                                                    $stposition1 = array_values(array_diff($stufftype, array($stposition[0])));
                                                    foreach ($stposition1 as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                echo"</select></td></tr>";

                                                for ($i = 7; $i < 10; $i++) {
                                                    echo"<tr class='warning'><td>$title[$i]</td><td><input name='$name[$i]' value='$info[$i]' class='form-control'/></td></tr>";
                                                }
                                                for ($i = 10; $i < 13; $i++) {
                                                    echo"<tr class='warning'><td>$title[$i]</td><td><textarea name='$name[$i]' class='form-control'>$info[$i] </textarea></td></tr>";
                                                }
                                                /* for ($i = 7; $i < count($title); $i++) {
                                                  echo"<tr><td>$title[$i]</td><td><input value='$info[$i]' class='form-control'/></td></tr>";
                                                  } */
                                                echo"<input type='hidden' value='$stserial[0]' name='serial'/>";
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                        </p>
                    </div>
                </div>
            </li>
            <li class="">
                <div class="timeline-badge info"><i class="fa fa-trophy"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">ATTENDANCE SHEET </h4>

                        <hr />
                    </div>
                    <div class="timeline-body">
                        <p>
                        <form class='change_content_by_form' content="#att_sheet" action='<? echo base_url(); ?>admin/stuff_attendence_sheet' method='POST' enctype='multipart/form-data'>
                            <div class='alert alert-success' style="text-align: center">
                                SELECT A MONTH: 
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input name="month" type="month" class='form-control'>
                                    </div>
                                    <div class="col-lg-6">
                                        <button type="submit"  class="btn btn-default" style="width:100%">VIEW</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-success">
                                        <div style="text-align: center; margin-top: 20px" id="att_sheet">
                                            <div class="panel-heading">
                                                <div class='alert alert-success'>
                                                    <?php
                                                    $today = date("F-Y");
                                                    echo"$today";
                                                    if (isset($stserial[0])) {
                                                        echo"<input name='stserial' type='hidden' value='$stserial[0]'/>";
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <?php
                                                $date = date('t');
                                                for ($i = 1; $i <= $date; $i++) {
                                                    if (isset($yatt["date$i"])) {
                                                        $title = date('D', strtotime($yatt["title$i"]));
                                                        echo"<div class='attny' title='$title Day<br>PRESENT'>$i</div>";
                                                    } else {
                                                        if (isset($natt["date$i"])) {
                                                            $title = date('D', strtotime($natt["title$i"]));
                                                            echo"<div class='attnn' title='$title Day<br>ABSENT'>$i</div>";
                                                        } else {
                                                            //$title=date("D",strtotime($name));
                                                            $date1 = date("Y-m-") . $i;
                                                            $title = date("D", strtotime($date1));
                                                            echo"<div class='attno' title='$title Day'>$i</div>";
                                                        }
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </p>
                    </div>
                </div>
            </li>
        </ul>
    </div>

</div>

<!--PARENT'S INFORMATION -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                EDUCATIONAL BACKGROUND&nbsp<button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">ADD MORE</button>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="active">
                            <th>SERIAL</th>
                            <th>QUALIFICATION</th>
                            <th>PASSING YEAR</th>
                            <th>SPECIALIZATION</th>
                            <th>INSTITUTE</th>
                            <th>CGPA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($edu_serial)) {
                            for ($i = 0; $i < count($edu_serial); $i++) {
                                echo"<tr class='warning'>";
                                echo"<td>$i.</td><td>$edu_type[$i]</td><td>PASSING YEAR</td><td>$specialization[$i]</td><td><pre>$institute[$i]</pre><td>$cgpa[$i]</td>";
                                echo"</tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <form class='alert_ajax_form' action='../admin2/update_stuff_transaction' method='POST' enctype='multipart/form-data'>
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    TOTAL PAID TO THIS STUFF <?php echo"$total_paid TAKA AND DUE $total_sallary_due TAKA" ?>
                    <i class="fa fa-unlock"></i>
                    <input name='amount' class="myfield1" placeholder="DUE AMOUNT"/><button class="update_due_btn">UPDATE</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="active">
                                <th>CHECK</th>
                                <th>PAID FOR</th>
                                <th>PAID AMOUNT</th>
                                <th>DUE AMOUNT</th>
                                <th>PAYMENT DATE</th>
                                <th>INSERTION DATE</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($paid_serial)) {
                                for ($i = 0; $i < count($paid_serial); $i++) {
                                    echo"<tr class='warning'><td><input type='checkbox' name='serial[]' value='$paid_serial[$i]'/></td><td>$sallary_for[$i]</td><td>$sallary_ammount[$i]</td>
                                         <td>$sallary_due_amount[$i]</td><td>$sallary_date[$i]</td><td>$paid_insertion[$i]</td></tr>";
                                }
                                echo"<input type='hidden' value='cr' name='trans_type'/>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <form class='alert_ajax_form' action='<? echo base_url(); ?>admin2/update_stuff_transaction' method='POST' enctype='multipart/form-data'>
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    TOTAL PAID BY THIS STUFF<?php echo"$total_paidby TAKA AND $total_paidby_due TAKA" ?>
                    <i class="fa fa-unlock"></i>
                    <input name='amount' class="myfield1" placeholder="DUE AMOUNT"/><button class="update_due_btn">UPDATE</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="active">
                                <th>CHECK</th>
                                <th>PAID FOR</th>
                                <th>PAID AMOUNT</th>
                                <th>DUE AMOUNT</th>
                                <th>PAYMENT DATE</th>
                                <th>RECEIPT</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($paidby_serial)) {
                                for ($i = 0; $i < count($paidby_serial); $i++) {
                                    echo"<tr class='warning'><td><input type='checkbox' name='serial[]' value='$paidby_serial[$i]'/></td><td>$pay_for[$i]</td><td>$pay_ammount[$i]</td>
                                         <td>$pay_due_amount[$i]</td><td>$pay_date[$i]</td><td><a serial='$paidby_serial[$i]' class='btn get_st_drreceipt' data-toggle='modal' data-target='.bs-example-modal-lg'>GET RECEIPT</a></td></tr>";
                                }
                                echo"<input type='hidden' value='dr' name='trans_type'/>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>





<div class="row">
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Add More Educational Background</h4>
                </div>
                <div class="modal-body">
                    <form class='change_content_by_form' content="#contentc" action='<? echo base_url(); ?>admin2/addmorestedu' method='POST' enctype='multipart/form-data'>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td>QUALIFICATION</td>
                                        <td>
                                            <select class='form-control' name="qualification[]">
                                                <?
                                                $info = array('SSC', 'HSC', 'BSC(hons)', 'BA(hons)', 'BCOM');
                                                foreach ($info as $option) {
                                                    echo"<option>$option</option>";
                                                }
                                                ?>
                                            </select>  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>PASSING YEAR</td><td><input required name="passing_year[]" type="date" placeholder="PASSING YEAR" class='form-control'/></td>
                                    </tr>
                                    <tr>
                                        <td>SPECIALIZATION</td>
                                        <td>
                                            <input required name="specialization[]" placeholder="SPECIALIZATION" class='form-control'/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>INSTITUTE</td><td><textarea required name="institute[]" placeholder="INSTITUTE" class='form-control'></textarea></td>
                                    </tr>
                                    <tr>
                                        <td>CGPA</td><td><input required name="gpa[]" placeholder="GPA" class='form-control'/></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php
                            if (isset($stserial)) {
                                echo"<input type='hidden' value='$stid[0]' name='stid'/>";
                            }
                            ?>
                        </div>
                        <button type="submit" class="btn btn-primary">ADD</button> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <div id="contentc">

                        </div>
                    </form>
                </div>

                <div class="modal-footer">


                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-header">
                 <h4 class="modal-title" id="myModalLabel">PRINT THIS RECEIPT</h4>
            </div>
            <div id="print_content">
               
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" id="print_this_page">PRINT</button>
            </div>
        </div>
    </div>
</div>
</div>
<script src='<?php echo $GLOBALS['asset_url']; ?>js/customize.js'>
    </script>
<script>
    $(document).ready(function(){
        $(".get_st_drreceipt").click(function(){
              $("#print_content").html("PLEASE WAIT...");
            serial=$(this).attr("serial");
            page="admin2/get_stuff_dr_receipt/"+serial;
            change_content("#print_content",page);
        });
        
    })
</script>

