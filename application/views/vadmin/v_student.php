
<div class="row">
    <div class="col-lg-12">
        <div class='alert alert-warning'>
            <h4>
                <i class='fa fa-fw fa-user'></i>
                TIMELINE OF 
                <?php
                if (isset($std_name)) {

                    echo"$std_name[0](STUDENT)";
                }
                ?>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" style="float: right">
                    UPGRADE
                </button>
            </h4>
        </div>

    </div>
</div>


<div class="row">
    <div class="col-lg-4">
        <div id="pp_content">
            <?php
            if (isset($std_serial)) {
                $img_url = $GLOBALS['asset_url'] . "img/profile/student/{$std_serial[0]}.jpg";
                echo"<img class='media-object' src='$img_url' height='200px' width='100%'/>
                         ";
            } else {
                echo"<img class='media-object' src='' height='200px' width='100%'/>";
            }
            ?>
        </div>
        <form class='change_content_by_form' content="#pp_content" action='<? echo base_url(); ?>admin/update_std_pro_pic' method='POST' enctype='multipart/form-data'>
            <input id="file-4" type="file" class="file" name="image">
            <?php
            if (isset($std_serial)) {
                echo"<input type='hidden' value='$std_serial[0]' name='serial'/>";
            }
            ?>
        </form>
    </div>
    <div class="col-lg-8">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="active">
                        <th>EXAM TERM</th>
                        <th>GPA</th>
                        <th>TOTAL MARKS</th>
                        <th>COMMENT</th>
                        <th>VIEW</th>
                        <th>PRINT RESULT</th>

                    </tr>
                </thead>
                <?php
                if (isset($exam_serial)) {
                    for ($i = 0; $i < count($exam_serial); $i++) {
                        $info = "$std_serial[0]-$term[$i]";
                        $url = base_url() . "admin2/stdprintresult/$info";
                        echo"<tr><td>$exam_term[$i]</td><td>$exam_gpa[$i]</td><td>$exam_marks[$i]</td><td>$exam_comment[$i]</td>
                                <td><button class='get_details_result' title='$info'>DETAILS</button></td>
                                <td><a href='$url'><button class='btn btn-info'>PRINT</button></a></td>
                                </tr>";
                    }
                }
                ?>
            </table>
        </div>
        <div class="row">
            <div class="col-lg-12" id="exam_details_content">

            </div>
        </div>
    </div>

</div>




<div class="row">
    <div class="col-lg-12">
        <ul class="timeline">
            <li>
                <div class="timeline-badge info"><i class="fa fa-user"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">STUDENT'S INFO</h4>                            

                        <hr />
                        <div id="student_info_update_alert">
                        </div>
                    </div>
                    <div class="timeline-body">
                        <p>
                        <form class='change_content_by_form' content="#student_info_update_alert" action='<? echo base_url(); ?>admin/update_student_basic_info' method='POST' enctype='multipart/form-data'>
                            <div class="panel panel-yellow">
                                <div class="panel-body">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><i class="fa fa-long-arrow-right"></i>
                                            <button type="submit" class="btn btn-info">UPDATE</button> STUDENT'S INFORMATION
                                        </h4>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <tbody>
                                                <?php
                                                if (isset($std_serial)) {
                                                    $title = array("IDENTITY", "CLASS", "SECTION", "SESSION", "NAME", "BIRTH DATE", "PREVIOUS INSTITUTE", "PHONE NO", "E-MAIL", "ADDRESS");
                                                    $info = array($std_id[0], $class[0],
                                                        $section[0], $session[0], $std_name[0], $birth_date[0], $prev_ins[0], $std_phone[0], $std_email[0], $std_address[0]);
                                                    $name = array("", '', '', '', 'student_name', 'birth_date', 'prev_ins', 'student_phone', 'student_email', 'student_address');
                                                    for ($i = 0; $i < 4; $i++) {
                                                        echo"<tr class='warning'><td>$title[$i]</td><td><label>$info[$i]</label></td></tr>";
                                                    }
                                                    for ($i = 4; $i < count($title); $i++) {
                                                        echo"<tr class='warning'><td>$title[$i]</td><td><input name='$name[$i]' value='$info[$i]' class='form-control'/></td></tr>";
                                                    }


                                                    echo"<input type='hidden' value='$std_serial[0]' name='serial'/>";
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </p>
                    </div>
                </div>
            </li>

            <li class="timeline-inverted">
                <div class="timeline-badge success"><i class="fa fa-users"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">PARENT'S INFO</h4>
                        <hr />
                    </div>
                    <div id="parent_info_update_alert">
                    </div>
                    <div class="timeline-body">
                        <p>
                        <form class='change_content_by_form' content="#parent_info_update_alert" action='<? echo base_url(); ?>admin/update_parent_info' method='POST' enctype='multipart/form-data'>
                            <div class="panel panel-yellow">
                                <div class="panel-body">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><i class="fa fa-long-arrow-right"></i>
                                            <button type="submit" class="btn btn-info">UPDATE</button> PARENT'S INFORMATION
                                        </h4>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <tbody>
                                                <?php
                                                if (isset($std_serial)) {
                                                    $title2 = array("IDENTITY", "FATHER'S NAME", "MOTHER'S NAME", "RELIGION", "PHONE NO", "E-MAIL", "ADDRESS", "FATHER'S OCCUPATION", "MOTHER'S OCCUPATION");
                                                    $name = array('', 'father_name', 'mother_name', 'religion', 'parent_phone', 'parent_email', 'parent_address', 'father_occupation', 'mother_occupation');
                                                    $info2 = array($par_id[0], $father_name[0], $mother_name[0], $religion[0], $par_phone[0],
                                                        $par_email[0], $par_address[0], $father_occupation[0], $mother_occupation[0]);
                                                    for ($i = 0; $i < 1; $i++) {
                                                        echo"<tr class='warning'><td>$title2[$i]</td><td><label>$info2[$i]</label></td></tr>";
                                                    }
                                                    for ($i = 1; $i < count($title2); $i++) {
                                                        echo"<tr class='warning'><td>$title2[$i]</td><td><input name='$name[$i]' value='$info2[$i]' class='form-control'/></td></tr>";
                                                    }
                                                    echo"<input type='hidden' name='serial' value='$par_serial[0]'/>";
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </p>
                    </div>
                </div>
            </li>
            <li class="">
                <div class="timeline-badge info"><i class="fa fa-xing-square"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">TAKEN COURSES </h4>
                        <hr />
                    </div>
                    <div class="timeline-body">
                        <p>
                        <div class="panel panel-yellow">
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <?php
                                        if (isset($std_serial)) {
                                            $url = base_url() . "admin/studentcourse/$std_serial[0]";
                                            echo"<a href='$url'><button class='btn btn-success' style=''>COURSES</button></a>";
                                        }
                                        ?>
                                    </h4>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <tr class="active">
                                            <th>SERIAL</th>
                                            <th>COURSE TITLE</th>
                                            <th>COURSE NAME</th>
                                        </tr>
                                        <?php
                                        if (isset($std_course_serial)) {
                                            for ($i = 0; $i < count($std_course_serial); $i++) {
                                                echo"<tr><td>$i</td>
                                        <td>$std_course_title[$i]</td>
                                        <td>$std_course_name[$i]</td>
                                        </tr>";
                                            }
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </p>
                    </div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-badge warning"><i class="fa fa-trophy"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">ATTENDANCE SHEET </h4>
                        <hr />
                    </div>
                    <div class="timeline-body">
                        <p>
                        <form class='change_content_by_form' content="#att_sheet" action='<? echo base_url(); ?>admin/student_attendence_sheet' method='POST' enctype='multipart/form-data'>
                            <div class='alert alert-success' style="text-align: center">
                                SELECT A MONTH:
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input name="month" type="month" class='form-control'>
                                    </div>
                                    <div class="col-lg-6">
                                        <button type="submit"  class="btn btn-default" style="width:100%">VIEW</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-success">
                                        <div style="text-align: center; margin-top: 20px" id="att_sheet">
                                            <div class="panel-heading">
                                                <div class='alert alert-success'>
                                                    <?php
                                                    $today = date("F-Y");
                                                    echo"$today";
                                                    if (isset($std_serial[0])) {
                                                        echo"<input name='std_serial' type='hidden' value='$std_serial[0]'/>";
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <?php
                                                $date = date('t');
                                                for ($i = 1; $i <= $date; $i++) {
                                                    if (isset($yatt["date$i"])) {
                                                        $title = date('D', strtotime($yatt["title$i"]));
                                                        echo"<div class='attny' title='$title Day<br>PRESENT'>$i</div>";
                                                    } else {
                                                        if (isset($natt["date$i"])) {
                                                            $title = date('D', strtotime($natt["title$i"]));
                                                            echo"<div class='attnn' title='$title Day<br>ABSENT'>$i</div>";
                                                        } else {
                                                            $date1 = date("Y-m-") . $i;
                                                            $title = date("D", strtotime($date1));
                                                            echo"<div class='attno' title='$title Day'>$i</div>";
                                                        }
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </p>
                    </div>
                </div>
            </li>
        </ul>
    </div>

</div>


<!-- STUDENT'S INFORMATION -->
<!--PARENT'S INFORMATION -->
<!-- /.row -->

<br><br>
<div class="row">
    <form class='alert_ajax_form' action='<? echo base_url(); ?>admin2/update_student_transaction' method='POST' enctype='multipart/form-data'>
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    TUTION FEE FROM THIS STUDENT <?php echo"$fee_total TAKA AND DUE $fee_due_total" ?>
                    <i class="fa fa-unlock"></i>
                    <input name="amount" class="myfield1" placeholder="DUE AMOUNT"/><button class="update_due_btn">UPDATE</button>
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal2" style="float: right">ADD MORE</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="active">
                                <th>CHECK</th>
                                <th>REASON</th>
                                <th>PAID AMOUNT</th>
                                <th>DUE AMOUNT</th>
                                <th>FEE DATE</th>
                                <th>GET RECEIPT</th>
                                <th>EDIT</th>
                                <th>DELETE</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($fee_serial)) {

                                for ($i = 0; $i < count($fee_serial); $i++) {
                                    echo"<tr class='warning'>";
                                    echo"<td><input type='checkbox' name='serial[]' value='$fee_serial[$i]'/></td>";
                                    echo"<td>$fee_for[$i]</td>";
                                    echo"<td>$fee_amount[$i]</td>";
                                    echo"<td>$fee_due_amount[$i]</td>";
                                    echo"<td>$fee_date[$i]</td>";
                                    echo"<td><a serial='$fee_serial[$i]' class='btn get_student_drreceipt' data-toggle='modal' data-target='.bs-example-modal-lg'>RECEIPT</a></td>";
                                    //echo"<td><a href='javascript:stddredit($fee_serial[$i])'  data-toggle='modal' data-target='#myModal4'>EDIT</a></td>";
                                    echo"<td><button class='stddredit' serial='$fee_serial[$i]' data-toggle='modal' data-target='#myModal4'>EDIT</button></td>";
                                    //echo"<td><a href='javascript:stddrdlt($fee_serial[$i])'>DELETE</a></td>";
                                    echo"<td><button class='stddrdlt' serial='$fee_serial[$i]' data-toggle='modal' data-target='#myModal5'>DELETE</button></td>";
                                    echo"</tr>";
                                }
                                echo"<input type='hidden' value='dr' name='trans_type'/>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <form class='alert_ajax_form' action='<? echo base_url(); ?>admin2/update_student_transaction' method='POST' enctype='multipart/form-data'>
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    COST FOR THIS STUDENT<?php echo"$pay_total TAKA AND DUE $pay_due_total TAKA" ?>
                    <i class="fa fa-unlock"></i>
                    <input name='amount' class="myfield1" placeholder="DUE AMOUNT"/><button class="update_due_btn">UPDATE</button>
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal3" style="float: right">ADD MORE</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="active">
                                <th>CHECK</th>
                                <th>REASON</th>
                                <th>PAID AMOUNT</th>
                                <th>DUE AMOUNT</th>
                                <th>PAYMENT DATE</th>
                                <th>EDIT</th>
                                <th>DELETE</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($pay_for)) {
                                for ($i = 0; $i < count($pay_for); $i++) {
                                    $edit_url = base_url() . "admin2/editstdcr/$pay_serial[$i]";
                                    echo"<tr class='warning'>";
                                    echo"<td><input type='checkbox' name='serial[]' value='$pay_serial[$i]'/></td>";
                                    echo"<td>$pay_for[$i]</td>";
                                    echo"<td>$pay_amount[$i]</td>";
                                    echo"<td>$pay_due_amount[$i]</td>";
                                    echo"<td>$pay_date[$i]</td>";
                                    // echo"<td><a  href='javascript:stdcrreceipt($pay_serial[$i])'>RECEIPT</a></td>";
                                    // 
                                    //echo"<td><a href='javascript:stdcredit($pay_serial[$i])'>EDIT</a></td>";
                                    echo"<td><button  class='stdcredit' serial='$pay_serial[$i]' data-toggle='modal' data-target='#myModal4'>EDIT</button></td>";
                                    //echo"<td><a href='javascript:stdcrdlt($pay_serial[$i])'>DELETE</a></td>";
                                    echo"<td><button class='stdcrdlt' serial='$pay_serial[$i]' data-toggle='modal' data-target='#myModal5'>DELETE</button></td>";
                                    echo"</tr>";
                                }
                                echo"<input type='hidden' value='cr' name='trans_type'/>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">ARE YOU SURE TO UPGRADE THIS STUDENT?</h4>
                    </div>
                    <div class="modal-body">
                        <p style="color:#fa8a52">If you Upgrade This Student There Will Be Create A New Student Database.<br> 
                            Click Yes To Create A New Student Database
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">NO</button>
                        <?
                        if (isset($std_name)) {
                            $url = base_url() . "admin2/upgrade_student/$std_serial[0]";
                            echo"<a href='$url'><button type='button' class='btn btn-success'>YES</button></a>";
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">ADD FEE</h4>
                    </div>

                    <form class='change_content_by_form' content="#content1" action='<? echo base_url(); ?>admin2/addstddr' method='POST' enctype='multipart/form-data'>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr class="warning">
                                        <td>DATE</td>
                                        <td>
                                            <input required type="date" class="form-control" name="fee_date">
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>REASON</td>
                                        <td>
                                            <select class="form-control" name="fee_for">
                                                <?php
                                                if (isset($studentdr)) {
                                                    foreach ($studentdr as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            PAYMENT METHOD
                                        </td>
                                        <td>
                                            <select class="form-control" name="fee_payment_method">
                                                <?php
                                                echo"<option>PAYMENT METHOD</option>";
                                                if (isset($payment_method)) {
                                                    foreach ($payment_method as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            PAID AMOUNT
                                        </td>
                                        <td>
                                            <input required value="0" name="fee_amount" class="form-control" placeholder="PAID AMOUNT">
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            DUE AMOUNT
                                        </td>
                                        <td>
                                            <input required value="0" name="fee_due_amount" class="form-control" placeholder="DUE AMOUNT">
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="alert alert-success" id="content1">

                            </div>
                        </div>
                        <?php
                        if (isset($std_serial)) {
                            echo"<input type='hidden' name='std_serial' value='$std_serial[0]'/>";
                        }
                        ?>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">ADD</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">ADD SCHOLARSHIP</h4>
                        </div>
                        <form class='change_content_by_form' content="#content2" action='<? echo base_url(); ?>admin2/addstdcr' method='POST' enctype='multipart/form-data'>
                            <div class="modal-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <tr class="warning">
                                            <td>DATE</td>
                                            <td>
                                                <input required   type="date" class="form-control" name="pay_date">
                                            </td>
                                        </tr>
                                        <tr class="warning">
                                            <td>REASON</td>
                                            <td>
                                                <select class="form-control" name="pay_for">
                                                    <?php
                                                    if (isset($studentcr)) {
                                                        foreach ($studentcr as $option) {
                                                            echo"<option>$option</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="warning">
                                            <td>PAYMENT METHOD</td>
                                            <td>
                                                <select class="form-control" name="sch_payment_method">
                                                    <?php
                                                    echo"<option>PAYMENT METHOD</option>";
                                                    if (isset($payment_method)) {
                                                        foreach ($payment_method as $option) {
                                                            echo"<option>$option</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="warning">
                                            <td>PAID AMOUNT</td>
                                            <td>
                                                <input required value="0" name="pay_amount" class="form-control" placeholder="PAID AMOUNT">
                                            </td>
                                        </tr>
                                        <tr class="warning">
                                            <td>
                                                DUE AMOUNT
                                            </td>
                                            <td>
                                                <input required value="0" name="pay_due_amount" class="form-control" placeholder="DUE AMOUNT">
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="alert alert-success" id="content2">

                                </div>
                            </div>
                            <?php
                            if (isset($std_serial)) {
                                echo"<input type='hidden' name='std_serial' value='$std_serial[0]'/>";
                            }
                            ?>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">ADD</button> 
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">DO YOU WANT TO UPDATE?</h4>
                </div>
                <div class="modal-body" id="change_modal">
                    PLEASE WAIT...
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">DELETING</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning" id="change_modal2">
                        PLEASE WAIT...
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-header">
                 <h4 class="modal-title" id="myModalLabel">PRINT THIS RECEIPT</h4>
            </div>
            <div id="print_content">
               
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" id="print_this_page">PRINT</button>
            </div>
        </div>
    </div>
</div>
</div>

<script src='<?php echo $GLOBALS['asset_url']; ?>js/customize.js'>
</script>
<script>
    $(document).ready(function(){
        $(".stddredit").click(function(){
            serial=$(this).attr("serial");
            page="admin2/getstddrdata/"+serial;
            change_content("#change_modal",page);
        });
        $(".stddrdlt").click(function(){
            serial=$(this).attr("serial");
            page="admin2/dltstddrdata/"+serial;
            change_content("#change_modal2",page);
        });
        
        $(".stdcredit").click(function(){
            serial=$(this).attr("serial");
            page="admin2/getstdcrdata/"+serial;
            change_content("#change_modal",page);
        });
        $(".stdcrdlt").click(function(){
            serial=$(this).attr("serial");
            page="admin2/dltstdcrdata/"+serial;
            change_content("#change_modal2",page);
        });
        $(".get_student_drreceipt").click(function(){
            $("#print_content").html("PLEASE WAIT...");
            serial=$(this).attr("serial");
            page="admin2/student_dr_receipt/"+serial;
            change_content("#print_content",page);
        });
         
    })
</script>