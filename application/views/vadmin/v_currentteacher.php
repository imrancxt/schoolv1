<style>
    .select_box {
        display: block;
        width: 100%;
        height: 34px;
        /*padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;*/
    }
    .std_info{
        /* background-color:#ccc;
         padding: 2px;*/
    }
</style>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<? echo base_url(); ?>admin/dashboard">DASHBOARD</a>
            </li>
            <li class="">
                TEACHERS
            </li>
            <li class="active">
                <a href="<? echo base_url(); ?>admin/currentteacher">CURRENT TEACHER</a>
            </li>
          
        </ol>
    </div>
</div>

<!-- /.row -->
 <div class="row">
        <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">Teacher</div>
            <div class="panel-body">
                <div class="form-group">
           <select  name="class" id="get_class" class="select form-control">   
              <?php
                    echo"<option>ALL</option>";
                    if (isset($teachertype)) {
                        foreach ($teachertype as $option) {
                            echo"<option>$option</option>";
                        }
                    }
                    ?>
             </select>
              <div class="form-group">
        <button class="btn btn-default btn-raised" id="filter_current_student" status="current" style="width:100%;">FILTER</button>
    </div>
        
    </div>
                </div>
        </div>
        </div>
     </div>



<br>
<form class="change_content_by_form" content="#content" action='../admin/add_teacher_info' method='POST' enctype='multipart/form-data'>
    <div class="row">
        <div class="col-lg-3">
            <label class="btn btn-default active" style="width:100%; border-radius: 0px">
                <!--<input class="click_radio" attr="fee" type='radio' name='option' id='' value='add_sallary'>-->
                <input class="click_radio" type='radio' name='option' name='option' value='add_sallary' data-toggle="modal" data-target="#exampleModal1" data-whatever="@mdo">
                ADD SALARY TO TEACHER
            </label>
            <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">ADD SALARY TO TEACHER</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr class="warning">
                                        <td>DATE</td>
                                        <td> <input  type="date" class="form-control" name="sallary_date"></td>
                                    </tr>
                                    <tr class="warning">
                                        <td>REASON</td>
                                        <td>
                                            <select class="form-control" name="sallary_for">
                                                <?php
                                                if (isset($teachercr)) {
                                                    foreach ($teachercr as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>PAID BY</td>
                                        <td>
                                            <select class="form-control" name="sallary_payment_method">
                                                <?php
                                                echo"<option>PAYMENT METHOD</option>";
                                                if (isset($payment_method)) {
                                                    foreach ($payment_method as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>

                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            PAID
                                        </td>
                                        <td>
                                            <input value="0" name="sallary_ammount" class="form-control" placeholder="PAID AMOUNT">
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>
                                            DUE
                                        </td>
                                        <td>
                                            <input value="0" name="sallary_due_amount" class="form-control" placeholder="DUE AMOUNT">
                                        </td>
                                    </tr>

                                </table>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">ADD</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-3">
            <label class="btn btn-info" style="width:100%; border-radius: 0px">
                <!--<input class="click_radio" attr="pay" type='radio' name='option' id='' value='add_loan'>ADD AMOUNT FROM TEACHER</label>-->
                <input  class="click_radio" type='radio' name='option' value='add_loan' data-toggle="modal" data-target="#exampleModal2" data-whatever="@mdo">
                ADD AMOUNT FROM TEACHER
            </label>
            <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">ADD AMOUNT FROM TEACHER</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr class="warning">
                                        <td>DATE</td>
                                        <td><input type="date" class="form-control" name="loan_date"></td>
                                    </tr>
                                    <tr class="warning">
                                        <td>REASON</td>
                                        <td>
                                            <select class="form-control" name="loan_for">
                                                <?php
                                                if (isset($teacherdr)) {
                                                    foreach ($teacherdr as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>PAID BY</td>
                                        <td>
                                            <select class="form-control" name="loan_payment_method">
                                                <?php
                                                echo"<option>PAYMENT METHOD</option>";
                                                if (isset($payment_method)) {
                                                    foreach ($payment_method as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="warning">
                                        <td>PAID</td>
                                        <td><input value="0" name="loan_ammount" class="form-control" placeholder="PAID AMOUNT"></td>
                                    </tr>
                                    <tr class="warning">
                                        <td>DUE</td>
                                        <td>
                                            <input value="0" name="loan_due_amount" class="form-control" placeholder="DUE AMOUNT">
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">ADD</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-3">
            <label class="btn btn-info" style="width:100%; border-radius: 0px">
               <!-- <input class="click_radio" attr="attendence" type='radio' name='option' id='' value='add_attendence'>UPDATE ATTENDANCE SHEET</label>-->
                <input  class="click_radio" type='radio' name='option' value='add_attendence' data-toggle="modal" data-target="#exampleModal3" data-whatever="@mdo">
                UPDATE ATTENDANCE SHEET
            </label>

            <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">UPDATE ATTENDANCE SHEET</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr class="warning">
                                        <td>DATE</td>
                                        <td><input name="attendence_date"  type="date" class="form-control"></td>
                                    </tr>
                                    <tr class="warning">
                                        <td>STATUS</td>
                                        <td>
                                            <select class="form-control" name="attendence_status">
                                                <?php
                                                $option = array("YES", "NO");
                                                foreach ($option as $option) {
                                                    echo"<option>$option</option>";
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">UPDATE</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-lg-3">
            <label class="btn btn-info" style="width:100%; border-radius: 0px">
               <!-- <input class="click_radio" attr="attendence" type='radio' name='option' id='' value='add_attendence'>UPDATE ATTENDANCE SHEET</label>-->
                <input  class="click_radio" type='radio' name='option' value='trash' data-toggle="modal" data-target="#exampleModal4" data-whatever="@mdo">
                TRASH
            </label>
             <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">TRASH THESE TEACHERS </h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-warning">
                                ARE YOU SURE?
                            </div>
                            
                        </div>
                         <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">YES</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                        </div>
                    </div>
                </div>
             </div>
         </div>
    </div>
    <br>
    <div id="content">
        
    </div>
    <div class="row">
        <div class="col-lg-12">
             <a class="btn btn-success" id="mark_unmark" style="width:113px; border-radius: 0px">MARK ALL</a>
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="current_teacher_tbl">
                    
                    <thead>
                        <tr class="active">
                            <th>CHECK</th>
                            <th>NAME</th>
                            <th>TEACHER's ID</th>
                            <th>PHONE</th>
                            <th>ADDRESS</th>
                            <th>FATHER's NAME</th>
                            <th>MOTHER's</th>
                            <th>SALARY SCALE</th>
                            <th>POSITION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($tid)) {
                            for ($i = 0; $i < count($tid); $i++) {
                                $url1 = $GLOBALS['asset_url'] . "img/profile/teacher/$tserial[$i].jpg";
                                $url2 = base_url() . "admin/teacher/$tserial[$i]";
                                echo"<tr class='warning'>
                                           <td><input class='toggle-one' unchecked type='checkbox' data-size='mini' name='serial[]' value='$tserial[$i]'/>
                                            <a href='$url2' target='_blank'><img src='$url1' height='50px' width='50px'/></a>
                                           </td>
                                           <td><a href='$url2' target='_blank'>$tname[$i]</a></td>
                                           <td><a href='$url2' target='_blank'>$tid[$i]</a></td>
                                           <td>$tphone[$i]</td>
                                           <td><pre>$taddress[$i]</pre></td>
                                           <td>$tfname[$i]</td>
                                           <td>$tmname[$i]</td>
                                           <td>$tsallary[$i]</td>
                                           <td>$tposition[$i]</td>
                                         </tr>";
                            }
                        }
                        ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</form>
