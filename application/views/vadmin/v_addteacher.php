
<!-- Page Heading -->
<form class='alert_ajax_form' action='<?php echo base_url(); ?>admin/add_new_teacher' method='POST' enctype='multipart/form-data'>
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>  <a href="<? echo base_url() ?>admin/dashboard">DASHBOARD</a>
                </li>
                <li class="">
                    TEACHERS
                </li>
                <li class="active">
                    <a href="<?php echo base_url(); ?>admin/addteacher">ADD NEW</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-6">
            <button type="submit" class="btn btn-info" style="width: 100%">SUBMIT</button>
        </div>
        <div class="col-lg-6">
            <button type="reset" class="btn btn-info" style="width: 100%">RESET</button>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                BASIC INFORMATION
            </ol>
        </div>
    </div>
    <div class="row">
        <!-- teachers'S INFORMATION -->
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <?php
                        $title = array("IDENTITY", "NAME", "BIRTH DATE", "FATHER'S NAME", "MOTHER'S NAME", "PHONE NUMBER", "E-MAIL", "SALAERY");
                        $name = array('tid', 'tname', 'tbdate', 'tfname', 'tmname', 'tphone', 'temail', 'tsallary');
                        for ($i = 0; $i < 2; $i++) {
                            echo"<tr><td>$title[$i]</td><td><input required name='$name[$i]' placeholder='$title[$i]' class='form-control'/></td></tr>";
                        }
                        echo"<tr><td>$title[2]</td><td><input name='$name[2]' placeholder='$title[2]' class='form-control' type='date'/></td></tr>";
                        echo"<tr><td>JOINING DATE</td><td><input name='tjdate' placeholder='JOINING DATE' class='form-control' type='date'/></td></tr>";
                        for ($i = 3; $i < 8; $i++) {
                            echo"<tr><td>$title[$i]</td><td><input required name='$name[$i]' placeholder='$title[$i]' class='form-control'/></td></tr>";
                        }
                        echo"<tr><td>ADDRESS</td><td><textarea name='taddress' placeholder='ADDRESS' class='form-control'></textarea></td></tr>";
                        echo"<tr><td>POSITION</td><td>
                              <select class='form-control' name='tposition'>";
                        if (isset($teachertype)) {
                            foreach ($teachertype as $option) {
                                echo"<option>$option</option>";
                            }
                        }
                        echo"</select>
                              </td></tr>";
                        echo"<tr><td>PREVIOUS EXPERIENCES</td><td><textarea name='prev_exp' placeholder='PREVIOUS EXPERIENCE' class='form-control'></textarea></td></tr>";
                        echo"<tr><td>ABOUT</td><td><textarea name='about' placeholder='ABOUT' class='form-control'></textarea></td></tr>";
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                EDUCATIONAL BACKGROUND <a href="javascript:add_educational_back()"><i class="fa fa-arrow-down"></i>ADD EDUCATION</a>
            </ol>
        </div>
    </div>
    <!--<div class="row">
        <div class="col-lg-4">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td>QUALIFICATION</td><td>SSC</td>
                        </tr>
                        <tr>
                            <td>PASSING YEAR</td><td><input name="ssc_pass_year" type="date" placeholder="PASSING YEAR" class='form-control'/></td>
                        </tr>
                        <tr>
                            <td>SPECIALIZATION</td>
                            <td>
                                <select name="ssc_specialization" class='form-control'>
                                    <option>SCIENCE</option>
                                    <OPTION>ARTS</option>
                                    <option>COMMERCE</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>INSTITUTE</td><td><textarea name="ssc_institute" placeholder="INSTITUTE" class='form-control'></textarea></td>
                        </tr>
                        <tr>
                            <td>CGPA</td><td><input name="ssc_cgpa" placeholder="CGPA" class='form-control'/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td>QUALIFICATION</td><td>HSC</td>
                        </tr>
                        <tr>
                            <td>PASSING YEAR</td><td><input name="hsc_pass_year" type="date" placeholder="PASSING YEAR" class='form-control'/></td>
                        </tr>
                        <tr>
                            <td>SPECIALIZATION</td>
                            <td>
                                <select name="hsc_specialization" class='form-control'>
                                    <option>SCIENCE</option>
                                    <OPTION>ARTS</option>
                                    <option>COMMERCE</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>INSTITUTE</td><td><textarea name="hsc_institute" placeholder="INSTITUTE" class='form-control'></textarea></td>
                        </tr>
                        <tr>
                            <td>CGPA</td><td><input name="hsc_cgpa" placeholder="CGPA" class='form-control'/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td>QUALIFICATION</td><td>BSC</td>
                        </tr>
                        <tr>
                            <td>PASSING YEAR</td><td><input name="bsc_pass_year" type="date" placeholder="PASSING YEAR" class='form-control'/></td>
                        </tr>
                        <tr>
                            <td>SPECIALIZATION</td>
                            <td>
                                <select name="bsc_specialization" class='form-control'>
                                    <option>SCIENCE</option>
                                    <OPTION>ARTS</option>
                                    <option>COMMERCE</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>INSTITUTE</td><td><textarea name="bsc_institute" placeholder="INSTITUTE" class='form-control'></textarea></td>
                        </tr>
                        <tr>
                            <td>CGPA</td><td><input name="bsc_cgpa" placeholder="CGPA" class='form-control'/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>-->
    <div class="row" id="education_back_row">
        <div class="col-lg-4">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td>QUALIFICATION</td>
                            <td>
                                <select class='form-control' name="qualification[]">
                                    <?
                                    $info = array('SSC', 'HSC', 'BSC(hons)', 'BA(hons)', 'BCOM');
                                    foreach ($info as $option) {
                                        echo"<option>$option</option>";
                                    }
                                    ?>
                                </select>  
                            </td>
                        </tr>
                        <tr>
                            <td>PASSING YEAR</td><td><input name="passing_year[]" type="date" placeholder="PASSING YEAR" class='form-control'/></td>
                        </tr>
                        <tr>
                            <td>SPECIALIZATION</td>
                            <td>
                                <input name="specialization[]" placeholder="SPECIALIZATION" class='form-control'/>
                            </td>
                        </tr>
                        <tr>
                            <td>INSTITUTE</td><td><textarea name="institute[]" placeholder="INSTITUTE" class='form-control'></textarea></td>
                        </tr>
                        <tr>
                            <td>CGPA</td><td><input name="gpa[]" placeholder="GPA" class='form-control'/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-6">
            <button type="submit" class="btn btn-info" style="width: 100%">SUBMIT</button>
        </div>
        <div class="col-lg-6">
            <button type="reset" class="btn btn-info" style="width: 100%">RESET</button>
        </div>

    </div>
</form>


