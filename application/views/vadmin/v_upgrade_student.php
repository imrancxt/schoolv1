
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url(); ?>admin/dashboard">DASHBOARD</a>
                    </li>
                    <li class="active">
                        <a href="<?php echo base_url(); ?>admin2/upgrade_student">UPGRADE</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div id="pp_content">
                    <?php
                    if (isset($std_serial)) {
                        $img_url =$GLOBALS['asset_url']. "img/profile/student/{$std_serial[0]}.jpg";
                        echo"<img class='media-object' src='$img_url' height='200px' width='100%'/>
                         ";
                    } else {
                        echo"<img class='media-object' src='' height='200px' width='100%'/>";
                    }
                    ?>
                </div>
            </div>
            <div class="col-lg-9">

                <form class='alert_ajax_form' action='<?php echo base_url(); ?>admin/add_new_student' method='POST' enctype='multipart/form-data'>
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-info" style="width:100%">Submit</button>
                        </div>
                        <div class="col-lg-6">
                            <button type="reset" class="btn btn-info" style="width:100%">Reset</button>
                        </div>

                    </div><br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="current_student_tbl">
                                    <?php
                                    $type = array('text', 'text', 'text', 'text', 'date');
                                    $value = array($std_name[0], $prev_ins[0], $std_phone[0], $std_email[0], $birth_date[0]);
                                    $sfname = array('student_name', 'prev_ins', 'student_phone', 'student_email', 'birth_date');
                                    $sfplaceholder = array("STUDENT's NAME", "PREVIOUS INSTITUTE", "PHONE NUMBER", 'E-MAIL', 'BIRTH DATE');
                                    for ($i = 0; $i < count($sfname); $i++) {
                                        echo"<tr><td>$sfplaceholder[$i]</td><td><input value='$value[$i]' required type='$type[$i]' name='$sfname[$i]' class='form-control' placeholder='$sfplaceholder[$i]'></td></tr>";
                                    }
                                    echo"<tr><td>ADDRESS</td><td><textarea name='student_address' class='form-control' rows='3' placeholder='ADDRESS'>$std_address[0]</textarea></td></tr>";
                                    ?>
                                    <tr>
                                        <td>
                                            DEPARTMENT
                                        </td>
                                        <td>
                                            <select name='dept' class="form-control">
                                                <?php
                                                $value = array("SCIENCE", "ARTS", "COMMERCE");
                                                for ($i = 0; $i < count($value); $i++) {
                                                    echo"<option>$value[$i]</option>";
                                                    // echo"<label class='radio-inline'>
                                                    //<input type='radio' name='dept' id='dept$i' value='$value[$i]' checked>$value[$i]</label>";
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>SESSION</td>
                                        <td>
                                            <select name="year" class="form-control">
                                                <?php
                                                $option = array("2014", "2015", "2016", "2016", "2017", "2018");
                                                foreach ($option as $option) {
                                                    $session = $option + 1;
                                                    echo"<option>$option-$session</option>";
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            ROLL NO
                                        </td>
                                        <td>
                                            <input required name="roll" class="form-control" placeholder="ROLL NO"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            CLASS
                                        </td>
                                        <td>
                                            <select class="form-control" name="class">
                                                <?php
                                                $option = array("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                                                foreach ($option as $option) {
                                                    echo"<option>$option</option>";
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            SECTION
                                        </td>
                                        <td>
                                            <select class="form-control" name="section">
                                                <?php
                                                $option = array("A", "B", "C", "D", "E", "F");
                                                foreach ($option as $option) {
                                                    echo"<option>$option</option>";
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MONTHLY FEE</td>
                                        <td>
                                            <input required name="tution_fee" class="form-control" placeholder="MONTHLY TUTION FEE" value="<? echo $std_tution_fee[0] ?>"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <?php
                                    $paname = array("father_name", "mother_name", "religion", "parent_phone", "parent_email");
                                    $value2 = array($father_name[0], $mother_name[0], $religion[0], $par_phone[0], $par_email[0]);
                                    $paplaceholder = array("FATHERS'S NAME", "MOTHER'S NAME", "RELIGION", "PHONE NUMBER", "E-MAIL");
                                    for ($i = 0; $i < count($paname); $i++) {
                                        echo"<tr><td>$paplaceholder[$i]</td><td>
                                <div class='form-group'>
                                <input value='$value2[$i]' required type='text' name='$paname[$i]' class='form-control' placeholder='$paplaceholder[$i]'/>
                                </div>
                                </td></tr>";
                                    }
                                    ?>
                                    <tr>
                                        <td>ADDRESS</td>
                                        <td>
                                            <textarea name="parent_address" class="form-control" rows="3" placeholder="ADDRESS"><? echo $par_address[0]; ?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            FATHER'S OCCUPATION
                                        </td>
                                        <td>
                                            <select  name='father_occupation' class="form-control">
                                                <?php
                                                echo"<option>$father_occupation[0]</option>";
                                                if (isset($fatheroccupation)) {
                                                    $new_focp = array_values(array_diff($fatheroccupation, (array) $father_occupation[0]));
                                                    foreach ($new_focp as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                    //print_r($new_focp);
                                                //
                                        }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            MOTHER'S OCCUPATION
                                        </td>
                                        <td>
                                            <select name='mother_occupation' class="form-control">
                                                <?php
                                                echo"<option>$mother_occupation[0]</option>";
                                                if (isset($motheroccupation)) {
                                                    $new_mocp = array_values(array_diff($motheroccupation, (array) $mother_occupation[0]));
                                                    foreach ($new_mocp as $option) {
                                                        echo"<option>$option</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-info" style="width:100%">Submit</button>
                        </div>
                        <div class="col-lg-6">
                            <button type="reset" class="btn btn-info" style="width:100%">Reset</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
<script>
    /* $('#file-4').on('fileselectnone', function() {
        alert('Huh! You selected no files.');
    });
    $('#file-4').on('filebrowse', function() {
        alert('File browse clicked for #file-4');
    });
    $(document).ready(function() {
        $("#test-upload").fileinput({
            'showPreview' : false,
            'allowedFileExtensions' : ['jpg', 'png','gif'],
            'elErrorContainer': '#errorBlock'
        });
    });*/
</script>


