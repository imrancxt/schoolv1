<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

         <title>Admin Panel</title>
        <?
        $GLOBALS['asset_url'] = "http://localhost/school/";
        ?>
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/tooltip.css" rel="stylesheet">
        <link href="../css/sb-admin.css" rel="stylesheet">

        <link href="../css/plugins/morris.css" rel="stylesheet">

        <link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../css/fileinput.css" rel="stylesheet">
        <link href="../css/customize.css" rel="stylesheet">
        <link href="../css/datepicker.css" rel="stylesheet">
        <link href="../css/toggle.css" rel="stylesheet">
        
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/admin.js"></script>
        <script src="../js/tooltip.js"></script>
        <script src="../js/fileinput.js"></script>
        <script src='../js/plugins/morris/raphael.min.js'></script>
        <script src='../js/plugins/morris/morris.js'></script>
        <script src='../js/print.js'></script>
        <script src='../js/datepicker.js'></script>
        <script src='../js/toggle.js'></script>
       
    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                     <a class="navbar-brand" href="<?php echo base_url(); ?>admin/dashboard"> <? echo $_SESSION['school_name'];?></a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <form class='change_content_by_form' content=".container-fluid" action='../admin/search2' method='POST' enctype='multipart/form-data'>
                            <input name="search_query" id="search_query" class="form-control" type="search" placeholder="WHAT YOU WANT" style="margin-top:9px;"/>
                        </form>
                        <!-- <button id="search_button" class="btn btn-info" style="margin-top: 9px;margin-left: 5px;">SEARCH&nbsp<i class="fa fa-search"></i></button> -->
                    </li>
                    <li class="dropdown">
                        <button id="search_button" class="btn btn-info" style="margin-top: 9px;margin-left: 5px;">Search&nbsp<i class="fa fa-search"></i></button> 
                        <!--<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>-->
                    </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa "></i>Have Fun <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                             <li>
                                TOOLS
                            </li>
                            <li class="divider"></li>
                             
                            <li>
                                <a href="javascript:have_fun(2)"><i class="fa fa-fw fa-smile-o"></i>Solar System</a>
                            </li>
                            <li>
                                <a href="javascript:have_fun(3)"><i class="fa fa-fw fa-smile-o"></i>Periodic Table</a>
                            </li>
                            
                            <li>
                                <a href="javascript:have_fun(4)"><i class="fa fa-fw fa-smile-o"></i> Calculator</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                GAMES
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="javascript:have_fun(1)"><i class="fa fa-fw fa-smile-o"></i>Snake </a>
                            </li>
                            <li>
                                <a href="javascript:have_fun(6)"><i class="fa fa-fw fa-smile-o"></i>Pyramid</a>
                            </li>
                            <li>
                                <a href="javascript:have_fun(7)"><i class="fa fa-fw fa-smile-o"></i>Break-Bricks</a>
                            </li>
                            <li>
                                <a href="javascript:have_fun(8)"><i class="fa fa-fw fa-smile-o"></i>Ping-Pong </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>Admin <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="../admin2/admin_profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                            </li>
           
                            <li class="divider"></li>
                            <li>
                                <a href="../login/log_out"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="ctive">
                            <a href="../admin/dashboard"><i class="fa fa-fw fa-dashboard"></i>Home</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i>Student<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="../admin/currentstudent">Current</a>
                                </li>
                                <li>
                                    <a href="../admin/oldstudent">Previous</a>
                                </li>
                                <li>
                                    <a href="../admin/addstudent">Add New</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i>Teacher<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo2" class="collapse">
                                <li>
                                    <a href="../admin/currentteacher">Current</a>
                                </li>
                                <li>
                                    <a href="../admin/oldteacher">Previous</a>
                                </li>
                                <li>
                                    <a href="../admin/addteacher">Add New</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fa fa-fw fa-arrows-v"></i>Stuff<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo3" class="collapse">
                                <li>
                                    <a href="../dmin/currentstuff">Current</a>
                                </li>
                                <li>
                                    <a href="../admin/oldstuff">Previous</a>
                                </li>
                                <li>
                                    <a href="../admin/addstuff">Add New</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="../admin/classroutine"><i class="fa fa-fw fa-angle-right"></i>Class Routine</a>
                        </li>
                        <li>
                            <a href="../admin/transaction"><i class="fa fa-fw fa-angle-right"></i>Transaction</a>
                        </li>
                        <li>
                            <a href="../admin/courses"><i class="fa fa-fw fa-angle-right"></i>Courses</a>
                        </li>
                        <li>
                            <a href="../admin/prepare_result_sheet"><i class="fa fa-fw fa-angle-right"></i>Generate Result </a>
                        </li>
                         <li>
                                <a href="../admin2/admin_profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                            </li>
                        <li>
                                <a href="../login/log_out"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                         <!--<li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-arrows-v"></i>HAVE FUN<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo4" class="collapse">
                                <li>
                                    <a href="<?php //echo base_url(); ?>admin2/solarsystem">SOLAR SYSTEM</a>
                                </li>
                                <li>
                                    <a href="<?php //echo base_url(); ?>admin2/periodictable">PERIODIC TABLE</a>
                                </li>
                                <li>
                                    <a href="<?php //echo base_url(); ?>admin2/calculator">CALCULATOR</a>
                                </li>
                                <li>
                                    <a href="<?php //echo base_url(); ?>admin2/game">GAME</a>
                                </li>
                               
                            </ul>
                        </li>-->
                       
                   
                      

                    </ul>

                </div>
                <!-- /.navbar-collapse -->
            </nav>
            <div id="core_container">
            <div id="page-wrapper">

                <div class="container-fluid" >


