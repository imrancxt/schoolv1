<style>
    .select_box {
        display: block;
        width: 100%;
        height: 34px;
        /*padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;*/
    }
    .std_info{
        background-color:#ccc;
        padding: 2px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="">
                TEACHERS
            </li>
            <li class="active">
                <a href="../admin/oldteacher">OLD TEACHER</a>
            </li>

        </ol>
    </div>
</div>

<!-- /.row -->
<div class="row">
        <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">Teacher</div>
            <div class="panel-body">
                <div class="form-group">
           <select  name="class" id="get_class" class="select form-control">   
              <?php
                    echo"<option>ALL</option>";
                    if (isset($teachertype)) {
                        foreach ($teachertype as $option) {
                            echo"<option>$option</option>";
                        }
                    }
                    ?>
             </select>
              <div class="form-group">
        <button class="btn btn-default btn-raised" id="filter_current_student" status="current" style="width:100%;">FILTER</button>
    </div>
        
    </div>
                </div>
        </div>
        </div>
     </div>


<form class='alert_ajax_form' action='../admin/addtocurrentteacher' method='POST' enctype='multipart/form-data'>
    
    <br>
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" id="mark_unmark" style="border-radius: 0px">MARK ALL</a>
            <button type="submit" class="btn btn-info">ADD TO CURRENT TEACHER</button>
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="current_teacher_tbl">
                    <thead>
                        <tr class="active">
                            <th>CHECK</th>
                            <th>NAME</th>
                            <th>TEACHER's ID</th>
                            <th>PHONE</th>
                            <th>ADDRESS</th>
                            <th>FATHER's NAME</th>
                            <th>MOTHER's</th>
                            <th>SALARY SCALE</th>
                            <th>POSITION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($tid)) {
                            for ($i = 0; $i < count($tid); $i++) {
                                $url1 = $GLOBALS['asset_url'] . "img/profile/teacher/$tserial[$i].jpg";
                                $url2 = base_url() . "admin/teacher/$tserial[$i]";
                                echo"<tr class='warning'>
                                           <td><input class='toggle-one' unchecked type='checkbox' data-size='mini' name='serial[]' value='$tserial[$i]'/>
                                            <a href='$url2' target='_blank'><img src='$url1' height='50px' width='50px'/></a>
                                           </td>
                                           <td><a href='$url2' target='_blank'>$tname[$i]</a></td>
                                           <td><a href='$url2' target='_blank'>$tid[$i]</a></td>
                                           <td>$tphone[$i]</td>
                                           <td><pre>$taddress[$i]</pre></td>
                                           <td>$tfname[$i]</td>
                                           <td>$tmname[$i]</td>
                                           <td>$tsallary[$i]</td>
                                           <td>$tposition[$i]</td>
                                         </tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
