<script src='../js/plugins/morris/raphael.min.js'></script>
        <script src='../js/plugins/morris/morris.js'></script>
<style>
    .carousel,
    .item,
    .active {
        height: 100%;
    }

    .carousel-inner {
        height: 100%;
    }
    .fill {
        width: 100%;
        height:100%;
        background-position: center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
    #myCarousel{

    }
</style>

<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                INCOME-COST CHART
            </div>
            <div class="panel-body">
                <div id="morris-bar-chart2"></div>
                <div class="text-right">
                    <a href="../admin/transaction">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                PEOPLE CHART
            </div>
            <div class="panel-body">
                <div id="morris-donut-chart"></div>
                <div class="text-right">
                    <a href="../admin/currentstudent">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                SESSION-STUDENT CHART
            </div>
            <div class="panel-body">
                <div id="morris-bar-chart"></div>
                <div class="text-right">
                    <a href="../admin/courses">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                STUDENTS
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr class="warning">
                        <th>IMAGE</th>
                        <th>NAME</th>
                        <th>CLASS</th>
                        <th>ROLL</th>
                    </tr>
                    <?php
                    if (isset($student_info['serial'])) {
                        for ($i = 0; $i < count($student_info['serial']); $i++) {
                            $url ="../img/profile/student/{$student_info['serial'][$i]}.jpg";
                            $url2 ="../admin/student/{$student_info['serial'][$i]}";

                            echo"<tr class='active'><td><a href='$url2'><img height='50px' width='50px' src='$url'/></a></td>
                        <td><a href='$url2'>{$student_info['student_name'][$i]}</a></td><td>{$student_info['class'][$i]}</td>
                        <td>{$student_info['roll'][$i]}</td></tr>";
                        }
                    }
                    ?>
                </table>
            </div>
            <div class="text-right">
                <a href="../admin/currentstudent">View More <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                TEACHERS
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr class="warning">
                        <th>IMAGE</th>
                        <th>NAME</th>
                        <th>POSITION</th>
                        <th>ADDRESS</th>
                    </tr>
                    <?php
                    if (isset($teacher_info['tserial'])) {
                        for ($i = 0; $i < count($teacher_info['tserial']); $i++) {
                            $url ="../img/profile/teacher/{$teacher_info['tserial'][$i]}.jpg";
                            $url2 ="../admin/teacher/{$teacher_info['tserial'][$i]}";
                            echo"<tr class='active'><td><a href='$url2'><img height='50px' width='50px' src='$url'/></a></td>
                        <td><a href='$url2'>{$teacher_info['tname'][$i]}</a></td><td>{$teacher_info['tposition'][$i]}</td>
                        <td>{$teacher_info['taddress'][$i]}</td></tr>";
                        }
                    }
                    ?>
                </table>
                <div class="text-right">
                    <a href="../admin/currentteacher">View More <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php
        if (isset($class)) {
            for ($i = 0; $i < count($class); $i++) {
                if (isset($class_routine[$i]['time'])) {
                    if ($i == 0) {
                        echo"<li data-target='#carousel-example-generic' data-slide-to='$i' class='active'></li>";
                    } else {
                        echo"<li data-target='#carousel-example-generic' data-slide-to='$i'></li>";
                    }
                }
            }
        }
        ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php
        include_once 'common_function.php';
        $cm = new common_function();
        if (isset($class)) {
            for ($i = 0; $i < count($class); $i++) {
                if (isset($class_routine[$i]['time'])) {
                    if ($i == 0) {
                        $cm->print_class_routine($class_routine[$i]['time'], $class_routine[$i]['period'], $class_routine[$i]['sub'], $class_routine[$i]['teacher'], "active", $class[$i]);
                    } else {
                        $cm->print_class_routine($class_routine[$i]['time'], $class_routine[$i]['period'], $class_routine[$i]['sub'], $class_routine[$i]['teacher'], "", $class[$i]);
                    }
                }
            }
        }
        ?>
    </div>

    <!-- Controls -->
    <?php
    if (isset($class_routine[0]['time'])){
        echo"<a class='left carousel-control' href='#carousel-example-generic' role='button' data-slide='prev'>
        <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>
        <span class='sr-only'>Previous</span>
    </a>
    <a class='right carousel-control' href='#carousel-example-generic' role='button' data-slide='next'>
        <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>
        <span class='sr-only'>Next</span>
    </a>";
    }
    ?>
    
</div>


<?php
echo"<script>";
echo"$(function() {

    // Donut Chart
    Morris.Donut({
        element: 'morris-donut-chart',
        data:$people,
        resize: true
    });

 Morris.Bar({
        element: 'morris-bar-chart',
        data:$year_student,
        xkey: 'device',
        ykeys: ['geekbench'],
        labels: ['STUDENT'],
        barRatio: 0.4,
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true
    });
 Morris.Bar({
        element: 'morris-bar-chart2',
        data:$total_income_cost,
        xkey: 'device',
        ykeys: ['geekbench'],
        labels: ['TOTAL'],
        barRatio: 0.4,
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true
    });

    });";
echo"</script>";
?>
<script>
    $(function () {
        $('.carousel').carousel({
            interval: 2000
        })
    })
</script>
