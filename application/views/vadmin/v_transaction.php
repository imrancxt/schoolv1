
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="active">
                <a href="../admin/transaction">TRANSACTION</a>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-12">
        
        <div class="panel panel-default">
            <div class="panel panel-heading">Filter Transaction</div>
    <div class="col-lg-6">
    <form class='change_content_by_form' content="#trans_tbl"  action='../admin/import_trans' method='POST' enctype='multipart/form-data'>
        <div class="col-lg-12">
            <input required name="tn_date" class="form-control" placeholder="DATE" type="date"/>
        </div> 
        <div class="col-lg-12">
            <input required name="description" name='' class='form-control' placeholder='DESCRIPTION'/>
        </div>
        <div class="col-lg-12">
            <select name="dr_cr" class='form-control' >
                <option>DR</option>
                <option>CR</option>
            </select>
        </div>
        <div class="col-lg-12">
            <input required name="ammount" name='' class='form-control' placeholder='AMMOUNT'/>
        </div>
       <div class="col-lg-12">
        <button class="btn btn-default btn-raised" id="filter_current_student" status="current" style="width:100%;">FILTER</button>
    </div>
    </form>
        </div>
        
    
    
    <div class="col-lg-6">
    <form class='change_content_by_form' content="#trans_tbl"  action='../admin/filter_trans_tbl' method='POST' enctype='multipart/form-data'>
        <div class="col-lg-12">
            <input required  name="date1" class="form-control" placeholder="FORM DATE" type="date"/>
            <input required name="date2" class="form-control" placeholder="TO DATE" type="date"/>
            <button type="submit" class="btn btn-default btn-raised" style="width: 100%">VIEW</button>
        </div>
    </form>
        </div>
            </div>
    </div>
    </div>



<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <div class="table-responsive">
            <br><div class='alert alert-warning'>
                <h4>TRANSACTION TABLE(CASH ACCOUNT)<i class='fa fa-fw fa-smile-o'></i>.</h4>
            </div>
            <table class="table table-bordered table-hover" id="trans_tbl">
                <tr class="active">
                    <th>DATE</th>
                    <th>DESCRIPTION</th>
                    <th>DR(+)</th>
                    <th>CR(-)</th>
                    <th>BALANCE</th>
                    <th>EDIT</th>
                </tr>
                <?php
                $balance = 0;
                $balance+=$std_dr;
                echo"<tr class='warning'><td>TILL TODAY</td><td>PAID BY STUDENT</td><td>$std_dr</td><td>-</td><td>$balance</td><td></td></tr>";
                $balance-=$std_cr;
                echo"<tr class='warning'><td>TILL TODAY</td><td>PAID TO STUDENT</td><td>-</td><td>$std_cr</td><td>$balance</td><td></td></tr>";
                $balance+=$tec_dr;
                echo"<tr class='warning'><td>TILL TODAY</td><td>PAID BY TEACHER</td><td>$tec_dr</td><td>-</td><td>$balance</td><td></td></tr>";
                $balance-=$tec_cr;
                echo"<tr class='warning'><td>TILL TODAY</td><td>PAID TO TEACHER</td><td>-</td><td>$tec_cr</td><td>$balance</td><td></td></tr>";
                $balance+=$stf_dr;
                echo"<tr class='warning'><td>TILL TODAY</td><td>PAID BY STUFF</td><td>$stf_dr</td><td>-</td><td>$balance</td><td></td></tr>";
                $balance-=$stf_cr;
                echo"<tr class='warning'><td>TILL TODAY</td><td>PAID TO STUFF</td><td>-</td><td>$stf_cr</td><td>$balance</td><td></td></tr>";

                if (isset($tn_serial)) {
                    for ($i = 0; $i < count($tn_date); $i++) {
                        $balance = $balance + $dr[$i] - $cr[$i];
                        if ($dr[$i] > 0) {
                            $dr1 = $dr[$i];
                        } else {
                            $dr1 = "-";
                        }
                        if ($cr[$i] > 0) {
                            $cr1 = $cr[$i];
                        } else {
                            $cr1 = "-";
                        }
                        echo"<tr class='warning'><td>$tn_date[$i]</td><td>$description[$i]</td><td>$dr1</td><td>$cr1</td><td>$balance</td>
                                <td><button serial='$tn_serial[$i]' class='trns_edit_btn' data-toggle='modal' data-target='#exampleModal1' data-whatever='@mdo'>edit</button></td></tr>";
                    }
                }
                ?>
            </table>
        </div>
    </div>
        </div>
</div>


<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">EDIT TRANSACTION DATA</h4>
            </div>
            <form class='change_content_by_form' content="#tn_update_content"  action='<? echo base_url() ?>admin2/update_trans' method='POST' enctype='multipart/form-data'>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="trans_edit_tbl">
                            
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">UPDATE</button> 
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <div id="tn_update_content">
                    
                </div>
            </form>
        </div>
    </div>
</div>
<script src='<?php echo $GLOBALS['asset_url']; ?>js/customize.js'>
</script>
<script>
    $(document).ready(function(){
        
        $(document).on('click','.trns_edit_btn',function(){
            serial=$(this).attr('serial');
            page="admin2/tran_edit_data/"+serial;
            $("#tn_update_content").html("");
            change_content("#trans_edit_tbl",page);
        });
    });
</script>
