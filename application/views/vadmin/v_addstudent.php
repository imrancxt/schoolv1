

<!-- Page Heading -->

<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="../admin/dashboard">DASHBOARD</a>
            </li>
            <li class="">
                STUDENTS
            </li>
            <li class="active">
                <a href="../admin/addstudent">ADD NEW</a>
            </li>
        </ol>
    </div>
</div>

<form class='alert_ajax_form' action='../admin/add_new_student' method='POST' enctype='multipart/form-data'>
    <div class="row">
        <div class="col-lg-6">
            <button type="submit" class="btn btn-info" style="width:100%">Submit</button>
        </div>
        <div class="col-lg-6">
            <button type="reset" class="btn btn-info" style="width:100%">Reset</button>
        </div>

    </div><br>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="current_student_tbl">
                    <?php
                    $type = array('text', 'text', 'text', 'text', 'date');
                    $sfname = array('student_name', 'prev_ins', 'student_phone', 'student_email', 'birth_date');
                    $sfplaceholder = array("STUDENT's NAME", "PREVIOUS INSTITUTE", "PHONE NUMBER", 'E-MAIL', 'BIRTH DATE');
                    for ($i = 0; $i < count($sfname); $i++) {
                        echo"<tr><td>$sfplaceholder[$i]</td><td><input required type='$type[$i]' name='$sfname[$i]' class='form-control' placeholder='$sfplaceholder[$i]'></td></tr>";
                    }
                    echo"<tr><td>ADDRESS</td><td><textarea name='student_address' class='form-control' rows='3' placeholder='ADDRESS'></textarea></td></tr>";
                    ?>
                    <tr>
                        <td>
                            DEPARTMENT
                        </td>
                        <td>
                            <select name='dept' class="form-control">
                                <?php
                                $value = array("SCIENCE", "ARTS", "COMMERCE");
                                for ($i = 0; $i < count($value); $i++) {
                                    echo"<option>$value[$i]</option>";
                                    //echo"<label class='radio-inline'>
                                    //<input type='radio' name='dept' id='dept$i' value='$value[$i]' checked>$value[$i]</label>";
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>SESSION</td>
                        <td>
                            <?php
                            include_once 'common_function.php';
                            $cm = new common_function();
                            $cm->print_session();
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ROLL NO
                        </td>
                        <td>
                            <input required name="roll" class="form-control" placeholder="ROLL NO"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            CLASS
                        </td>
                        <td>
                            <select class="form-control" name="class">
                                <?php
                                $option = array("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                                foreach ($option as $option) {
                                    echo"<option>$option</option>";
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            SECTION
                        </td>
                        <td>
                            <select class="form-control" name="section">
                                <?php
                                $option = array("A", "B", "C", "D", "E", "F");
                                foreach ($option as $option) {
                                    echo"<option>$option</option>";
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>MONTHLY FEE</td>
                        <td>
                            <input required name="tution_fee" class="form-control" placeholder="MONTHLY TUTION FEE"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <?php
                    $paname = array("father_name", "mother_name", "religion", "parent_phone", "parent_email");
                    $paplaceholder = array("FATHERS'S NAME", "MOTHER'S NAME", "RELIGION", "PHONE NUMBER", "E-MAIL");
                    for ($i = 0; $i < count($paname); $i++) {
                        echo"<tr><td>$paplaceholder[$i]</td><td>
                                <div class='form-group'>
                                <input required type='text' name='$paname[$i]' class='form-control' placeholder='$paplaceholder[$i]'/>
                                </div>
                                </td></tr>";
                    }
                    ?>
                    <tr>
                        <td>ADDRESS</td>
                        <td>
                            <textarea name="parent_address" class="form-control" rows="3" placeholder="ADDRESS"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            FATHER'S OCCUPATION
                        </td>
                        <td>
                            <select  name='father_occupation' class="form-control">
                                <?php
                                if (isset($fatheroccupation)) {
                                    foreach ($fatheroccupation as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            MOTHER'S OCCUPATION
                        </td>
                        <td>
                            <select name='mother_occupation' class="form-control">
                                <?php
                                if (isset($motheroccupation)) {
                                    foreach ($motheroccupation as $option) {
                                        echo"<option>$option</option>";
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <button type="submit" class="btn btn-info" style="width:100%">Submit</button>
        </div>
        <div class="col-lg-6">
            <button type="reset" class="btn btn-info" style="width:100%">Reset</button>
        </div>

    </div>
</form>
<!-- /.row -->



