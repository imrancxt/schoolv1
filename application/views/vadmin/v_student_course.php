
        <div class="row">
            <div class="col-lg-6">
                <div class='alert alert-warning'>
                    <h4>
                        BASIC INFO
                    </h4>
                    <div class="row">
                        <div class="col-lg-4">
                            <?php
                            if (isset($std_serial)) {
                                $img_url = $GLOBALS['asset_url']. "img/profile/student/$std_serial[0].jpg";
                                echo"<img src='$img_url' height='150px' width='150px'>";
                            }
                            ?>
                        </div>
                        <div class="col-lg-8">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <?php
                                    if (isset($std_name)) {
                                        $basic_title = array('NAME', 'SESSION', 'CLASS', 'SECTION');
                                        $basic_info = array($std_name[0], $session[0], $class[0], $section[0]);
                                        for ($i = 0; $i < count($basic_title); $i++) {
                                            echo"<tr class='warning'><td>$basic_title[$i]</td><td>$basic_info[$i]</td></tr>";
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class='alert alert-warning'>
                        <form class='alert_ajax_form' content="#course_list"  action='<?php echo base_url(); ?>admin2/deletestudentcourse' method='POST' enctype='multipart/form-data'>
                            <h4>
                                TAKEN COURSE<button class="btn btn-warning">DELETE</button>
                            </h4>
                            <?php
                            if (isset($std_serial)) {
                                echo"<input type='hidden' name='student_serial' value='$std_serial[0]'/>";
                            }
                            ?>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr class="active">
                                        <th>SELECT</th>
                                        <th>COURSE TITLE</th>
                                        <th>COURSE NAME</th>
                                    </tr>
                                    <?php
                                    if (isset($std_course_serial)) {
                                        for ($i = 0; $i < count($std_course_serial); $i++) {
                                            echo"<tr class='warning'><td><input type='checkbox' value='$taken_course_serial[$i]' name='taken_course_serial[]'/></td>
                                        <td>$std_course_title[$i]</td>
                                        <td>$std_course_name[$i]</td>
                                        </tr>";
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="alert alert-warning">
                    <h4>CLICK ON A CLASS TO ADD NEW COURSES</h4>
                    <?php
                    $course_list = array("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                    foreach ($course_list as $course_list) {
                        echo"<button type='submit' class='get_course_list' title='$course_list'>class$course_list</button>";
                    }
                    ?>
                    <form class='alert_ajax_form' content="#course_list"  action='<?php echo base_url(); ?>admin/submitstudentcourse' method='POST' enctype='multipart/form-data'>
                        <?php
                        if (isset($std_serial)) {
                            echo"<input type='hidden' name='student_serial' value='$std_serial[0]'/>";
                        }
                        ?>
                        <div id="course_list">
                        </div>
                    </form>
                </div>
            </div>
        </div>
<style>
    .get_course_list{
        margin: 3px;
        margin-left: 0px;
        background-color:#ffcc66;
        color: white;
        border: 1px #ffcc66 solid;

    }
</style>