<form class='alert_ajax_form' action='<? echo base_url();?>admin/submitresultsheet' method='POST' enctype='multipart/form-data'>
    <div class="row">
        <div class="col-lg-3">
            <select class="form-control" name="exam_term">
                <?php
                $term = array('1ST TERM', '2ND TERM', '3RD TERM', 'FINAL');
                foreach ($term as $term) {
                    echo"<option>$term</option>";
                }
                ?>
            </select>
        </div>
        <div class="col-lg-3">
            <input required class="form-control" placeholder="GPA" name="gpa"/>
        </div>
        <div class="col-lg-3">
            <input required class="form-control" placeholder="COMMENT" name="comment"/>
        </div>
        <div class="col-lg-3">
            <button type="submit" class="btn btn-info" style="width: 100%">SUBMIT</button>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <tr class="active">
                <th>SERIAL</th>
                <th>COURSE TITLE</th>
                <th>COURSE NAME</th>
                <th>MARKS</th>
                <th>GRADE</th>
            </tr>
            <?php
            if (isset($std_course_serial)) {
                $grade = array("A+", "A", "A-", "B", "C","D","F");
                for ($i = 0; $i < count($std_course_serial); $i++) {
                    echo"<tr class='warning'>
                <td>$i<input type='hidden' value='$std_course_serial[$i]' name='course_serial[]'/></td>
                <td>$std_course_title[$i]</td>
                <td>$std_course_name[$i]</td>
                <td><input required class='form-control' name='marks[]'/></td>
                <td><select class='form-control' name='grade[]'>";
                    for ($j = 0; $j < count($grade); $j++) {
                        echo"<option>$grade[$j]</option>";
                    }
                    echo"</select></td>";
                    echo"</tr>";
                }
                echo"<input type='hidden' value='$std_serial' name='std_serial'/>";
            }
            ?>
        </table>
    </div>

</form>