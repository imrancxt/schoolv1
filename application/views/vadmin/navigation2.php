<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Admin Panel</title>
        <?
        $GLOBALS['asset_url'] = "http://localhost/school/";
        ?>
        <link href="<?php echo $GLOBALS['asset_url']; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $GLOBALS['asset_url']; ?>css/tooltip.css" rel="stylesheet">
        <link href="<?php echo $GLOBALS['asset_url']; ?>css/sb-admin.css" rel="stylesheet">

        <link href="<?php echo $GLOBALS['asset_url']; ?>css/plugins/morris.css" rel="stylesheet">

        <link href="<?php echo $GLOBALS['asset_url']; ?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $GLOBALS['asset_url']; ?>css/fileinput.css" rel="stylesheet">
        <link href="<?php echo $GLOBALS['asset_url']; ?>css/customize.css" rel="stylesheet">
        <link href="<?php echo $GLOBALS['asset_url']; ?>css/datepicker.css" rel="stylesheet">
        <link href="<?php echo $GLOBALS['asset_url']; ?>css/toggle.css" rel="stylesheet">
        
        <script src="<?php echo $GLOBALS['asset_url']; ?>js/jquery.js"></script>
        <script src="<?php echo $GLOBALS['asset_url']; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $GLOBALS['asset_url']; ?>js/admin.js"></script>
        <script src="<?php echo $GLOBALS['asset_url']; ?>js/tooltip.js"></script>
        <script src="<?php echo $GLOBALS['asset_url']; ?>js/fileinput.js"></script>
        <script src='<?php echo $GLOBALS['asset_url']; ?>js/plugins/morris/raphael.min.js'></script>
        <script src='<?php echo $GLOBALS['asset_url']; ?>js/plugins/morris/morris.js'></script>
        <script src='<?php echo $GLOBALS['asset_url']; ?>js/print.js'></script>
        <script src='<?php echo $GLOBALS['asset_url']; ?>js/datepicker.js'></script>
        <script src='<?php echo $GLOBALS['asset_url']; ?>js/toggle.js'></script>
        <script>
            $(document).ready(function(){
               have_fun=function(serial){
                   alert(serial);
               } 
            });
        </script>
    </head>

    <body style="background-color:<? echo"{$_SESSION['sidetop_color']}"; ?>">

        <div id="wrapper" style="background-color:<? echo"{$_SESSION['sidetop_color']}"; ?>">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                     <a class="navbar-brand" href="<?php echo base_url(); ?>admin/dashboard"> <? echo $_SESSION['school_name'];?></a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <form class='change_content_by_form' content=".container-fluid" action='<?php echo base_url(); ?>admin/search2' method='POST' enctype='multipart/form-data'>
                            <input name="search_query" id="search_query" class="form-control" type="search" placeholder="WHAT YOU WANT" style="margin-top:9px;"/>
                        </form>
                        <!-- <button id="search_button" class="btn btn-info" style="margin-top: 9px;margin-left: 5px;">SEARCH&nbsp<i class="fa fa-search"></i></button> -->
                    </li>
                    <li class="dropdown">
                        <button id="search_button" class="btn btn-info" style="margin-top: 9px;margin-left: 5px;">SEARCH&nbsp<i class="fa fa-search"></i></button> 
                        <!--<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>-->
                    </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa "></i>Have Fun <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="javascript:have_fun(1)"><i class="fa fa-fw fa-smile-o"></i>Snake Game</a>
                            </li>
                            <li>
                                <a href="javascript:have_fun(2)"><i class="fa fa-fw fa-smile-o"></i>Solar System</a>
                            </li>
                            <li>
                                <a href="javascript:have_fun(3)"><i class="fa fa-fw fa-smile-o"></i>Periodic Table</a>
                            </li>
                            
                            <li>
                                <a href="javascript:have_fun(4)"><i class="fa fa-fw fa-smile-o"></i> Calculator</a>
                            </li>
                        </ul>
                    </li>
                    <!--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu alert-dropdown">
                            <li>
                                <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">View All</a>
                            </li>
                        </ul>
                    </li>-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>Admin <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo base_url(); ?>admin2/admin_profile"><i class="fa fa-fw fa-user"></i>Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<? echo base_url(); ?>login/log_out"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse" style="background-color:<? echo"{$_SESSION['sidetop_color']}"; ?>">
                    <ul class="nav navbar-nav side-nav" style="background-color:<? echo"{$_SESSION['sidetop_color']}"; ?>">
                        <li class="ctive">
                            <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-fw fa-dashboard"></i>DASHBOARD</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i>STUDENTS<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/currentstudent">CURRENT</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/oldstudent">OLD</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/addstudent">ADD NEW</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i>TEACHERS<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo2" class="collapse">
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/currentteacher">CURRENT</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/oldteacher">OLD</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/addteacher">ADD NEW</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fa fa-fw fa-arrows-v"></i>STUFF<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo3" class="collapse">
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/currentstuff">CURRENT</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/oldstuff">OLD</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>admin/addstuff">ADD NEW</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/classroutine"><i class="fa fa-fw fa-fighter-jet"></i>CLASS ROUTINE</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/transaction"><i class="fa fa-fw fa-tree"></i>TRANSACTION</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/courses"><i class="fa fa-fw fa-xing-square"></i>COURSES</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/prepare_result_sheet"><i class="fa fa-fw fa-trophy"></i>MAKE RESULT </a>
                        </li>
                        
                      <!--  <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-arrows-v"></i>HAVE FUN<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo4" class="collapse">
                                <li>
                                    <a href="<?php //echo base_url(); ?>admin2/solarsystem">SOLAR SYSTEM</a>
                                </li>
                                <li>
                                    <a href="<?php //echo base_url(); ?>admin2/periodictable">PERIODIC TABLE</a>
                                </li>
                                <li>
                                    <a href="<?php //echo base_url(); ?>admin2/calculator">CALCULATOR</a>
                                </li>
                                 <li>
                                    <a href="<?php //echo base_url(); ?>admin2/game">GAME</a>
                                </li>
                            </ul>
                        </li>-->
                        <li>
                            <a href="<?php echo base_url(); ?>admin2/about_me"><i class="fa fa-fw fa-trophy"></i>ABOUT</a>
                        </li>
                         
                       
                        <!-- <li>
                             <a href="#"><i class="fa fa-fw fa-times-circle-o"></i>TIMETABLE</a>
                         </li>
                         <li>
                             <a href="#"><i class="fa fa-fw fa-reorder"></i>REPORTS</a>
                         </li>-->
                      

                    </ul>

                </div>
                <!-- /.navbar-collapse -->
            </nav>

