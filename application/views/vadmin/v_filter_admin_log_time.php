<thead>
    <tr>
        <th>SERIAL</th>
        <th>LOGIN TIME</th>
        <th>LOGOUT TIME</th>
        <th>SPENT TIME</th>
    </tr>
</thead>
<tbody>
    <?
    if (isset($log_serial)) {
        for ($i = 0; $i < count($log_serial); $i++) {
            if (date_create($logout_time[$i]) && date_create($login_time[$i])) {
                $date1 = date_create($logout_time[$i]);
                $date2 = date_create($login_time[$i]);
                $date = date_diff($date1, $date2);
                $spent = $date->format("%h hour:%i min:%s sec");
            } else {
                $spent = "UNKNOWN";
            }
            echo"<tr><td>$i</td><td>$login_time[$i]</td><td>$logout_time[$i]</td><td>$spent</td></tr>";
        }
    }
    ?>
</tbody>