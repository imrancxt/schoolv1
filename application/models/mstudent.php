<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mparent
 *
 * @author imran
 */
class mstudent extends CI_Model {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    public function madd_new_student($student_info, $parent_info) {
        $this->db->trans_start();
        $this->db->insert('student_info', $student_info);
        $this->db->insert('parent_info', $parent_info);
        echo"SAVED!";
        //print_r($student_info);
        $this->db->trans_complete();
    }

    public function get_current_student($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['serial'][$i] = $row->serial;
                $info['id'][$i] = $row->id;
                $info['class'][$i] = $row->class;
                $info['student_name'][$i] = $row->student_name;
                $info['student_phone'][$i] = $row->student_phone;
                $info['section'][$i] = $row->section;
                $info['roll'][$i] = $row->roll;
                $info['father_name'][$i] = $row->father_name;
                $info['mother_name'][$i] = $row->mother_name;
                $info['tution_fee'][$i] = $row->tution_fee;
                $i++;
            }
        }
        return $info;
    }

    public function mget_student_info($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['std_serial'][$i] = $row->serial;
                $info['std_tution_fee'][$i] = $row->tution_fee;
                $info['std_id'][$i] = $row->id;
                $info['std_name'][$i] = $row->student_name;
                $info['birth_date'][$i] = $row->birth_date;
                $info['prev_ins'][$i] = $row->prev_ins;
                $info['std_phone'][$i] = $row->student_phone;
                $info['std_email'][$i] = $row->student_email;
                $info['std_address'][$i] = $row->student_address;
                $info['class'][$i] = $row->class;
                $info['section'][$i] = $row->section;
                $info['session'][$i] = $row->session;
                $info['roll'][$i]=$row->roll;
                $i++;
            }
        }
        return $info;
    }

    public function mget_partent_info($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['par_serial'][$i] = $row->serial;
                $info['par_id'][$i] = $row->id;
                $info['father_name'][$i] = $row->father_name;
                $info['mother_name'][$i] = $row->mother_name;
                $info['religion'][$i] = $row->religion;
                $info['par_phone'][$i] = $row->parent_phone;
                $info['par_email'][$i] = $row->parent_email;
                $info['par_address'][$i] = $row->parent_address;
                $info['father_occupation'][$i] = $row->father_occupation;
                $info['mother_occupation'][$i] = $row->mother_occupation;
                $i++;
            }
        }
        return $info;
    }

    public function get_student_fee($query) {
        $info = array();
        $rs = $this->db->query($query);
        $info['fee_total'] = 0;
        $info['fee_due_total'] = 0;
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['fee_serial'][$i] = $row->serial;
                $info['fee_for'][$i] = $row->fee_for;
                $info['fee_amount'][$i] = $row->fee_amount;
                $info['fee_due_amount'][$i] = $row->fee_due_amount;
                $info['fee_date'][$i] = $row->fee_date;
                $info['fee_ins_date'][$i] = $row->insertion_date;
                $info['std_fee_payment_method'][$i] = $row->payment_method;
                $info['fee_total']+=$info['fee_amount'][$i];
                $info['fee_due_total']+=$info['fee_due_amount'][$i];
                $i++;
            }
        }
        return $info;
    }

    public function get_student_cost($query) {
        $info = array();
        $rs = $this->db->query($query);
        $info['pay_total'] = 0;
        $info['pay_due_total'] = 0;
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['pay_serial'][$i] = $row->serial;
                $info['pay_for'][$i] = $row->pay_for;
                $info['pay_amount'][$i] = $row->pay_amount;
                $info['pay_due_amount'][$i] = $row->pay_due_amount;
                $info['pay_date'][$i] = $row->pay_date;
                $info['pay_ins_date'][$i] = $row->insertion_date;
                $info['std_cost_payment_method'][$i] = $row->payment_method;
                $info['pay_total']+=$info['pay_amount'][$i];
                $info['pay_due_total']+= $info['pay_due_amount'][$i];
                $i++;
            }
        }
        return $info;
    }

    public function get_current_teacher($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result()as $row) {
                $info['tserial'][$i] = $row->serial;
                $info['tid'][$i] = $row->tid;
                $info['tname'][$i] = $row->tname;
                $info['tbdate'][$i] = $row->tbdate;
                $info['tjdate'][$i] = $row->tjdate;
                $info['tfname'][$i] = $row->tfname;
                $info['tmname'][$i] = $row->tmname;
                $info['tphone'][$i] = $row->tphone;
                $info['temail'][$i] = $row->temail;
                $info['tsallary'][$i] = $row->tsallary;
                $info['taddress'][$i] = $row->taddress;
                $info['tposition'][$i] = $row->tposition;
                $info['prev_exp'][$i] = $row->prev_exp;
                $info['tabout'][$i] = $row->about;
                $i++;
            }
        }
        return $info;
    }

    public function get_teacher_edu_info($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['edu_serial'][$i] = $row->serial;
                $info['edu_type'][$i] = $row->edu_type;
                $info['pass_year'][$i] = $row->pass_year;
                $info['specialization'][$i] = $row->specialization;
                $info['institute'][$i] = $row->institute;
                $info['cgpa'][$i] = $row->cgpa;
                $i++;
            }
        }
        return $info;
    }

    public function get_teacher_sallary_info($query) {
        $info = array();
        $rs = $this->db->query($query);
        $info['total_paid'] = 0;
        $info['total_sallary_due'] = 0;
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['paid_serial'][$i] = $row->serial;
                $info['sallary_date'][$i] = $row->sallary_date;
                $info['sallary_for'][$i] = $row->sallary_for;
                $info['sallary_ammount'][$i] = $row->sallary_ammount;
                $info['sallary_due_amount'][$i] = $row->sallary_due_amount;
                $info['sallary_payment_method'][$i]=$row->payment_method;
                $info['total_paid']+=$info['sallary_ammount'][$i];
                $info['total_sallary_due']+=$info['sallary_due_amount'][$i];
                $info['paid_insertion'][$i] = $row->insertion_date;
                $i++;
            }
        }
        return $info;
    }

    public function get_loan_from_teacher($query) {
        $info = array();
        $rs = $this->db->query($query);
        $info['total_loan'] = 0;
        $info['total_loan_due'] = 0;
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['loan_serial'][$i] = $row->serial;
                $info['loan_date'][$i] = $row->loan_date;
                $info['loan_for'][$i] = $row->loan_for;
                $info['loan_ammount'][$i] = $row->loan_ammount;
                $info['loan_due_amount'][$i] = $row->loan_due_amount;
                $info['total_loan']+=$info['loan_ammount'][$i];
                $info['total_loan_due']+=$info['loan_due_amount'][$i];
                $info['loan_insertion'][$i] = $row->insertion_date;
                $info['loan_payment_method'][$i] = $row->payment_method;
                $i++;
            }
        }
        return $info;
    }

    public function get_attendence($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                if ($row->status == "YES") {
                    $info["yatt"]["date$row->date"] = $row->date;
                    $info["yatt"]["title$row->date"] = $row->date_time;
                } else {
                    if ($row->status == "NO") {
                        $info["natt"]["date$row->date"] = $row->date;
                        $info["natt"]["title$row->date"] = $row->date_time;
                    }
                }
            }
        }
        return $info;
    }

    public function get_current_stuff($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result()as $row) {
                $info['stserial'][$i] = $row->serial;
                $info['stid'][$i] = $row->stid;
                $info['stname'][$i] = $row->stname;
                $info['stbdate'][$i] = $row->stbdate;
                $info['stjdate'][$i] = $row->stjdate;
                $info['stfname'][$i] = $row->stfname;
                $info['stmname'][$i] = $row->stmname;
                $info['stphone'][$i] = $row->stphone;
                $info['stemail'][$i] = $row->stemail;
                $info['stsallary'][$i] = $row->stsallary;
                $info['staddress'][$i] = $row->staddress;
                $info['stposition'][$i] = $row->stposition;
                $info['prev_exp'][$i] = $row->prev_exp;
                $info['stabout'][$i] = $row->about;
                $i++;
            }
        }
        return $info;
    }

    public function get_stuff_edu_info($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['edu_serial'][$i] = $row->serial;
                $info['edu_type'][$i] = $row->edu_type;
                $info['pass_year'][$i] = $row->pass_year;
                $info['specialization'][$i] = $row->specialization;
                $info['institute'][$i] = $row->institute;
                $info['cgpa'][$i] = $row->cgpa;
                $i++;
            }
        }
        return $info;
    }

    public function get_stuff_sallary_info($query) {
        $info = array();
        $rs = $this->db->query($query);
        $info['total_paid'] = 0;
        $info['total_sallary_due'] = 0;
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['paid_serial'][$i] = $row->serial;
                $info['sallary_date'][$i] = $row->sallary_date;
                $info['sallary_for'][$i] = $row->sallary_for;
                $info['sallary_ammount'][$i] = $row->sallary_ammount;
                $info['sallary_due_amount'][$i] = $row->sallary_due_amount;
                $info['total_paid']+=$info['sallary_ammount'][$i];
                $info['total_sallary_due']+= $info['sallary_due_amount'][$i];
                $info['paid_insertion'][$i] = $row->insertion_date;
                $i++;
            }
        }
        return $info;
    }

    public function get_paid_by_stuff($query) {
        $info = array();
        $info['total_paidby'] = 0;
        $info['total_paidby_due'] = 0;
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['paidby_serial'][$i] = $row->serial;
                $info['pay_date'][$i] = $row->pay_date;
                $info['pay_for'][$i] = $row->pay_for;
                $info['pay_ammount'][$i] = $row->pay_ammount;
                $info['pay_due_amount'][$i] = $row->pay_due_amount;
                $info['paidby_insertion'][$i] = $row->insertion_date;
                $info['paid_payment_method'][$i]=$row->payment_method;
                $info['total_paidby']+=$info['pay_ammount'][$i];
                $info['total_paidby_due']+=$info['pay_due_amount'][$i];
                $i++;
            }
        }
        return $info;
    }

    public function mget_class_routine($class, $admin, $day) {
        $query = "SELECT * FROM `classroutine` where admin=$admin and class='$class' order by serial";
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            $period = "$day" . "period";
            $time = "$day" . "time";
            $teacher = "$day" . "teacher";
            $sub = "$day" . "sub";
            foreach ($rs->result() as $row) {
                $info['day'][$i] = $row->day;
                $info['period'][$i] = $row->period;
                $info['time'][$i] = $row->time;
                $info['teacher'][$i] = $row->teacher;
                $info['sub'][$i] = $row->sub;
                $i++;
            }
        }
        return $info;
    }

    public function get_transaction_data($query) {
        $rs = $this->db->query($query);
        $info = array();

        if ($rs->num_rows() > 0) {
            $i = 0;
            $j = 0;
            $k = 1;
            foreach ($rs->result() as $row) {
                $info['tn_serial'][$i]=$row->serial;
                $info['tn_date'][$i] = $row->tn_date;
                $info['description'][$i] = $row->description;
                $info['dr'][$i] = $row->dr;
                $info['cr'][$i] = $row->cr;
                $i++;
            }
        }
        return $info;
    }

    public function get_course_data($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['serial'][$i] = $row->serial;
                $info['course_title'][$i] = $row->course_title;
                $info['course_name'][$i] = $row->course_name;
                $i++;
            }
        }
        return $info;
    }

    public function get_student_course($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['std_course_serial'][$i] = $row->serial;
                $info['std_course_title'][$i] = $row->course_title;
                $info['std_course_name'][$i] = $row->course_name;
                $info['taken_course_serial'][$i] = $row->std_course_serial;
                $i++;
            }
        }
        return $info;
    }

    public function get_exam_info($query) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['exam_serial'][$i] = $row->serial;
                $info['exam_term'][$i] = $this->get_term($row->exam_term);
                $info['term'][$i] = $row->exam_term;
                $info['exam_comment'][$i] = $row->comment;
                $info['exam_gpa'][$i] = $row->gpa;
                $info['exam_marks'][$i] = $row->total_marks;
                $i++;
            }
        }
        return $info;
    }

    private function get_term($term) {
        switch ($term) {
            case '1':
                return "1ST TERM";
            case '2':
                return '2ND TERM';
            case '3':
                return '3RD TERM';
            case 'f':
                return 'FINAL';
            default:
                return 'FINAL';
        }
    }

    public function get_course_marks($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['serial'][$i] = $row->serial;
                $info['mcourse_serial'][$i] = $row->course_serial;
                $info['mcourse_title'][$i] = $row->course_title;
                $info['mcourse_name'][$i] = $row->course_name;
                $info['mcourse_marks'][$i] = $row->marks;
                $info['mcourse_grade'][$i] = $row->grade;
                $i++;
            }
        }
        return $info;
    }

    public function get_account($query) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                array_push($info, array('d' => "$row->date", 'visits' => "$row->ammount"));
                $i = 0;
            }
        }
        return json_encode($info, JSON_NUMERIC_CHECK);
    }

    public function get_public($query) {
        $rs = $this->db->query($query);
        $info = array();
        $type = array("STUDENT", "TEACHER", "STUFF");
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                //echo"$row->total";
                array_push($info, array('label' => "$type[$i]", 'value' => "$row->total"));
                $i++;
            }
        }
        return json_encode($info, JSON_NUMERIC_CHECK);
    }

    public function get_year_student_graph($query) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                array_push($info, array('device' => "YEAR:$row->year", 'geekbench' => $row->student));
            }
        }
        return json_encode($info, JSON_NUMERIC_CHECK);
    }

    public function get_income_cost_graph($query) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                //array_push($info, array('device' => "INCOME-$row->income", 'geekbench' => "$row->cost"));
                $total_transaction=$row->income+$row->cost;
                array_push($info, array('device' => "INCOME:$row->income", 'geekbench' =>$row->income));
                array_push($info, array('device' => "COST:$row->cost", 'geekbench' =>$row->cost));
            }
        }
        return json_encode($info, JSON_NUMERIC_CHECK);
    }

    public function check_class_routine($query) {
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function get_person_cost($query, $data) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info[$data[0]][$i] = $row->$data[1];
                $i++;
            }
        }
        return $info;
    }

    public function get_only_teacher($query) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['teacher_name'][$i] = $row->tname;
                $info['teacher_id'][$i] = $row->tid;
                $i++;
            }
        }
        return $info;
    }

    public function get_other_transaction_data($query) {
        $rs = $this->db->query($query);
        $info = array();
        $info['std_cr'] = 0;
        $info['std_dr'] = 0;
        $info['tec_dr'] = 0;
        $info['tec_cr'] = 0;
        $info['stf_dr'] = 0;
        $info['stf_cr'] = 0;
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                $info['std_cr'] = $row->std_cr;
                $info['std_dr'] = $row->std_dr;
                $info['tec_dr'] = $row->tec_dr;
                $info['tec_cr'] = $row->tec_cr;
                $info['stf_dr'] = $row->stf_dr;
                $info['stf_cr'] = $row->stf_cr;
            }
        }
        return $info;
    }

    public function get_logged_time($query) {
        $rs = $this->db->query($query);
        $info = array();
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info['log_serial'][$i] = $row->serial;
                $info['login_time'][$i] = $row->login_time;
                $info['logout_time'][$i] = $row->logout_time;
                $i++;
            }
        }
        return $info;
    }

    public function get_admin_info($query) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                $info['admin_serial'] = $row->serial;
                $info['admin_name'] = $row->admin;
                $info['password'] = $row->password;
                $info['school_name'] = $row->school_name;
                $info['school_address'] = $row->school_address;
            }
        }
        return $info;
    }

    public function get_total_amount($query, $var) {
        $info = array();
        $info[$var] = 0;
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                $info[$var] = $row->$var;
            }
        }
        return $info;
    }

    public function check_exam($query) {
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    public function check_data_base($query){
        $rs=$this->db->query($query);
        if($rs->num_rows>0){
            return true;
        }
        else{
            return false;
        }
    }

}

?>
